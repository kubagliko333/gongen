function(gen_split_identifier identifier out_namespace out_item)
	string(FIND ${identifier} "::" id_split_1)
	if(id_split_1 EQUAL -1)
		message(FATAL_ERROR "Could not split identifier - the string '${identifier}' does not contain the '::' separator!")
	endif()
	math(EXPR id_split_2 "${id_split_1}+2")
	string(LENGTH ${identifier} id_length)
	string(SUBSTRING ${identifier} 0 ${id_split_1} namespace_id)
	string(SUBSTRING ${identifier} ${id_split_2} -1 item_id)
	set(${out_namespace} ${namespace_id} PARENT_SCOPE)
	set(${out_item} ${item_id} PARENT_SCOPE)
endfunction()

function(gen_define_unit LIB_SUFFIX)
	set(options NO_INSTANCES)
	set(one_value_args TYPE ID NAME)
	set(multi_value_args SOURCES)
	cmake_parse_arguments(gen_register_unit "${options}" "${one_value_args}" "${multi_value_args}" "${ARGN}")
	gen_split_identifier(${gen_register_unit_ID} namespace_id item_id)
	string(TOUPPER ${namespace_id} namespace_id_upper)
	string(TOUPPER ${item_id} item_id_upper)
	set(gen_register_unit_LIB_NAME "GEN_${namespace_id_upper}_${item_id_upper}_${LIB_SUFFIX}")

	add_library(${gen_register_unit_LIB_NAME} STATIC ${gen_register_unit_SOURCES})
	target_link_libraries(${gen_register_unit_LIB_NAME} "GEN_GEN_CORE_${LIB_SUFFIX}")
	target_include_directories(${gen_register_unit_LIB_NAME} PUBLIC SYSTEM "${GEN_SRC_DIR}/include")
	set(${gen_register_unit_LIB_NAME}_TYPE ${gen_register_unit_TYPE} PARENT_SCOPE)
	set(${gen_register_unit_LIB_NAME}_ID ${gen_register_unit_ID} PARENT_SCOPE)
	set(${gen_register_unit_LIB_NAME}_NAME ${gen_register_unit_NAME} PARENT_SCOPE)
	set(${gen_register_unit_LIB_NAME}_NO_INSTANCES ${gen_register_unit_NO_INSTANCES} PARENT_SCOPE)
endfunction()

function(gen_generate_unit_loader LIB_SUFFIX)
	set(multi_value_args UNITS)
	cmake_parse_arguments(target_add_gongen "" "" "${multi_value_args}" "${ARGN}")

	set(UNIT_LOADER_SOURCE "${CMAKE_BINARY_DIR}/gongen_unit_loader_${LIB_SUFFIX}.cpp")
	file(WRITE ${UNIT_LOADER_SOURCE}
"#include <gongen_unit_loader/loader.hpp>

void gen_unit_loader::loadUnitsToRegistry(AbstractUnitRegistry* registry) {
"
	)
	foreach(UNIT IN LISTS target_add_gongen_UNITS)
		gen_split_identifier(${UNIT} namespace_id item_id)
		string(TOUPPER ${namespace_id} namespace_id_upper)
		string(TOUPPER ${item_id} item_id_upper)
		set(lib_name "GEN_${namespace_id_upper}_${item_id_upper}_${LIB_SUFFIX}")

		string(TOLOWER ${${lib_name}_NO_INSTANCES} no_instances)
		file(APPEND ${UNIT_LOADER_SOURCE}
"	registry->registerUnit(UnitMeta{.type = UnitType::${${lib_name}_TYPE}, .idLiteral = \"${UNIT}\", .name = \"${${lib_name}_NAME}\", .noInstances = ${no_instances}});
"
		)
	endforeach()
	file(APPEND ${UNIT_LOADER_SOURCE} "}")
	add_library("GongEnUnitLoader_${LIB_SUFFIX}" ${UNIT_LOADER_SOURCE})
	target_include_directories("GongEnUnitLoader_${LIB_SUFFIX}" PUBLIC SYSTEM "${GEN_SRC_DIR}/include")
endfunction()