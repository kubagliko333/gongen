set(CPM_DOWNLOAD_VERSION "0.35.0")
set(cpmlicenses_TAG "v0.0.5")

# GongEn deps
set(glfw_TAG "62e175ef9fae75335575964c845a302447c012c7")
set(tinyxml2_TAG "9.0.0")
set(zstd_TAG "v1.5.2")
set(volk_TAG "1.3.215")
set(vulkan_headers_TAG "v1.3.215")
set(vkmemalloc_TAG "v3.0.1")
set(imgui_TAG "docking")

# GenTools deps
set(bc7enc_TAG "1a4e9dbe8c3bcbcb9a7ecb237ea3afff8a29fddd")
set(freetype_TAG "VER-2-12-1")
set(msdfgen_TAG "v1.9.2")

# ExampleApp and GenTools deps
set(stb_TAG "af1a5bc352164740c1cc1354942b1c6b72eacb8a")
