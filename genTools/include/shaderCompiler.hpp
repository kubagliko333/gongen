#pragma once

#include <3rd_party/shaderc.h>
#include <3rd_party/spirv_cross.h>
#include <vector>
#include <stdint.h>
#include <string>
#include <gongen/graphics_api/graphics_def.hpp>
#include <gongen/core/hash.hpp>
#include <list>
#include <atomic>

extern void unloadShaderc();
extern bool loadShaderc();

extern void unloadSPIRVCross();
extern bool loadSPIRVCross();

namespace gent {
	struct ShaderVariant;

	namespace internal {
		class IShaderCompiler {
			spvc_context context = nullptr;
		public:
			IShaderCompiler();
			virtual ~IShaderCompiler() {}
			virtual std::vector<uint32_t> compileSPIRV(const std::string& code, const std::string& filename, gen::ShaderStage type, std::vector<std::pair<std::string, std::string>> macros) = 0;
			virtual std::vector<uint32_t> compileSPIRV(const std::string& filename, gen::ShaderStage type, std::vector<std::pair<std::string, std::string>> macros) = 0;
			virtual gen::ShaderMetadata reflectSPIRV(std::vector<uint32_t>& spirv);
			void initSPIRVCross();

			std::string compileGLSL(uint32_t stage, gent::ShaderVariant* variant, gen::ShaderMetadata* metadata, const std::string& code, const std::string& filename, std::vector<uint32_t>& spirv, bool onlyReflect, gen::ShaderType type);
		};

		class ShadercShaderCompiler :public IShaderCompiler {
			shaderc_compiler_t compiler = nullptr;
			shaderc_compile_options_t compileOptions = nullptr;
			spvc_context context = nullptr;
		public:
			ShadercShaderCompiler();
			~ShadercShaderCompiler();
			std::vector<uint32_t> compileSPIRV(const std::string& code, const std::string& filename, gen::ShaderStage type, std::vector<std::pair<std::string, std::string>> macros);
			std::vector<uint32_t> compileSPIRV(const std::string& filename, gen::ShaderStage type, std::vector<std::pair<std::string, std::string>> macros);
		};
	}

	struct ShaderStage {
		gen::ShaderMetadata metadata;

		std::vector<uint32_t> spirv;
		std::string glsl;
	};

	struct BindingRemapContext {
		std::atomic<uint32_t> nextBuffer = 0;
		std::atomic<uint32_t> nextImage = 0;
		std::atomic<uint32_t> nextTexture = 0;
		std::atomic<uint32_t> nextTexture2 = 0;
		std::atomic<uint32_t> nextSampler = 0;
		std::atomic<uint32_t> nextSRV = 0;
		std::atomic<uint32_t> nextUAV = 0;
	};

	struct ShaderVariant {
		uint32_t shaderTypes;
		uint32_t stageFlags;

		BindingRemapContext glslRemapContext;
		BindingRemapContext elslRemapContext;
		BindingRemapContext hlslRemapContext;

		std::vector<std::pair<std::string, std::string>> macros;

		ShaderStage stages[(size_t)gen::ShaderStage::TOTAL_SHADER_STAGES];
	};

	struct Shader {
		uint32_t shaderFlags;
		uint32_t stageFlags;
		std::string name;
		std::string file;

		std::list<ShaderVariant*> variants;

		Shader() {}

		ShaderVariant* getVariant(std::vector<std::pair<std::string, std::string>> macros) {
			for (auto& v : variants) {
				if (v->macros.size() == macros.size()) {
					bool same = true;
					for (uint32_t i = 0; i < v->macros.size(); i++) {
						if (macros[i].first != v->macros[i].first) same = false;
						if (macros[i].second != v->macros[i].second) same = false;
					}
					if (same) return v;
				}
			}

			return nullptr;
		}
	};

	class ShaderCompiler {
	public:
		std::list<gent::Shader*> shaders;

		Shader* addShader(const std::string& name, const std::string& filename, uint32_t shaderFlags, const std::string& stages);
		ShaderVariant* addShaderVariant(Shader *shader, std::vector<std::pair<std::string, std::string>> macros, uint32_t types);
	};
}
