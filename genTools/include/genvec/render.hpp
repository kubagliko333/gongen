#pragma once

#include <stdint.h>
#include "editor.hpp"
#include <memory>
#include <limits.h>
#include <gongen/part_renderer/partRenderer.hpp>

#define VERTEX_TYPE_FILL 0
#define VERTEX_TYPE_QUADRATIC_SPLINE 1
#define VERTEX_TYPE_NEGATIVE_SIGN_FLAG 128

#define MAX_GRADIENT_POINTS 2
#define MAX_COLORS 2

#define PART_FLAG_FLAT_COLOR 1
#define PART_FLAG_LINEAR_GRADIENT 2
#define PART_FLAG_DONT_RENDER 4

namespace genvec {
#pragma pack(push, 1)
	struct previewVertex {
		float x;
		float y;
		uint8_t r, g, b, a;
	};
#pragma pack(pop)

	struct mesh {
		std::vector<gen::vectorVertex> vertexData;
		std::vector<uint32_t> indexData;
	};

	struct previewMesh {
		std::vector<previewVertex> vertexData;
		std::vector<uint32_t> indexData;
	};

	void triangulate(genvec::object* object);
	void createPreviewMesh(genvec::object* object);

	void render(gen::CommandBufferPtr &commandBuffer, genvec::mesh* m, const gen::vectorPartInstanceData &p);
	void render(gen::CommandBufferPtr &commandBuffer, genvec::previewMesh* m);

	extern mesh activeMesh;
	extern previewMesh activePreviewMesh;
}
