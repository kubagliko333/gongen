#pragma once

#include <stdint.h>
#include <vector>

namespace genvec {
	struct point {
		uint64_t id;

		float x, y;
	};

	enum class LineType {
		LINE,
		QUADRATIC_SPLINE
		//CUBIC_SPLINE
	};

	struct line {
		LineType type;

		uint64_t point0;
		uint64_t point1;

		uint64_t control0;
		uint64_t control1;
	};

	struct object {
		std::vector<point> points;
		std::vector<line> lines;

		point* getPoint(uint64_t id);
	};

	void editorMain();
}
