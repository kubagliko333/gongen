#pragma once

#include <gongen/gcf/gcf.hpp>
#include <vector>

namespace gent {
	enum class TextureType {
		TEXTURE_2D,
		TEXTURE_2D_PACKED
	};

	enum class TextureFormat {
		PNG,
		BC1,
		BC3,
		BC4,
		BC5,
		BC7
	};

	struct SubTexture {
		uint32_t layer;
		uint32_t comp;
		uint32_t w;
		uint32_t h;
		void* pixelData;
		std::string name;

		gen::Hash128 hash;
	};

	struct PackedTexture {
		std::string name;

		uint32_t x;
		uint32_t y;
		uint32_t w;
		uint32_t h;
		uint32_t layer;
	};

	struct TextureVariant {
		TextureFormat format;
		gen::CompressionMode compression;
		void* data = nullptr;
	};

	class Texture {
		uint32_t layers;
		uint32_t packedSize;
		std::vector<SubTexture> subTextures;
		std::vector<PackedTexture> packedTextures;
		bool packed;
		bool mipmaps;

		std::vector<TextureVariant> variants;

		std::string name;

		void pack(std::vector<gent::SubTexture> &out);
		void processSubTexture(gent::TextureVariant *variant, gent::SubTexture* subTexture, tinyxml2::XMLElement *pNode, gen::GCFHandle& handle, uint32_t level, gen::Hash128 textureHash);
	public:
		Texture(TextureType type, bool mipmaps, std::string name, uint32_t layers = 1, uint32_t packedSize = 0);
		~Texture();
		void addSubTexture(const std::string& filepath, uint32_t layer, const std::string& name = "");
		void addSubTexture(void* pixelData, uint32_t w, uint32_t h, uint32_t comp, uint32_t layer, const std::string& name = "");
		void addVariant(TextureFormat format, gen::CompressionMode compression);
		void save(gen::GCFHandle& handle, tinyxml2::XMLElement* pGCF_Node);
	};
}
