#pragma once

#include <string>
#include <gongen/core/hash.hpp>
#include <tinyxml2.h>
#include <unordered_map>

namespace gent {
	struct CacheEntry {
		std::string filename;

		gen::Hash128 hash;
	};

	class Cache {
		std::unordered_map<std::string, CacheEntry> entries;
		tinyxml2::XMLDocument xmlDoc;

		std::string dir;
	public:
		Cache(const std::string& dir = "cache");
		void save();
		void addEntry(std::string name, gen::Hash128 hash, void *data, uint64_t dataSize);
		bool checkEntry(std::string name, gen::Hash128 hash);
		void* getEntryData(std::string name, gen::Hash128 hash, uint64_t& dataSize);
	};
}
