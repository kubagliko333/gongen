// Copyright 2015 The Shaderc Authors. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef SHADERC_SHADERC_H_
#define SHADERC_SHADERC_H_

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

    // Source language kind.
    typedef enum {
        shaderc_source_language_glsl,
        shaderc_source_language_hlsl,
    } shaderc_source_language;

    typedef enum {
        // Forced shader kinds. These shader kinds force the compiler to compile the
        // source code as the specified kind of shader.
        shaderc_vertex_shader,
        shaderc_fragment_shader,
        shaderc_compute_shader,
        shaderc_geometry_shader,
        shaderc_tess_control_shader,
        shaderc_tess_evaluation_shader,

        shaderc_glsl_vertex_shader = shaderc_vertex_shader,
        shaderc_glsl_fragment_shader = shaderc_fragment_shader,
        shaderc_glsl_compute_shader = shaderc_compute_shader,
        shaderc_glsl_geometry_shader = shaderc_geometry_shader,
        shaderc_glsl_tess_control_shader = shaderc_tess_control_shader,
        shaderc_glsl_tess_evaluation_shader = shaderc_tess_evaluation_shader,

        // Deduce the shader kind from #pragma annotation in the source code. Compiler
        // will emit error if #pragma annotation is not found.
        shaderc_glsl_infer_from_source,
        // Default shader kinds. Compiler will fall back to compile the source code as
        // the specified kind of shader when #pragma annotation is not found in the
        // source code.
        shaderc_glsl_default_vertex_shader,
        shaderc_glsl_default_fragment_shader,
        shaderc_glsl_default_compute_shader,
        shaderc_glsl_default_geometry_shader,
        shaderc_glsl_default_tess_control_shader,
        shaderc_glsl_default_tess_evaluation_shader,
        shaderc_spirv_assembly,
        shaderc_raygen_shader,
        shaderc_anyhit_shader,
        shaderc_closesthit_shader,
        shaderc_miss_shader,
        shaderc_intersection_shader,
        shaderc_callable_shader,
        shaderc_glsl_raygen_shader = shaderc_raygen_shader,
        shaderc_glsl_anyhit_shader = shaderc_anyhit_shader,
        shaderc_glsl_closesthit_shader = shaderc_closesthit_shader,
        shaderc_glsl_miss_shader = shaderc_miss_shader,
        shaderc_glsl_intersection_shader = shaderc_intersection_shader,
        shaderc_glsl_callable_shader = shaderc_callable_shader,
        shaderc_glsl_default_raygen_shader,
        shaderc_glsl_default_anyhit_shader,
        shaderc_glsl_default_closesthit_shader,
        shaderc_glsl_default_miss_shader,
        shaderc_glsl_default_intersection_shader,
        shaderc_glsl_default_callable_shader,
        shaderc_task_shader,
        shaderc_mesh_shader,
        shaderc_glsl_task_shader = shaderc_task_shader,
        shaderc_glsl_mesh_shader = shaderc_mesh_shader,
        shaderc_glsl_default_task_shader,
        shaderc_glsl_default_mesh_shader,
    } shaderc_shader_kind;

    typedef enum {
        shaderc_profile_none,  // Used if and only if GLSL version did not specify
                               // profiles.
                               shaderc_profile_core,
                               shaderc_profile_compatibility,  // Disabled. This generates an error
                               shaderc_profile_es,
    } shaderc_profile;

    // Optimization level.
    typedef enum {
        shaderc_optimization_level_zero,  // no optimization
        shaderc_optimization_level_size,  // optimize towards reducing code size
        shaderc_optimization_level_performance,  // optimize towards performance
    } shaderc_optimization_level;

    // Resource limits.
    typedef enum {
        shaderc_limit_max_lights,
        shaderc_limit_max_clip_planes,
        shaderc_limit_max_texture_units,
        shaderc_limit_max_texture_coords,
        shaderc_limit_max_vertex_attribs,
        shaderc_limit_max_vertex_uniform_components,
        shaderc_limit_max_varying_floats,
        shaderc_limit_max_vertex_texture_image_units,
        shaderc_limit_max_combined_texture_image_units,
        shaderc_limit_max_texture_image_units,
        shaderc_limit_max_fragment_uniform_components,
        shaderc_limit_max_draw_buffers,
        shaderc_limit_max_vertex_uniform_vectors,
        shaderc_limit_max_varying_vectors,
        shaderc_limit_max_fragment_uniform_vectors,
        shaderc_limit_max_vertex_output_vectors,
        shaderc_limit_max_fragment_input_vectors,
        shaderc_limit_min_program_texel_offset,
        shaderc_limit_max_program_texel_offset,
        shaderc_limit_max_clip_distances,
        shaderc_limit_max_compute_work_group_count_x,
        shaderc_limit_max_compute_work_group_count_y,
        shaderc_limit_max_compute_work_group_count_z,
        shaderc_limit_max_compute_work_group_size_x,
        shaderc_limit_max_compute_work_group_size_y,
        shaderc_limit_max_compute_work_group_size_z,
        shaderc_limit_max_compute_uniform_components,
        shaderc_limit_max_compute_texture_image_units,
        shaderc_limit_max_compute_image_uniforms,
        shaderc_limit_max_compute_atomic_counters,
        shaderc_limit_max_compute_atomic_counter_buffers,
        shaderc_limit_max_varying_components,
        shaderc_limit_max_vertex_output_components,
        shaderc_limit_max_geometry_input_components,
        shaderc_limit_max_geometry_output_components,
        shaderc_limit_max_fragment_input_components,
        shaderc_limit_max_image_units,
        shaderc_limit_max_combined_image_units_and_fragment_outputs,
        shaderc_limit_max_combined_shader_output_resources,
        shaderc_limit_max_image_samples,
        shaderc_limit_max_vertex_image_uniforms,
        shaderc_limit_max_tess_control_image_uniforms,
        shaderc_limit_max_tess_evaluation_image_uniforms,
        shaderc_limit_max_geometry_image_uniforms,
        shaderc_limit_max_fragment_image_uniforms,
        shaderc_limit_max_combined_image_uniforms,
        shaderc_limit_max_geometry_texture_image_units,
        shaderc_limit_max_geometry_output_vertices,
        shaderc_limit_max_geometry_total_output_components,
        shaderc_limit_max_geometry_uniform_components,
        shaderc_limit_max_geometry_varying_components,
        shaderc_limit_max_tess_control_input_components,
        shaderc_limit_max_tess_control_output_components,
        shaderc_limit_max_tess_control_texture_image_units,
        shaderc_limit_max_tess_control_uniform_components,
        shaderc_limit_max_tess_control_total_output_components,
        shaderc_limit_max_tess_evaluation_input_components,
        shaderc_limit_max_tess_evaluation_output_components,
        shaderc_limit_max_tess_evaluation_texture_image_units,
        shaderc_limit_max_tess_evaluation_uniform_components,
        shaderc_limit_max_tess_patch_components,
        shaderc_limit_max_patch_vertices,
        shaderc_limit_max_tess_gen_level,
        shaderc_limit_max_viewports,
        shaderc_limit_max_vertex_atomic_counters,
        shaderc_limit_max_tess_control_atomic_counters,
        shaderc_limit_max_tess_evaluation_atomic_counters,
        shaderc_limit_max_geometry_atomic_counters,
        shaderc_limit_max_fragment_atomic_counters,
        shaderc_limit_max_combined_atomic_counters,
        shaderc_limit_max_atomic_counter_bindings,
        shaderc_limit_max_vertex_atomic_counter_buffers,
        shaderc_limit_max_tess_control_atomic_counter_buffers,
        shaderc_limit_max_tess_evaluation_atomic_counter_buffers,
        shaderc_limit_max_geometry_atomic_counter_buffers,
        shaderc_limit_max_fragment_atomic_counter_buffers,
        shaderc_limit_max_combined_atomic_counter_buffers,
        shaderc_limit_max_atomic_counter_buffer_size,
        shaderc_limit_max_transform_feedback_buffers,
        shaderc_limit_max_transform_feedback_interleaved_components,
        shaderc_limit_max_cull_distances,
        shaderc_limit_max_combined_clip_and_cull_distances,
        shaderc_limit_max_samples,
    } shaderc_limit;

    // Uniform resource kinds.
    // In Vulkan, uniform resources are bound to the pipeline via descriptors
    // with numbered bindings and sets.
    typedef enum {
        // Image and image buffer.
        shaderc_uniform_kind_image,
        // Pure sampler.
        shaderc_uniform_kind_sampler,
        // Sampled texture in GLSL, and Shader Resource View in HLSL.
        shaderc_uniform_kind_texture,
        // Uniform Buffer Object (UBO) in GLSL.  Cbuffer in HLSL.
        shaderc_uniform_kind_buffer,
        // Shader Storage Buffer Object (SSBO) in GLSL.
        shaderc_uniform_kind_storage_buffer,
        // Unordered Access View, in HLSL.  (Writable storage image or storage
        // buffer.)
        shaderc_uniform_kind_unordered_access_view,
    } shaderc_uniform_kind;

    // Usage examples:
    //
    // Aggressively release compiler resources, but spend time in initialization
    // for each new use.
    //      shaderc_compiler_t compiler = shaderc_compiler_initialize();
    //      shaderc_compilation_result_t result = shaderc_compile_into_spv(
    //          compiler, "#version 450\nvoid main() {}", 27,
    //          shaderc_glsl_vertex_shader, "main.vert", "main", nullptr);
    //      // Do stuff with compilation results.
    //      shaderc_result_release(result);
    //      shaderc_compiler_release(compiler);
    //
    // Keep the compiler object around for a long time, but pay for extra space
    // occupied.
    //      shaderc_compiler_t compiler = shaderc_compiler_initialize();
    //      // On the same, other or multiple simultaneous threads.
    //      shaderc_compilation_result_t result = shaderc_compile_into_spv(
    //          compiler, "#version 450\nvoid main() {}", 27,
    //          shaderc_glsl_vertex_shader, "main.vert", "main", nullptr);
    //      // Do stuff with compilation results.
    //      shaderc_result_release(result);
    //      // Once no more compilations are to happen.
    //      shaderc_compiler_release(compiler);

    // An opaque handle to an object that manages all compiler state.
    typedef struct shaderc_compiler* shaderc_compiler_t;

    // An opaque handle to an object that manages options to a single compilation
    // result.
    typedef struct shaderc_compile_options* shaderc_compile_options_t;

    // An include result.
    typedef struct shaderc_include_result {
        // The name of the source file.  The name should be fully resolved
        // in the sense that it should be a unique name in the context of the
        // includer.  For example, if the includer maps source names to files in
        // a filesystem, then this name should be the absolute path of the file.
        // For a failed inclusion, this string is empty.
        const char* source_name;
        size_t source_name_length;
        // The text contents of the source file in the normal case.
        // For a failed inclusion, this contains the error message.
        const char* content;
        size_t content_length;
        // User data to be passed along with this request.
        void* user_data;
    } shaderc_include_result;

    // The kinds of include requests.
    enum shaderc_include_type {
        shaderc_include_type_relative,  // E.g. #include "source"
        shaderc_include_type_standard   // E.g. #include <source>
    };

    // An includer callback type for mapping an #include request to an include
    // result.  The user_data parameter specifies the client context.  The
    // requested_source parameter specifies the name of the source being requested.
    // The type parameter specifies the kind of inclusion request being made.
    // The requesting_source parameter specifies the name of the source containing
    // the #include request.  The includer owns the result object and its contents,
    // and both must remain valid until the release callback is called on the result
    // object.
    typedef shaderc_include_result* (*shaderc_include_resolve_fn)(
        void* user_data, const char* requested_source, int type,
        const char* requesting_source, size_t include_depth);

    typedef void (*shaderc_include_result_release_fn)(
        void* user_data, shaderc_include_result* include_result);

    typedef enum {
        shaderc_target_env_vulkan,  // SPIR-V under Vulkan semantics
        shaderc_target_env_opengl,  // SPIR-V under OpenGL semantics
        // NOTE: SPIR-V code generation is not supported for shaders under OpenGL
        // compatibility profile.
        shaderc_target_env_opengl_compat,  // SPIR-V under OpenGL semantics,
                                           // including compatibility profile
                                           // functions
                                           shaderc_target_env_webgpu,         // Deprecated, SPIR-V under WebGPU
                                                                              // semantics
                                                                              shaderc_target_env_default = shaderc_target_env_vulkan
    } shaderc_target_env;

    typedef enum {
        // For Vulkan, use Vulkan's mapping of version numbers to integers.
        // See vulkan.h
        shaderc_env_version_vulkan_1_0 = ((1u << 22)),
        shaderc_env_version_vulkan_1_1 = ((1u << 22) | (1 << 12)),
        shaderc_env_version_vulkan_1_2 = ((1u << 22) | (2 << 12)),
        shaderc_env_version_vulkan_1_3 = ((1u << 22) | (3 << 12)),
        // For OpenGL, use the number from #version in shaders.
        // TODO(dneto): Currently no difference between OpenGL 4.5 and 4.6.
        // See glslang/Standalone/Standalone.cpp
        // TODO(dneto): Glslang doesn't accept a OpenGL client version of 460.
        shaderc_env_version_opengl_4_5 = 450,
        shaderc_env_version_webgpu,  // Deprecated, WebGPU env never defined versions
    } shaderc_env_version;

    // The known versions of SPIR-V.
    typedef enum {
        // Use the values used for word 1 of a SPIR-V binary:
        // - bits 24 to 31: zero
        // - bits 16 to 23: major version number
        // - bits 8 to 15: minor version number
        // - bits 0 to 7: zero
        shaderc_spirv_version_1_0 = 0x010000u,
        shaderc_spirv_version_1_1 = 0x010100u,
        shaderc_spirv_version_1_2 = 0x010200u,
        shaderc_spirv_version_1_3 = 0x010300u,
        shaderc_spirv_version_1_4 = 0x010400u,
        shaderc_spirv_version_1_5 = 0x010500u,
        shaderc_spirv_version_1_6 = 0x010600u
    } shaderc_spirv_version;

    typedef struct shaderc_compilation_result* shaderc_compilation_result_t;

    // Indicate the status of a compilation.
    typedef enum {
        shaderc_compilation_status_success = 0,
        shaderc_compilation_status_invalid_stage = 1,  // error stage deduction
        shaderc_compilation_status_compilation_error = 2,
        shaderc_compilation_status_internal_error = 3,  // unexpected failure
        shaderc_compilation_status_null_result_object = 4,
        shaderc_compilation_status_invalid_assembly = 5,
        shaderc_compilation_status_validation_error = 6,
        shaderc_compilation_status_transformation_error = 7,
        shaderc_compilation_status_configuration_error = 8,
    } shaderc_compilation_status;

    //This part of the header was modified by Kubagliko_PL
    #include <functional>

    extern std::function<shaderc_compiler_t(void)> shaderc_compiler_initialize;
    extern std::function<void(shaderc_compiler_t)> shaderc_compiler_release;

    extern std::function<shaderc_compile_options_t(void)> shaderc_compile_options_initialize;
    extern std::function<void(shaderc_compile_options_t options)> shaderc_compile_options_release;
    extern std::function<void(shaderc_compile_options_t options, const char* name, size_t name_length, const char* value, size_t value_length)> shaderc_compile_options_add_macro_definition;
    extern std::function<void(shaderc_compile_options_t options, shaderc_source_language lang)> shaderc_compile_options_set_source_language;
    extern std::function<void(shaderc_compile_options_t options)> shaderc_compile_options_set_generate_debug_info;
    extern std::function<void(shaderc_compile_options_t options, shaderc_optimization_level level)> shaderc_compile_options_set_optimization_level;
    extern std::function<void(shaderc_compile_options_t options, shaderc_target_env target, uint32_t version)> shaderc_compile_options_set_target_env;
    extern std::function<void(shaderc_compile_options_t options, shaderc_spirv_version version)> shaderc_compile_options_set_target_spirv;
    extern std::function<void(shaderc_compile_options_t options, shaderc_include_resolve_fn resolver, shaderc_include_result_release_fn result_releaser, void* user_data)> shaderc_compile_options_set_include_callbacks;

    extern std::function<shaderc_compilation_result_t(const shaderc_compiler_t compiler, const char* source_text,
        size_t source_text_size, shaderc_shader_kind shader_kind,
        const char* input_file_name, const char* entry_point_name,
        const shaderc_compile_options_t additional_options)> shaderc_compile_into_spv;

    extern std::function<void(shaderc_compilation_result_t result)> shaderc_result_release;
    extern std::function<size_t(const shaderc_compilation_result_t result)> shaderc_result_get_length;
    extern std::function<const char*(const shaderc_compilation_result_t result)> shaderc_result_get_bytes;
    extern std::function<shaderc_compilation_status(const shaderc_compilation_result_t result)> shaderc_result_get_compilation_status;
    extern std::function<const char*(const shaderc_compilation_result_t result)> shaderc_result_get_error_message;

#endif  // SHADERC_SHADERC_H_