#pragma once

#include "shaderCompiler.hpp"
#include "cache.hpp"
#include <gongen/core/exception.hpp>

namespace gent {
	struct Context {
		internal::IShaderCompiler *shaderCompiler;
		ShaderCompiler shaderCompilerAPI;
		gent::Cache cache;
	};

	void initContext(Context* context);

	class GEN_EXPORTED exception : public gen::Exception {
	protected:
		exception(const std::string& name, const std::string& message, const Exception* cause) : gen::Exception(name, message, cause) {}
	public: 
		exception(const std::string& message, const Exception* cause) : exception("GenTools Exception", message, cause) {}
		exception(const std::string& message) : exception(message, nullptr) {}
	};
}
