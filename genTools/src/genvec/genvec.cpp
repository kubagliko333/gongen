#include <genvec/genvec.hpp>

extern gen::WindowAPI* glfwCreateWindowAPI();
extern gen::GraphicsAPI* vulkanCreateGraphicsAPI();

gen::ShaderDesc loadShaderFromGCF(gen::GCFHandle& handle, const std::string shaderName, std::vector<std::pair<std::string, std::string>> macros, gen::ShaderType type);

genvec::Context context;

int main() {
	//Context Initialization
	context.windowAPI = glfwCreateWindowAPI();
	context.window = context.windowAPI->createWindow({ gen::WindowType::WINDOWED, 1280, 720 });
	context.graphicsAPI = vulkanCreateGraphicsAPI();
	context.graphicsAPI->init(context.windowAPI, { true, false });
	context.graphicsDevice = context.graphicsAPI->createDevice(nullptr, context.window, &context.swapchain); \

	gen::GCFHandle shadersGCF;
	gen::openGCF(shadersGCF, "shaders.gcf");
	gen::ShaderPtr imguiShader = context.graphicsDevice->createShader(loadShaderFromGCF(shadersGCF, "imgui", {}, context.graphicsAPI->getShaderType()));
	gen::freeGCF(shadersGCF);

	gen::ImGui_Init(context.window, context.graphicsDevice, imguiShader);

	//Main Loop
	while (context.window->isOpen()) {
		context.windowAPI->poolEvents();
		for (auto& e : context.windowAPI->events) {
			gen::ImGui_ProcessEvent(e);
		}

		context.commandBuffer = context.graphicsDevice->getCommandBuffer();
		
		ImGui::ShowAboutWindow();

		context.commandBuffer->beginRenderPass(context.swapchain->getRenderPass(context.commandBuffer.get()));
			gen::ImGui_Render(context.commandBuffer);
		context.commandBuffer->endRenderPass();
		context.commandBuffer->execute(0);
		context.commandBuffer->display(context.swapchain);
	}

	return 0;
}

void loadShaderFromGCF_variant(gen::ShaderDesc& desc, gen::GCFHandle& handle, tinyxml2::XMLElement* pNode, gen::ShaderType type) {
	tinyxml2::XMLElement* pSubNode = pNode->FirstChildElement();
	while (pSubNode) {
		bool validStage = false;
		gen::ShaderStage stage;
		if (!strcmp(pSubNode->Name(), "VertexShader")) {
			stage = gen::ShaderStage::VERTEX_SHADER;
			validStage = true;
		}
		else if (!strcmp(pSubNode->Name(), "HullShader")) {
			stage = gen::ShaderStage::HULL_SHADER;
			validStage = true;
		}
		else if (!strcmp(pSubNode->Name(), "DomainShader")) {
			stage = gen::ShaderStage::DOMAIN_SHADER;
			validStage = true;
		}
		else if (!strcmp(pSubNode->Name(), "GeometryShader")) {
			stage = gen::ShaderStage::GEOMETRY_SHADER;
			validStage = true;
		}
		else if (!strcmp(pSubNode->Name(), "PixelShader")) {
			stage = gen::ShaderStage::PIXEL_SHADER;
			validStage = true;
		}
		else if (!strcmp(pSubNode->Name(), "ComputeShader")) {
			stage = gen::ShaderStage::COMPUTE_SHADER;
			validStage = true;
		}

		if (validStage) {
			tinyxml2::XMLElement* pSubNode2 = pSubNode->FirstChildElement();
			while (pSubNode2) {
				if (!strcmp(pSubNode2->Name(), "SPIRV")) {
					if (type == gen::ShaderType::SPIRV) {
						gen::ShaderStage_t* stagePtr = &desc.stages[(size_t)stage];
						uint32_t dataSize = pSubNode2->UnsignedAttribute("dataSize");
						uint32_t uncompressedSize = pSubNode2->UnsignedAttribute("uncompressedSize");
						uint32_t dataOffset = pSubNode2->UnsignedAttribute("dataOffset");
						stagePtr->spirv.resize(uncompressedSize / sizeof(uint32_t));
						gen::gcfGetData(handle, stagePtr->spirv.data(), dataOffset, dataSize, uncompressedSize);

						tinyxml2::XMLElement* pSubNode3 = pSubNode2->FirstChildElement();
						while (pSubNode3) {
							if (!strcmp(pSubNode3->Name(), "Descriptor")) {
								uint32_t set = pSubNode3->UnsignedAttribute("set");
								uint32_t slot = pSubNode3->UnsignedAttribute("slot");
								uint32_t type = pSubNode3->UnsignedAttribute("type");

								stagePtr->metadata.descriptors[set][slot] = (gen::SPIRVDescriptorType)type;
							}
							pSubNode3 = pSubNode3->NextSiblingElement();
						}
					}
				}
				pSubNode2 = pSubNode2->NextSiblingElement();
			}
		}

		pSubNode = pSubNode->NextSiblingElement();
	}
}

gen::ShaderDesc loadShaderFromGCF_func(gen::GCFHandle& handle, tinyxml2::XMLElement* pNode, const std::string shaderName, std::vector<std::pair<std::string, std::string>> macros, gen::ShaderType type) {
	gen::ShaderDesc out;
	out.stageFlags = 0;
	tinyxml2::XMLElement* pSubNode = pNode->FirstChildElement();
	while (pSubNode) {
		if (!strcmp(pSubNode->Name(), "Node")) {
			if (!strcmp(pSubNode->Attribute("type"), "shader")) {
				if (!strcmp(pSubNode->Attribute("name"), shaderName.c_str())) {
					uint32_t stages = pSubNode->UnsignedAttribute("stages");
					out.stageFlags = stages;
					tinyxml2::XMLElement* pSubNode2 = pSubNode->FirstChildElement();
					while (pSubNode2) {
						if (!strcmp(pSubNode2->Name(), "Variant")) {
							tinyxml2::XMLElement* pSubNode3 = pSubNode2->FirstChildElement();

							std::vector<std::pair<std::string, std::string>> macros2;

							while (pSubNode3) {
								if (!strcmp(pSubNode3->Name(), "Macro")) {
									macros2.push_back(std::make_pair(pSubNode3->Attribute("name"), pSubNode3->Attribute("value")));
								}
								pSubNode3 = pSubNode3->NextSiblingElement();
							}

							bool variantSame = true;
							if (macros.size() != macros2.size()) {
								variantSame = false;
							}
							else {
								for (uint32_t i = 0; i < macros.size(); i++) {
									bool found = false;
									for (auto& m : macros2) {
										if (macros[i] == m) found = true;
									}
									if (!found) variantSame = false;
								}
							}
							if (variantSame) {
								loadShaderFromGCF_variant(out, handle, pSubNode2, type);
							}
						}
						pSubNode2 = pSubNode2->NextSiblingElement();
					}
				}
			}
		}
		pSubNode = pSubNode->NextSiblingElement();
	}
	return out;
}

gen::ShaderDesc loadShaderFromGCF(gen::GCFHandle& handle, const std::string shaderName, std::vector<std::pair<std::string, std::string>> macros, gen::ShaderType type) {
	tinyxml2::XMLElement* pRoot = handle.xmlDoc.FirstChildElement("GCF");

	return loadShaderFromGCF_func(handle, pRoot, shaderName, macros, type);
}
