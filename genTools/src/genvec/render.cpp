#include <genvec/render.hpp>

namespace genvec {
	mesh activeMesh;
	previewMesh activePreviewMesh;
}

void genvec::triangulate(genvec::object* object) {
	//Setting up verticies and lines for triangulation

	//Triangulation

	//Settuping vertex and index data
}

struct pointRemap {
	uint32_t vtx;
	uint32_t pointID;
};

void genvec::createPreviewMesh(genvec::object* object) {
	activePreviewMesh.indexData.clear();
	activePreviewMesh.vertexData.clear();

	std::vector<pointRemap> pointRemapTable;
	pointRemapTable.resize(object->points.size());

	for (uint32_t i = 0; i < object->points.size(); i++) {
		pointRemapTable.push_back({ i, (uint32_t)object->points[i].id });

		
	}
}

#include <imgui.h>

gen::PipelineDesc pipeline;
gen::ShaderPtr genvecShader;
gen::ShaderPtr genvecPreviewShader;

#include <genvec/genvec.hpp>

extern genvec::Context context;

void genvec::render(gen::CommandBufferPtr& commandBuffer, genvec::mesh* m, const gen::vectorPartInstanceData& p) {
	context.renderer.clearPartTypes();
	context.renderer.clear();
}

void genvec::render(gen::CommandBufferPtr& commandBuffer, genvec::previewMesh* m) {
	ImVec2 displaySize = ImGui::GetIO().DisplaySize;
	pipeline.scissor = { 0, 0, (uint32_t)displaySize.x, (uint32_t)displaySize.y };
	pipeline.viewport = { 0, 0, (uint32_t)displaySize.x, (uint32_t)displaySize.y };
	pipeline.shader = genvecPreviewShader;

	gen::BufferPtr uniformBuffer = commandBuffer->createDynamicBuffer(gen::BufferBindFlag::BUFFER_BIND_UNIFORM_BUFFER, sizeof(float) * 16);

	//commandBuffer->draw()
}
