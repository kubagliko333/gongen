#include <genvec/editor.hpp>
#include <genvec/genvec.hpp>

extern genvec::Context context;

genvec::point* genvec::object::getPoint(uint64_t id) {
	for (auto& p : context.root.points) {
		if (p.id = id) return &p;
	}
	return nullptr;
}

uint32_t nextPointID = 0;
genvec::point* lastPoint;

genvec::LineType activeLineType = genvec::LineType::LINE;

void editorMain() {
	/*if (ImGui::Begin("Editor")) {
		ImGui::Text("L - Line Mode");
		ImGui::Text("Q - Quadratic Mode");
		ImGui::Text("Right Mouse - Add Point");
	}
	ImGui::End();

	if (ImGui::IsKeyPressed(ImGuiKey_L, false)) {
		activeLineType = genvec::LineType::LINE;
	}
	if (ImGui::IsKeyPressed(ImGuiKey_Q, false)) {
		activeLineType = genvec::LineType::QUADRATIC_SPLINE;
	}
	if (ImGui::GetIO().MouseClicked[1]) {
		ImVec2 pointPos = ImGui::GetIO().MousePos;

		genvec::point p;
		p.id = nextPointID;
		p.x = pointPos.x;
		p.y = pointPos.y;

		context.root.points.push_back(p);

		if (lastPoint) {
			genvec::line l;
			l.type = activeLineType;
			l.point0 = lastPoint->id;
			l.point1 = p.id;
		}

		lastPoint = context.root.getPoint(nextPointID);
		nextPointID++;
	}*/
	genvec::triangulate(&context.root);
	genvec::createPreviewMesh(&context.root);

	


}
