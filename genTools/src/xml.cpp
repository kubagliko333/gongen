#include <genTools.hpp>
#include <tinyxml2.h>
#include <string.h>
#include <gongen/gcf/gcf.hpp>

extern gent::Context context;

bool strEqual(const char* c1, const char* c2) {
	uint32_t l1 = strlen(c1);
	uint32_t l2 = strlen(c2);
	if (l1 != l2) return false;
	for (uint32_t i = 0; i < l1; i++) {
		if (c1[i] != c2[i]) return false;
	}
	return true;
}

void xml_GCF(tinyxml2::XMLElement* pNode);

gen::CompressionMode getCompressioMode(tinyxml2::XMLElement* pNode) {
	if (pNode->Attribute("compression", "zstd")) return gen::CompressionMode::ZSTD;
	else return gen::CompressionMode::UNCOMPRESSED;
}

void processXMLTaskList(gent::Context *context, tinyxml2::XMLDocument &xmlDoc) {
	tinyxml2::XMLElement* pRoot = xmlDoc.FirstChildElement("GenTools");
	if (pRoot) {
		tinyxml2::XMLElement* pNode = pRoot->FirstChildElement();
		while (pNode) {
			if (strEqual("GCF", pNode->Name())) {
				xml_GCF(pNode);
			}
			pNode = pNode->NextSiblingElement();
		}
	}
}

void xml_Shader(gen::GCFHandle &handle, tinyxml2::XMLElement *pGCF_Node, tinyxml2::XMLElement* pNode);
void xml_File(gen::GCFHandle& handle, tinyxml2::XMLElement* pGCF_Node, tinyxml2::XMLElement* pNode);
void xml_Texture(gen::GCFHandle& handle, tinyxml2::XMLElement* pGCF_Node, tinyxml2::XMLElement* pNode);
void xml_Metadata(tinyxml2::XMLElement* pGCF_Node, tinyxml2::XMLElement* pNode);

std::string str(const char* cstr) {
	if (!cstr) return "";
	return std::string(cstr);
}

void xml_GCF(tinyxml2::XMLElement* pNode) {
	std::string gcf_namespace = str(pNode->Attribute("namespace"));
	std::string gcf_name = str(pNode->Attribute("name"));
	std::string gcf_file = str(pNode->Attribute("file"));
	gen::CompressionMode compressionMode = getCompressioMode(pNode);

	gen::GCFHandle handle;
	tinyxml2::XMLElement *pGCF = handle.xmlDoc.NewElement("GCF");
	if (!gcf_namespace.empty()) pGCF->SetAttribute("namespace", gcf_namespace.c_str());
	if (!gcf_name.empty()) pGCF->SetAttribute("name", gcf_name.c_str());

	tinyxml2::XMLElement* pSubNode = pNode->FirstChildElement();
	while (pSubNode) {
		if (strEqual("Shader", pSubNode->Name())) {
			tinyxml2::XMLElement* pGCF_Node = pGCF->InsertNewChildElement("Node");
			pGCF_Node->SetAttribute("type", "shader");
			xml_Shader(handle, pGCF_Node, pSubNode);
		}
		if (strEqual("File", pSubNode->Name())) {
			tinyxml2::XMLElement* pGCF_Node = pGCF->InsertNewChildElement("Node");
			pGCF_Node->SetAttribute("type", "file");
			xml_File(handle, pGCF_Node, pSubNode);
		}
		if (strEqual("Texture", pSubNode->Name())) {
			tinyxml2::XMLElement* pGCF_Node = pGCF->InsertNewChildElement("Node");
			pGCF_Node->SetAttribute("type", "texture");
			xml_Texture(handle, pGCF_Node, pSubNode);
		}

		pSubNode = pSubNode->NextSiblingElement();
	}

	handle.xmlDoc.InsertEndChild(pGCF);

	if (compressionMode == gen::CompressionMode::ZSTD) handle.header.header.xmlFlags |= gen::GCFXMLFlag::GCF_XML_ZSTD_COMPRESSION;

	gen::saveGCF(handle, gcf_file);
}

void xml_Shader(gen::GCFHandle& handle, tinyxml2::XMLElement* pGCF_Node, tinyxml2::XMLElement* pNode) {
	std::string name = str(pNode->Attribute("name"));
	std::string stages = str(pNode->Attribute("stages"));
	std::string file = str(pNode->Attribute("file"));
	gen::CompressionMode compressionMode = getCompressioMode(pNode);
	uint32_t shaderFlags = 0;

	gent::Shader* shader = context.shaderCompilerAPI.addShader(name, file, shaderFlags, stages);

	pGCF_Node->SetAttribute("name", name.c_str());
	pGCF_Node->SetAttribute("stages", shader->stageFlags);

	tinyxml2::XMLElement* pSubNode = pNode->FirstChildElement();
	while (pSubNode) {
		if (strEqual("Metadata", pSubNode->Name())) {
			xml_Metadata(pGCF_Node, pSubNode);
		}
		if (strEqual("Variant", pSubNode->Name())) {
			std::vector<std::pair<std::string, std::string>> macros;

			tinyxml2::XMLElement* pSubNode2 = pSubNode->FirstChildElement();
			while (pSubNode2) {
				macros.push_back(std::make_pair(pSubNode2->Name(), pSubNode2->GetText()));

				pSubNode2 = pSubNode2->NextSiblingElement();
			}

			uint32_t types = 0;
			if (pNode->BoolAttribute("spirv")) types |= (uint32_t)gen::ShaderType::SPIRV;
			if (pNode->BoolAttribute("glsl")) types |= (uint32_t)gen::ShaderType::GLSL330;
			if (pNode->BoolAttribute("elsl")) types |= (uint32_t)gen::ShaderType::ELSL300;
			if (pNode ->BoolAttribute("dxbc")) types |= (uint32_t)gen::ShaderType::DXBC;

			gent::ShaderVariant* variant = context.shaderCompilerAPI.addShaderVariant(shader, macros, types);

			tinyxml2::XMLElement* pGCF_Variant = pGCF_Node->InsertNewChildElement("Variant");

			for (auto& m : variant->macros) {
				tinyxml2::XMLElement* pGCF_Macro = pGCF_Variant->InsertNewChildElement("Macro");
				pGCF_Macro->SetAttribute("name", m.first.c_str());
				pGCF_Macro->SetAttribute("value", m.second.c_str());
			}

			for (uint32_t i = 0; i < 31; i++) {
				if (variant->stageFlags & (1 << i)) {
					std::string stageName = "";
					if ((1 << i) == gen::ShaderStageFlag::VERTEX_SHADER) stageName = "VertexShader";
					if ((1 << i) == gen::ShaderStageFlag::HULL_SHADER) stageName = "HullShader";
					if ((1 << i) == gen::ShaderStageFlag::DOMAIN_SHADER) stageName = "DomainShader";
					if ((1 << i) == gen::ShaderStageFlag::GEOMETRY_SHADER) stageName = "GeometryShader";
					if ((1 << i) == gen::ShaderStageFlag::PIXEL_SHADER) stageName = "PixelShader";
					if ((1 << i) == gen::ShaderStageFlag::COMPUTE_SHADER) stageName = "ComputeShader";

					tinyxml2::XMLElement* pGCF_Stage = pGCF_Variant->InsertNewChildElement(stageName.c_str());

					gen::ShaderMetadata metadata = variant->stages[i].metadata;
					for (uint32_t set = 0; set < gen::MAX_DESCRIPTOR_SETS; set++) {
						for (uint32_t slot = 0; slot < gen::MAX_DESCRIPTOR_SLOTS; slot++) {
							gen::SPIRVDescriptorType descriptorType = metadata.descriptors[set][slot];
							if (descriptorType != gen::SPIRVDescriptorType::INVALID) {
								tinyxml2::XMLElement* pGCF_Descriptor = pGCF_Stage->InsertNewChildElement("Descriptor");
								pGCF_Descriptor->SetAttribute("set", set);
								pGCF_Descriptor->SetAttribute("slot", slot);
								pGCF_Descriptor->SetAttribute("type", (size_t)descriptorType);
							}
						}
					}

					for (uint32_t set = 0; set < gen::MAX_DESCRIPTOR_SETS; set++) {
						for (uint32_t slot = 0; slot < gen::MAX_DESCRIPTOR_SLOTS; slot++) {
							uint32_t remapped = metadata.remapTable[set][slot];
							if (remapped < gen::INVALID_SHADER_SLOT) {
								tinyxml2::XMLElement* pGCF_Node = pGCF_Stage->InsertNewChildElement("Remapped");
								pGCF_Node->SetAttribute("set", set);
								pGCF_Node->SetAttribute("slot", slot);
								pGCF_Node->SetAttribute("remapped", remapped);
							}

							uint32_t spirvIndex = metadata.descriptorSPIRVIndexes[set][slot];
							if (spirvIndex != UINT32_MAX) {
								tinyxml2::XMLElement* pGCF_Node = pGCF_Stage->InsertNewChildElement("SPIRVIndex");
								pGCF_Node->SetAttribute("set", set);
								pGCF_Node->SetAttribute("slot", slot);
								pGCF_Node->SetAttribute("index", spirvIndex);
							}
						}
					}

					for (uint32_t slot = 0; slot < gen::MAX_DESCRIPTOR_SLOTS; slot++) {
						uint32_t imageBinding = metadata.textureCombiningTable[slot].imageBinding;
						uint32_t samplerBinding = metadata.textureCombiningTable[slot].samplerBinding;
						uint32_t combinedIndex = metadata.textureCombiningTable[slot].spirvIndex;
						if (imageBinding < UINT8_MAX) {
							tinyxml2::XMLElement* pGCF_Node = pGCF_Stage->InsertNewChildElement("CombinedImageSampler");
							pGCF_Node->SetAttribute("slot", slot);
							pGCF_Node->SetAttribute("imageBinding", imageBinding);
							pGCF_Node->SetAttribute("samplerBinding", samplerBinding);
							pGCF_Node->SetAttribute("combinedIndex", combinedIndex);
						}
					}

					if (variant->shaderTypes & (uint32_t)gen::ShaderType::SPIRV) {
						tinyxml2::XMLElement* pGCF_SPIRV = pGCF_Stage->InsertNewChildElement("SPIRV");
						uint64_t dataSize = variant->stages[i].spirv.size() * sizeof(uint32_t);
						uint64_t compressedSize;
						uint64_t dataOffset = gen::gcfAppendData(handle, variant->stages[i].spirv.data(), dataSize, &compressedSize, compressionMode);
						pGCF_SPIRV->SetAttribute("dataCompression", (uint32_t)compressionMode);
						pGCF_SPIRV->SetAttribute("dataSize", compressedSize);
						pGCF_SPIRV->SetAttribute("uncompressedSize", dataSize);
						pGCF_SPIRV->SetAttribute("dataOffset", dataOffset);
					}
					if (variant->shaderTypes & (uint32_t)gen::ShaderType::GLSL330) {
						tinyxml2::XMLElement* pGCF_GLSL = pGCF_Stage->InsertNewChildElement("GLSL");
						uint64_t dataSize = variant->stages[i].glsl.size();
						uint64_t compressedSize;
						uint64_t dataOffset = gen::gcfAppendData(handle, variant->stages[i].glsl.data(), dataSize, &compressedSize, compressionMode);
						pGCF_GLSL->SetAttribute("dataCompression", (uint32_t)compressionMode);
						pGCF_GLSL->SetAttribute("dataSize", compressedSize);
						pGCF_GLSL->SetAttribute("uncompressedSize", dataSize);
						pGCF_GLSL->SetAttribute("dataOffset", dataOffset);
					}
					if (variant->shaderTypes & (uint32_t)gen::ShaderType::ELSL300) {
						tinyxml2::XMLElement* pGCF_GLSL = pGCF_Stage->InsertNewChildElement("ELSL");
						uint64_t dataSize = variant->stages[i].glsl.size();
						uint64_t compressedSize;
						uint64_t dataOffset = gen::gcfAppendData(handle, variant->stages[i].glsl.data(), dataSize, &compressedSize, compressionMode);
						pGCF_GLSL->SetAttribute("dataCompression", (uint32_t)compressionMode);
						pGCF_GLSL->SetAttribute("dataSize", compressedSize);
						pGCF_GLSL->SetAttribute("uncompressedSize", dataSize);
						pGCF_GLSL->SetAttribute("dataOffset", dataOffset);
					}
				}
			}
		}

		pSubNode = pSubNode->NextSiblingElement();
	}
}

void xml_File(gen::GCFHandle& handle, tinyxml2::XMLElement* pGCF_Node, tinyxml2::XMLElement* pNode) {
	std::string name = str(pNode->Attribute("name"));
	std::string type = str(pNode->Attribute("type"));
	std::string file = str(pNode->Attribute("file"));
	gen::CompressionMode compressionMode = getCompressioMode(pNode);

	pGCF_Node->SetAttribute("name", name.c_str());
	pGCF_Node->SetAttribute("fileType", type.c_str());

	tinyxml2::XMLElement* pSubNode = pNode->FirstChildElement();
	while (pSubNode) {
		if (strEqual("Metadata", pSubNode->Name())) {
			xml_Metadata(pGCF_Node, pSubNode);
		}

		pSubNode = pSubNode->NextSiblingElement();
	}

	gen::FileHandle fhandle(file.c_str(), gen::FileOpenMode::READ);
	if (fhandle.isOpen()) {
		uint64_t dataSize = fhandle.size();
		uint8_t* fileData = (uint8_t*)malloc(dataSize);
		fhandle.read(dataSize, fileData);
		uint64_t compressedSize;
		uint64_t dataOffset = gen::gcfAppendData(handle, fileData, dataSize, &compressedSize, compressionMode);
		pGCF_Node->SetAttribute("dataCompression", (uint32_t)compressionMode);
		pGCF_Node->SetAttribute("dataSize", compressedSize);
		pGCF_Node->SetAttribute("uncompressedSize", dataSize);
		pGCF_Node->SetAttribute("dataOffset", dataOffset);

		free(fileData);
	}
}

#include <texture.hpp>

void xml_Texture(gen::GCFHandle& handle, tinyxml2::XMLElement* pGCF_Node, tinyxml2::XMLElement* pNode) {
	std::string name = str(pNode->Attribute("name"));
	uint64_t layers = pNode->Unsigned64Attribute("layers", 1);
	std::string type_str = str(pNode->Attribute("type"));

	gent::TextureType type = gent::TextureType::TEXTURE_2D;
	if (strEqual(type_str.c_str(), "2d")) type = gent::TextureType::TEXTURE_2D;
	else if (strEqual(type_str.c_str(), "packed_2d")) type = gent::TextureType::TEXTURE_2D_PACKED;

	gent::Texture texture(type, pNode->BoolAttribute("mipmaps", false), name, layers, pNode->Unsigned64Attribute("size"));

	pGCF_Node->SetAttribute("name", name.c_str());

	tinyxml2::XMLElement* pSubNode = pNode->FirstChildElement();
	while (pSubNode) {
		if (strEqual("Metadata", pSubNode->Name())) {
			xml_Metadata(pGCF_Node, pSubNode);
		}
		else if (strEqual("File", pSubNode->Name())) {
			uint64_t layer = pSubNode->Unsigned64Attribute("layer", 0);
			texture.addSubTexture(str(pSubNode->GetText()), layer, str(pSubNode->Attribute("name")));
		}
		else if (strEqual("Variant", pSubNode->Name())) {
			std::string format_str = str(pSubNode->Attribute("format"));
			gen::CompressionMode compressionMode = getCompressioMode(pSubNode);

			gent::TextureFormat format = gent::TextureFormat::PNG;

			if (strEqual(format_str.c_str(), "bc1")) format = gent::TextureFormat::BC1;
			else if (strEqual(format_str.c_str(), "bc3")) format = gent::TextureFormat::BC3;
			else if (strEqual(format_str.c_str(), "bc4")) format = gent::TextureFormat::BC4;
			else if (strEqual(format_str.c_str(), "bc5")) format = gent::TextureFormat::BC5;
			else if (strEqual(format_str.c_str(), "bc7")) format = gent::TextureFormat::BC7;

			texture.addVariant(format, compressionMode);
		}

		pSubNode = pSubNode->NextSiblingElement();
	}

	texture.save(handle, pGCF_Node);
}

void xml_Metadata(tinyxml2::XMLElement* pGCF_Node, tinyxml2::XMLElement* pNode) {
	tinyxml2::XMLElement* pGCF_Meta = pGCF_Node->InsertNewChildElement("Metadata");
	pGCF_Meta->InsertEndChild(pNode->DeepClone(pGCF_Meta->GetDocument()));
}
