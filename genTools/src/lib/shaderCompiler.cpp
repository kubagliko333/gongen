#include <shaderCompiler.hpp>
#include <genTools.hpp>
#include <stb_sprintf.h>

gent::Shader* gent::ShaderCompiler::addShader(const std::string& name, const std::string& filename, uint32_t shaderFlags, const std::string& stages) {
	Shader *shader = new Shader();
	shader->name = name;
	shader->file = filename;
	shader->shaderFlags = shaderFlags;
	shader->stageFlags = 0;
	for (auto& c : stages) {
		if (c == 'v') shader->stageFlags |= gen::ShaderStageFlag::VERTEX_SHADER;
		else if (c == 'h') shader->stageFlags |= gen::ShaderStageFlag::HULL_SHADER;
		else if (c == 'd') shader->stageFlags |= gen::ShaderStageFlag::DOMAIN_SHADER;
		else if (c == 'g') shader->stageFlags |= gen::ShaderStageFlag::GEOMETRY_SHADER;
		else if (c == 'p') shader->stageFlags |= gen::ShaderStageFlag::PIXEL_SHADER;
		else if (c == 'c') shader->stageFlags |= gen::ShaderStageFlag::COMPUTE_SHADER;
	}

	shaders.push_back(shader);
	return shaders.back();
}

extern gent::Context context;

extern std::string loadTextFile(const std::string& filename);

gent::ShaderVariant* gent::ShaderCompiler::addShaderVariant(Shader* shader, std::vector<std::pair<std::string, std::string>> macros, uint32_t types) {
	gent::ShaderVariant *variant = new gent::ShaderVariant();
	variant->stageFlags = shader->stageFlags;
	variant->macros = macros;

	uint32_t types2 = (types | (uint32_t)gen::ShaderType::SPIRV);

	variant->shaderTypes = types2;

	for (uint32_t i = 0; i < 31; i++) {
		if (variant->stageFlags & (1 << i)) {
			for (uint32_t i2 = 0; i2 < 31; i2++) {
				if (types2 & (1 << i2)) {
					std::string code = loadTextFile(shader->file);

					char buffer[128];
					stbsp_sprintf(buffer, "%u_%u", i, i2);

					std::string cacheName = shader->name + "_" + std::string(buffer);
					std::string cacheHashedString = code;
					for (auto& m : macros) {
						cacheName += m.first;
						cacheName += m.second;

						cacheHashedString += m.first;
						cacheHashedString += m.second;
					}
					gen::Hash128 cacheHash = gen::hash128_stable((void*)cacheHashedString.data(), cacheHashedString.size());
					bool entryCached = context.cache.checkEntry(cacheName, cacheHash);
					uint64_t cachedDataSize = 0;
					void* cachedData = context.cache.getEntryData(cacheName, cacheHash, cachedDataSize);

					if ((1 << i2) == (uint32_t)gen::ShaderType::SPIRV) {
						if (entryCached) {
							variant->stages[i].spirv.resize(cachedDataSize / sizeof(uint32_t));
							memcpy(variant->stages[i].spirv.data(), cachedData, cachedDataSize);
						}
						else {
							variant->stages[i].spirv = context.shaderCompiler->compileSPIRV(code, shader->file, (gen::ShaderStage)i, macros);
							context.cache.addEntry(cacheName, cacheHash, variant->stages[i].spirv.data(), variant->stages[i].spirv.size() * sizeof(uint32_t));
						}
						variant->stages[i].metadata = context.shaderCompiler->reflectSPIRV(variant->stages[i].spirv);
					}
					else if (((1 << i2) == (uint32_t)gen::ShaderType::GLSL330) || ((1 << i2) == (uint32_t)gen::ShaderType::ELSL300)) {
						if (entryCached) {
							variant->stages[i].glsl.resize(cachedDataSize);
							memcpy(variant->stages[i].glsl.data(), cachedData, cachedDataSize);
							context.shaderCompiler->compileGLSL((1 << i), variant, &variant->stages[i].metadata, code, shader->file, variant->stages[i].spirv, true, (gen::ShaderType)(1 << i2));
						}
						else {
							variant->stages[i].glsl = context.shaderCompiler->compileGLSL((1 << i), variant, &variant->stages[i].metadata, code, shader->file, variant->stages[i].spirv, false, (gen::ShaderType)(1 << i2));
							context.cache.addEntry(cacheName, cacheHash, variant->stages[i].glsl.data(), variant->stages[i].glsl.size());
						}
					}

					free(cachedData);
				}
			}
		}
	}

	shader->variants.push_back(variant);
	return shader->variants.back();
}
