#include <texture.hpp>
#define STB_IMAGE_IMPLEMENTATION
#define STBI_ONLY_PNG
#define STBI_ONLY_PSD
#define STBI_WINDOWS_UTF8
#include <stb_image.h>
#define STB_IMAGE_WRITE_IMPLEMENTATION
#define STBIW_WINDOWS_UTF8
#include <stb_image_write.h>
#define STB_IMAGE_RESIZE_IMPLEMENTATION
#include <stb_image_resize.h>
#define STB_RECT_PACK_IMPLEMENTATION
#include <stb_rect_pack.h>
#include <bc7enc.h>
#include <malloc.h>
#define RGBCX_IMPLEMENTATION
#include <rgbcx.h>

gent::Texture::Texture(TextureType type, bool mipmaps, std::string name, uint32_t layers, uint32_t size) {
	if (type == TextureType::TEXTURE_2D) packed = false;
	else if (type == TextureType::TEXTURE_2D_PACKED) packed = true;

	this->mipmaps = mipmaps;
	this->layers = layers;
	this->name = name;
	this->packedSize = size;
}

gent::Texture::~Texture() {
	for (auto& s : subTextures) {
		if (s.pixelData) free(s.pixelData);
	}
	for (auto& v : variants) {
		if (v.data) free(v.data);
	}
}

#include <genTools.hpp>

void gent::Texture::addSubTexture(const std::string& filepath, uint32_t layer, const std::string& name) {
	int w = 0, h = 0, comp = 0;
	int comp2;
	stbi_info(filepath.c_str(), &w, &h, &comp);
	if (comp == 3) comp2 = 4;
	else comp2 = comp;
	if ((w == 0) || (h == 0)) {
		throw gent::exception("Can't load texture file: " + filepath);
	}

	void *pixelData = stbi_load(filepath.c_str(), &w, &h, &comp, comp2);
	
	if (!pixelData) {
		throw gent::exception("Can't load texture file: " + filepath);
	}

	addSubTexture(pixelData, w, h, comp2, layer, name);
}

void gent::Texture::addSubTexture(void* pixelData, uint32_t w, uint32_t h, uint32_t comp, uint32_t layer, const std::string& name) {
	gent::SubTexture s;
	s.w = w;
	s.h = h;
	s.layer = layer;
	s.comp = comp;
	s.name = name;
	s.pixelData = pixelData;

	uint64_t pixelDataSize = s.w * s.h * s.comp;
	s.hash = gen::hash128_stable(s.pixelData, pixelDataSize);

	subTextures.push_back(s);
}

void gent::Texture::addVariant(TextureFormat format, gen::CompressionMode compression) {
	gent::TextureVariant variant;
	variant.format = format;
	variant.compression = compression;
	
	variants.push_back(variant);
}

#include <malloc.h>

void gent::Texture::pack(std::vector<gent::SubTexture>& out) {
	stbrp_context ctx;
	stbrp_node* nodes = (stbrp_node*)malloc(packedSize * sizeof(stbrp_node));
	stbrp_init_target(&ctx, packedSize, packedSize, nodes, packedSize);

	uint32_t layer = 0;
	uint32_t comp = 4;

	std::vector<stbrp_rect> rects;

	for (uint32_t i = 0; i < subTextures.size(); i++) {
		gent::SubTexture* s = &subTextures[i];
		stbrp_rect r;
		r.w = s->w;
		r.h = s->h;
		r.was_packed = false;
		r.id = i;

		rects.push_back(r);

		if (s->comp) comp = s->comp;
	}

	while (true) {
		gent::SubTexture s;
		s.w = this->packedSize;
		s.h = this->packedSize;
		s.comp = comp;
		s.layer = layer;
		s.name = name;
		
		uint64_t pixelDataSize = s.w * s.h * s.comp;
		s.pixelData = calloc(pixelDataSize, 1);

		stbrp_pack_rects(&ctx, rects.data(), rects.size());

		for (int32_t i = 0; i < rects.size(); i++) {
			if (rects[i].was_packed) {
				uint32_t x = rects[i].x;
				uint32_t subTextureID = rects[i].id;

				for (uint32_t y2 = 0; y2 < rects[i].h; y2++) {
					uint32_t y = rects[i].y + y2;

					uint32_t o = ((x + (y * packedSize)) * s.comp);
					uint32_t size = rects[i].w * s.comp;

					uint32_t o2 = y2 * rects[i].w * s.comp;

					memcpy(&((uint8_t*)s.pixelData)[o], &((uint8_t*)subTextures[subTextureID].pixelData)[o2], size);
				}

				gent::PackedTexture p;
				p.x = rects[i].x;
				p.y = rects[i].y;
				p.w = rects[i].w;
				p.h = rects[i].h;
				p.layer = layer;
				p.name = subTextures[subTextureID].name;
				packedTextures.push_back(p);

				rects.erase(rects.begin() + i);
				i--;
			}
		}
		rects.clear();

		s.hash = gen::hash128_stable(s.pixelData, pixelDataSize);

		out.push_back(s);

		bool allPacked = true;
		for (uint32_t i = 0; i < rects.size(); i++) {
			if (!rects[i].was_packed) allPacked = false;
		}

		if (allPacked) break;
		layer++;
	}

	free(nodes);
}

std::atomic<bool> bc1to5_initialized = false;
std::atomic<bool> bc7_initialized = false;

#include <math.h>

extern gent::Context context;

void gent::Texture::processSubTexture(gent::TextureVariant *variant, gent::SubTexture* subTexture, tinyxml2::XMLElement* pNode, gen::GCFHandle& handle, uint32_t level, gen::Hash128 textureHash) {
	if (variant->format != gent::TextureFormat::PNG) {
		if ((subTexture->w % 4) || (subTexture->h % 4)) {
			throw gent::exception("Texture size not multiple of 4: " + name + "::" + subTexture->name + "::" + std::to_string(subTexture->layer));
		}
	}

	std::string cacheName = this->name + subTexture->name + "_";
	cacheName += std::to_string(level);
	cacheName += "f";
	cacheName += std::to_string((uint32_t)variant->format);
	cacheName += "l";
	cacheName += std::to_string(subTexture->layer);

	if (!bc1to5_initialized) {
		rgbcx::init();
		bc1to5_initialized = true;
	}
	if (!bc7_initialized) {
		bc7enc_compress_block_init();
		bc7_initialized = true;
	}

	bc7enc_compress_block_params bc7_params;
	bc7enc_compress_block_params_init(&bc7_params);

	TextureFormat format = variant->format;
	
	uint64_t dataOffset;
	uint64_t dataSize;
	uint64_t uncompressedSize = 0;

	pNode->SetAttribute("dataCompression", (uint32_t)variant->compression);

	if (!context.cache.checkEntry(cacheName, textureHash)) {
		uint32_t levelW = (subTexture->w) / powl(2, level);
		uint32_t levelH = (subTexture->h) / powl(2, level);
		if (levelW == 0) levelW = 1;
		if (levelH == 0) levelH = 1;

		void* levelData = nullptr;
		if (level == 0) {
			levelData = subTexture->pixelData;
		}
		else {
			levelData = malloc(levelW * levelH * subTexture->comp);

			stbir_resize_uint8((const uint8_t*)subTexture->pixelData, subTexture->w, subTexture->h, 0, (uint8_t*)levelData, levelW, levelH, 0, 4);
		}

		if (format == gent::TextureFormat::PNG) {
			int32_t pngSize;
			void* pngData = stbi_write_png_to_mem((uint8_t*)levelData, levelW * subTexture->comp, levelW, levelH, subTexture->comp, &pngSize);
			context.cache.addEntry(cacheName, textureHash, pngData, pngSize);
			dataOffset = gen::gcfAppendData(handle, pngData, pngSize, &dataSize, variant->compression);
			free(pngData);

			uncompressedSize = pngSize;
		}
		else {
			uint64_t blockSize = 0;
			if ((format == gent::TextureFormat::BC1) || (format == gent::TextureFormat::BC4)) {
				blockSize = 8;
			}
			else {
				blockSize = 16;
			}

			uint8_t blockData[64];

			uint32_t blocks_w = levelW / 4;
			uint32_t blocks_h = levelH / 4;

			uncompressedSize = blocks_w * blocks_h * blockSize;
			variant->data = malloc(uncompressedSize);

			uint32_t rowSize = subTexture->comp * 4;

			for (uint32_t h = 0; h < blocks_h; h++) {
				for (uint32_t w = 0; w < blocks_w; w++) {
					uint32_t block_i = w + (h * blocks_w);

					for (uint32_t row = 0; row < 4; row++) {
						uint32_t x = w * 4;
						uint32_t y = h * 4;
						y += row;

						uint32_t i = x + (y * levelW);

						memcpy(&blockData[row * rowSize], &((uint8_t*)subTexture->pixelData)[i], rowSize);
					}

					uint8_t* blockDst = &((uint8_t*)variant->data)[block_i * blockSize];

					if (format == gent::TextureFormat::BC1) {
						rgbcx::encode_bc1(10, blockDst, blockData, false, false);
					}
					if (format == gent::TextureFormat::BC3) {
						rgbcx::encode_bc3(10, blockDst, blockData);
					}
					if (format == gent::TextureFormat::BC4) {
						rgbcx::encode_bc4(blockDst, blockData);
					}
					if (format == gent::TextureFormat::BC5) {
						rgbcx::encode_bc5(blockDst, blockData);
					}
					if (format == gent::TextureFormat::BC7) {
						bc7enc_compress_block(blockDst, blockData, &bc7_params);
					}
				}
			}

			context.cache.addEntry(cacheName, textureHash, variant->data, uncompressedSize);
			dataOffset = gen::gcfAppendData(handle, variant->data, uncompressedSize, &dataSize, variant->compression);

			free(variant->data);
			variant->data = nullptr;
		}

		if (level != 0) {
			free(levelData);
		}
	}
	else {
		void* data = context.cache.getEntryData(cacheName, textureHash, uncompressedSize);

		dataOffset = gen::gcfAppendData(handle, data, uncompressedSize, &dataSize, variant->compression);

		free(data);
	}

	

	pNode->SetAttribute("dataOffset", dataOffset);
	pNode->SetAttribute("dataSize", dataSize);
	pNode->SetAttribute("uncompressedSize", uncompressedSize);
}

uint64_t getAmountOfTextureMipLevels(uint32_t width, uint32_t height, bool compressed) {
		uint32_t levels = 0;
		uint32_t minW = 1;
		uint32_t minH = 1;
		if (compressed) {
			minW = 4;
			minH = 4;
		}
		uint32_t w = width;
		uint32_t h = height;

		while (true) {
			levels++;
			if ((w <= minW) && (h <= minH)) break;
			w = w / 2;
			h = h / 2;
		}

		return levels;
}

void gent::Texture::save(gen::GCFHandle& handle, tinyxml2::XMLElement* pGCF_Node) {
	if (packed) {
		std::vector<gent::SubTexture> packedSubTextures;

		pack(packedSubTextures);

		for (auto& s : subTextures) {
			if (s.pixelData) free(s.pixelData);
		}
		subTextures.clear();
		subTextures = packedSubTextures;
	}

	if (packed) {
		for (auto& p : packedTextures) {
			tinyxml2::XMLElement* pPackedTexture = pGCF_Node->InsertNewChildElement("PackedTexture");
			pPackedTexture->SetAttribute("x", p.x);
			pPackedTexture->SetAttribute("y", p.y);
			pPackedTexture->SetAttribute("layer", p.layer);
			pPackedTexture->SetAttribute("name", p.name.c_str());
		}
	}

	for (auto& v : variants) {
		tinyxml2::XMLElement* pVariant = pGCF_Node->InsertNewChildElement("Variant");

		gen::GPUFormat format = gen::GPUFormat::RGBA8_UNORM;

		if (v.format == gent::TextureFormat::PNG) {
			pVariant->SetAttribute("compressed", 0);

			pVariant->SetAttribute("format", "png");
		}
		else {
			pVariant->SetAttribute("compressed", 1);

			if (v.format == gent::TextureFormat::BC1) format = gen::GPUFormat::BC1_UNORM;
			else if (v.format == gent::TextureFormat::BC3) format = gen::GPUFormat::BC3_UNORM;
			else if (v.format == gent::TextureFormat::BC4) format = gen::GPUFormat::BC4_UNORM;
			else if (v.format == gent::TextureFormat::BC5) format = gen::GPUFormat::BC5_UNORM;
			else if (v.format == gent::TextureFormat::BC7) format = gen::GPUFormat::BC7_UNORM;

			pVariant->SetAttribute("format", (uint32_t)format);
		}

		for (auto& s : subTextures) {
			uint32_t mipmaps = 1;
			if (this->mipmaps) {
				if (v.format == gent::TextureFormat::PNG) {
					mipmaps = getAmountOfTextureMipLevels(s.w, s.h, false);
				}
				else {
					mipmaps = getAmountOfTextureMipLevels(s.w, s.h, true);
				}
			}

			tinyxml2::XMLElement* pSubTexture = pVariant->InsertNewChildElement("SubTexture");
			pSubTexture->SetAttribute("name", s.name.c_str());
			pSubTexture->SetAttribute("layer", s.layer);
			
			for (uint32_t i = 0; i < mipmaps; i++) {
				tinyxml2::XMLElement* pLevel = pSubTexture->InsertNewChildElement("Level");

				uint32_t levelW = (s.w) / powl(2, i);
				uint32_t levelH = (s.h) / powl(2, i);

				pLevel->SetAttribute("level", i);
				pLevel->SetAttribute("width", levelW);
				pLevel->SetAttribute("height", levelH);

				processSubTexture(&v, &s, pLevel, handle, i, s.hash);
			}
		}
	}
}
