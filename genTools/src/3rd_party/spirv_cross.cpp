#include <3rd_party/spirv_cross.h>
#include <gongen/core/platform.hpp>

#define extern

extern std::function<spvc_result(spvc_context* context)> spvc_context_create;
extern std::function<void(spvc_context context)> spvc_context_destroy;
extern std::function<void(spvc_context context, spvc_error_callback cb, void* userdata)> spvc_context_set_error_callback;

extern std::function<spvc_result(spvc_context context, const SpvId* spirv, size_t word_count, spvc_parsed_ir* parsed_ir)> spvc_context_parse_spirv;
extern std::function<spvc_result(spvc_context context, spvc_backend backend, spvc_parsed_ir parsed_ir, spvc_capture_mode mode, spvc_compiler* compiler)> spvc_context_create_compiler;

extern std::function<spvc_result(spvc_compiler compiler, spvc_resources* resources)> spvc_compiler_create_shader_resources;
extern std::function<spvc_result(spvc_resources resources, spvc_resource_type type, const spvc_reflected_resource** resource_list, size_t* resource_size)> spvc_resources_get_resource_list_for_type;

extern std::function<void(spvc_compiler compiler, SpvId id, SpvDecoration decoration, unsigned argument)> spvc_compiler_set_decoration;
extern std::function<void(spvc_compiler compiler, SpvId id, SpvDecoration decoration, const char* argument)> spvc_compiler_set_decoration_string;
extern std::function<void(spvc_compiler compiler, SpvId id, const char* argument)> spvc_compiler_set_name;
extern std::function<void(spvc_compiler compiler, SpvId id, SpvDecoration decoration)> spvc_compiler_unset_decoration;
extern std::function<const char* (spvc_compiler compiler, SpvId id)> spvc_compiler_get_name;
extern std::function<unsigned(spvc_compiler compiler, SpvId id, SpvDecoration decoration)> spvc_compiler_get_decoration;
extern std::function<const char* (spvc_compiler compiler, SpvId id, SpvDecoration decoration)> spvc_compiler_get_decoration_string;

extern std::function<spvc_result(spvc_compiler compiler, spvc_compiler_options* options)> spvc_compiler_create_compiler_options;
extern std::function<spvc_result(spvc_compiler_options options, spvc_compiler_option option, spvc_bool value)> spvc_compiler_options_set_bool;
extern std::function<spvc_result(spvc_compiler_options options, spvc_compiler_option option, unsigned  value)> spvc_compiler_options_set_uint;
extern std::function<spvc_result(spvc_compiler compiler, spvc_compiler_options options)> spvc_compiler_install_compiler_options;

extern std::function<spvc_result(spvc_compiler compiler, const char* name, SpvExecutionModel model)> spvc_compiler_set_entry_point;
extern std::function<spvc_result(spvc_compiler compiler, const char** source)> spvc_compiler_compile;

extern std::function<spvc_result(spvc_compiler compilerl)> spvc_compiler_build_combined_image_samplers;
extern std::function<spvc_result(spvc_compiler compiler, const spvc_combined_image_sampler** samplers, size_t* num_samplers)> spvc_compiler_get_combined_image_samplers;

const char* spirv_cross_name = "spirv-cross-c-shared";

void* spirv_cross_lib = nullptr;

bool loadSPIRVCross() {
    spirv_cross_lib = gen::loadLibrary(spirv_cross_name);
    if (spirv_cross_lib) {
        spvc_context_create = (spvc_result(*)(spvc_context*))gen::getProcAddress(spirv_cross_lib, "spvc_context_create");
        spvc_context_destroy = (void(*)(spvc_context))gen::getProcAddress(spirv_cross_lib, "spvc_context_destroy");
        spvc_context_set_error_callback = (void(*)(spvc_context, spvc_error_callback, void*))gen::getProcAddress(spirv_cross_lib, "spvc_context_set_error_callback");

        spvc_context_parse_spirv = (spvc_result(*)(spvc_context, const SpvId*, size_t, spvc_parsed_ir*))gen::getProcAddress(spirv_cross_lib, "spvc_context_parse_spirv");
        spvc_context_create_compiler = (spvc_result(*)(spvc_context, spvc_backend, spvc_parsed_ir, spvc_capture_mode, spvc_compiler*))gen::getProcAddress(spirv_cross_lib, "spvc_context_create_compiler");

        spvc_compiler_create_shader_resources = (spvc_result(*)(spvc_compiler, spvc_resources*))gen::getProcAddress(spirv_cross_lib, "spvc_compiler_create_shader_resources");
        spvc_resources_get_resource_list_for_type = (spvc_result(*)(spvc_resources resources, spvc_resource_type type, const spvc_reflected_resource * *resource_list, size_t * resource_size))gen::getProcAddress(spirv_cross_lib, "spvc_resources_get_resource_list_for_type");

        spvc_compiler_set_decoration = (void(*)(spvc_compiler, SpvId, SpvDecoration, unsigned))gen::getProcAddress(spirv_cross_lib, "spvc_compiler_set_decoration");
        spvc_compiler_set_decoration_string = (void(*)(spvc_compiler, SpvId, SpvDecoration, const char*))gen::getProcAddress(spirv_cross_lib, "spvc_compiler_set_decoration_string");
        spvc_compiler_set_name = (void(*)(spvc_compiler, SpvId, const char*))gen::getProcAddress(spirv_cross_lib, "spvc_compiler_set_name");
        spvc_compiler_unset_decoration = (void(*)(spvc_compiler, SpvId, SpvDecoration))gen::getProcAddress(spirv_cross_lib, "spvc_compiler_unset_decoration");
        spvc_compiler_get_name = (const char* (*)(spvc_compiler, SpvId))gen::getProcAddress(spirv_cross_lib, "spvc_compiler_get_name");
        spvc_compiler_get_decoration = (unsigned(*)(spvc_compiler, SpvId, SpvDecoration))gen::getProcAddress(spirv_cross_lib, "spvc_compiler_get_decoration");
        spvc_compiler_get_decoration_string = (const char* (*)(spvc_compiler, SpvId, SpvDecoration))gen::getProcAddress(spirv_cross_lib, "spvc_compiler_get_decoration_string");
        
        spvc_compiler_create_compiler_options = (spvc_result(*)(spvc_compiler, spvc_compiler_options*))gen::getProcAddress(spirv_cross_lib, "spvc_compiler_create_compiler_options");
        spvc_compiler_options_set_bool = (spvc_result(*)(spvc_compiler_options, spvc_compiler_option, spvc_bool))gen::getProcAddress(spirv_cross_lib, "spvc_compiler_options_set_bool");
        spvc_compiler_options_set_uint = (spvc_result(*)(spvc_compiler_options, spvc_compiler_option, unsigned))gen::getProcAddress(spirv_cross_lib, "spvc_compiler_options_set_uint");
        spvc_compiler_install_compiler_options = (spvc_result(*)(spvc_compiler , spvc_compiler_options))gen::getProcAddress(spirv_cross_lib, "spvc_compiler_install_compiler_options");

        spvc_compiler_set_entry_point = (spvc_result(*)(spvc_compiler, const char*, SpvExecutionModel))gen::getProcAddress(spirv_cross_lib, "spvc_compiler_set_entry_point");
        spvc_compiler_compile = (spvc_result(*)(spvc_compiler, const char**))gen::getProcAddress(spirv_cross_lib, "spvc_compiler_compile");

        spvc_compiler_build_combined_image_samplers = (spvc_result(*)(spvc_compiler))gen::getProcAddress(spirv_cross_lib, "spvc_compiler_build_combined_image_samplers");
        spvc_compiler_get_combined_image_samplers = (spvc_result(*)(spvc_compiler, const spvc_combined_image_sampler**, size_t*))gen::getProcAddress(spirv_cross_lib, "spvc_compiler_get_combined_image_samplers");

        return true;
    }
    else return false;
}

void unloadSPIRVCross() {
    if (spirv_cross_lib) gen::unloadLibrary(spirv_cross_lib);
}
