#include <3rd_party/shaderc.h>
#include <gongen/core/platform.hpp>

#define extern

extern std::function<shaderc_compiler_t(void)> shaderc_compiler_initialize;
extern std::function<void(shaderc_compiler_t)> shaderc_compiler_release;

extern std::function<shaderc_compile_options_t(void)> shaderc_compile_options_initialize;
extern std::function<void(shaderc_compile_options_t options)> shaderc_compile_options_release;
extern std::function<void(shaderc_compile_options_t options, const char* name, size_t name_length, const char* value, size_t value_length)> shaderc_compile_options_add_macro_definition;
extern std::function<void(shaderc_compile_options_t options, shaderc_source_language lang)> shaderc_compile_options_set_source_language;
extern std::function<void(shaderc_compile_options_t options)> shaderc_compile_options_set_generate_debug_info;
extern std::function<void(shaderc_compile_options_t options, shaderc_optimization_level level)> shaderc_compile_options_set_optimization_level;
extern std::function<void(shaderc_compile_options_t options, shaderc_target_env target, uint32_t version)> shaderc_compile_options_set_target_env;
extern std::function<void(shaderc_compile_options_t options, shaderc_spirv_version version)> shaderc_compile_options_set_target_spirv;
extern std::function<void(shaderc_compile_options_t options, shaderc_include_resolve_fn resolver, shaderc_include_result_release_fn result_releaser, void* user_data)> shaderc_compile_options_set_include_callbacks;

extern std::function<shaderc_compilation_result_t(const shaderc_compiler_t compiler, const char* source_text,
    size_t source_text_size, shaderc_shader_kind shader_kind,
    const char* input_file_name, const char* entry_point_name,
    const shaderc_compile_options_t additional_options)> shaderc_compile_into_spv;

extern std::function<void(shaderc_compilation_result_t result)> shaderc_result_release;
extern std::function<size_t(const shaderc_compilation_result_t result)> shaderc_result_get_length;
extern std::function<const char* (const shaderc_compilation_result_t result)> shaderc_result_get_bytes;
extern std::function<shaderc_compilation_status(const shaderc_compilation_result_t result)> shaderc_result_get_compilation_status;
extern std::function<const char* (const shaderc_compilation_result_t result)> shaderc_result_get_error_message;

const char* shaderc_name = "shaderc_shared";

void* shaderc_lib = nullptr;

bool loadShaderc() {
    shaderc_lib = gen::loadLibrary(shaderc_name);
    if (shaderc_lib) {
        shaderc_compiler_initialize = (shaderc_compiler_t(*)(void))gen::getProcAddress(shaderc_lib, "shaderc_compiler_initialize");
        shaderc_compiler_release = (void(*)(shaderc_compiler_t))gen::getProcAddress(shaderc_lib, "shaderc_compiler_release");
        
        shaderc_compile_options_initialize = (shaderc_compile_options_t(*)(void))gen::getProcAddress(shaderc_lib, "shaderc_compile_options_initialize");
        shaderc_compile_options_release = (void(*)(shaderc_compile_options_t))gen::getProcAddress(shaderc_lib, "shaderc_compile_options_release");
        shaderc_compile_options_add_macro_definition = (void(*)(shaderc_compile_options_t, const char*, size_t, const char*, size_t))gen::getProcAddress(shaderc_lib, "shaderc_compile_options_add_macro_definition");
        shaderc_compile_options_set_source_language = (void(*)(shaderc_compile_options_t, shaderc_source_language))gen::getProcAddress(shaderc_lib, "shaderc_compile_options_set_source_language");
        shaderc_compile_options_set_generate_debug_info = (void(*)(shaderc_compile_options_t))gen::getProcAddress(shaderc_lib, "shaderc_compile_options_set_generate_debug_info");
        shaderc_compile_options_set_optimization_level = (void(*)(shaderc_compile_options_t, shaderc_optimization_level))gen::getProcAddress(shaderc_lib, "shaderc_compile_options_set_optimization_level");
        shaderc_compile_options_set_target_env = (void(*)(shaderc_compile_options_t, shaderc_target_env, uint32_t))gen::getProcAddress(shaderc_lib, "shaderc_compile_options_set_target_env");
        shaderc_compile_options_set_target_spirv = (void(*)(shaderc_compile_options_t, shaderc_spirv_version))gen::getProcAddress(shaderc_lib, "shaderc_compile_options_set_target_spirv");
        shaderc_compile_options_set_include_callbacks = (void(*)(shaderc_compile_options_t, shaderc_include_resolve_fn, shaderc_include_result_release_fn, void*))gen::getProcAddress(shaderc_lib, "shaderc_compile_options_set_include_callbacks");
        
        shaderc_compile_into_spv = (shaderc_compilation_result_t(*)(const shaderc_compiler_t, const char*, size_t, shaderc_shader_kind, const char*, const char*, const shaderc_compile_options_t))gen::getProcAddress(shaderc_lib, "shaderc_compile_into_spv");
        
        shaderc_result_release = (void(*)(const shaderc_compilation_result_t result))gen::getProcAddress(shaderc_lib, "shaderc_result_release");
        shaderc_result_get_length = (size_t(*)(const shaderc_compilation_result_t result))gen::getProcAddress(shaderc_lib, "shaderc_result_get_length");
        shaderc_result_get_bytes = (const char* (*)(const shaderc_compilation_result_t result))gen::getProcAddress(shaderc_lib, "shaderc_result_get_bytes");
        shaderc_result_get_compilation_status = (shaderc_compilation_status(*)(const shaderc_compilation_result_t result))gen::getProcAddress(shaderc_lib, "shaderc_result_get_compilation_status");
        shaderc_result_get_error_message = (const char* (*)(const shaderc_compilation_result_t result))gen::getProcAddress(shaderc_lib, "shaderc_result_get_error_message");

        return true;
    }
    else return false;
}

void unloadShaderc() {
    if (shaderc_lib) gen::unloadLibrary(shaderc_lib);
}
