struct VSInput {
	float3 pos : POSITION;
	float3 splineData : TEXCOORD0;
	uint type : TEXCOORD1;
	
	float3x3 transformMatrix : COLOR0;
	float4 gradientPosition : COLOR1;
	float4 color1 : COLOR2;
	float4 color2 : COLOR3;
	uint flags : COLOR4;
};

struct PSInput {
	float4 pos : SV_POSITION;
	float3 splineData : TEXCOORD0;
	uint type : TEXCOORD1;

	float4 gradientPosition : COLOR1;
	float4 color1 : COLOR2;
	float4 color2 : COLOR3;
	uint flags : COLOR4;
};

layout(binding = 0, set = 0) cbuffer UniformBuffer {
	float4x4 projectionMatrix;
};

PSInput vertexShader(VSInput IN) {
	PSInput OUT;
	float3 pos1 = mul(IN.transformMatrix, IN.pos);
	OUT.pos = mul(projectionMatrix, float4(pos1, 1.0f));
	
	float3 gpos1 = mul(IN.transformMatrix, float3(IN.gradientPosition.xy, 0.0f));
	float3 gpos2 = mul(IN.transformMatrix, float3(IN.gradientPosition.zw, 0.0f));

	float4 gpos1_2 = mul(projectionMatrix, float4(gpos1, 1.0f));
	float4 gpos2_2 = mul(projectionMatrix, float4(gpos2, 1.0f));
	
	OUT.splineData = IN.splineData;
	OUT.type = IN.type;

	OUT.gradientPosition = float4(gpos1_2.xy, gpos2_2.xy);
	OUT.color1 = IN.color1;
	OUT.color2 = IN.color2;
	OUT.flags = IN.flags;
	return OUT;
}

#define VERTEX_TYPE_FILL 0
#define VERTEX_TYPE_QUADRATIC_SPLINE 1
#define VERTEX_TYPE_NEGATIVE_SIGN_FLAG 128

#define PART_FLAG_FLAT_COLOR 1
#define PART_FLAG_LINEAR_GRADIENT 2
#define PART_FLAG_DONT_RENDER 4

float4 pixelShader(PSInput IN) : SV_TARGET {
  bool curve_sign = (IN.type & VERTEX_TYPE_NEGATIVE_SIGN_FLAG);
  bool curve = (IN.type & VERTEX_TYPE_QUADRATIC_SPLINE);

  if (IN.flags & PART_FLAG_DONT_RENDER) {
	  discard;
  }

  if(curve) {
    float result = (pow(IN.splineData.x, 2) - IN.splineData.y);
    if( ((sign(result) < 0) && curve_sign) || ((sign(result) > 0) && !curve_sign) ) {
      discard;
    }
  }

  /*if (IN.flags & PART_FLAG_LINEAR_GRADIENT) {
	  float2 gradientPoint1 = float2(IN.gradientPosition.x, IN.gradientPosition.y);
	  float2 gradientPoint2 = float2(IN.gradientPosition.z, IN.gradientPosition.w);

	  float gradientDistance1 = pow(IN.pos.xy - gradientPoint1, 2);
	  float gradientDistance2 = pow(IN.pos.xy - gradientPoint2, 2);
  }*/

  return IN.color1;
}
