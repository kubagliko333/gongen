struct VSInput {
	float2 pos : POSITION;
	float4 color : COLOR0;
};

struct PSInput {
	float4 pos : SV_POSITION;
	float4 color : COLOR0;
};

layout(binding = 0, set = 0) cbuffer UniformBuffer {
	float4x4 projectionMatrix;
};

PSInput vertexShader(VSInput IN) {
	PSInput OUT;
	OUT.pos = mul(modelMatrix, float4(IN.pos, 0.0f, 1.0f));
	OUT.color = IN.color;
	return OUT;
}

float4 pixelShader(PSInput IN) : SV_TARGET{
  return IN.color;
}
