struct VSInput {
	float2 pos : POSITION;
};

struct PSInput {
	float4 pos : SV_POSITION;
	float2 pos2 : POSITION;
};

layout(binding = 0, set = 0) cbuffer UniformBuffer {
	float4x4 modelMatrix;
};

layout(binding = 1, set = 0) Texture2D texture0 : register(t0);
layout(binding = 2, set = 0) SamplerState sampler0 : register(s0);

PSInput vertexShader(VSInput IN) {
	PSInput OUT;
#ifdef USE_UBO
	OUT.pos = mul(modelMatrix, float4(IN.pos, 0.0f, 1.0f));
#else
	OUT.pos = float4(IN.pos, 0.0f, 1.0f);
#endif
	OUT.pos2 = IN.pos;
	return OUT;
}

float4 pixelShader(PSInput IN) : SV_TARGET{
  float2 pos2_norm = (IN.pos2 + 1.0f) / 2.0f;
#ifdef USE_TEXTURE
  return texture0.Sample(sampler0, pos2_norm);
#else
  return float4(pos2_norm.xy, 0.0f, 1.0f);
#endif
}

