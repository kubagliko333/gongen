#pragma once

#include <string>

namespace gen_unit_loader {

enum class UnitType {
	CORE,
	MODULE,
	API,
	IMPL,
	APP
};

struct UnitMeta {
	UnitType type;
	std::string idLiteral;
	std::string name;
	bool noInstances;
};

class AbstractUnitRegistry {
public:
	virtual void registerUnit(UnitMeta meta) = 0;
};

void loadUnitsToRegistry(AbstractUnitRegistry* registry);

}