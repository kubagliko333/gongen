#pragma once

#define IMGUI_DISABLE_OBSOLETE_FUNCTIONS
#define IMGUI_DISABLE_OBSOLETE_KEYIO
#define IMGUI_USE_WCHAR32
#define ImDrawIdx unsigned int
#define IMGUI_DEBUG_TOOL_ITEM_PICKER_EX
#define ImTextureID void*
