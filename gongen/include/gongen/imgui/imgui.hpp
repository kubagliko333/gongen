#pragma once

#include <gongen/graphics_api/graphics_device.hpp>
#include <gongen/window_api/window.hpp>
#include "imconfig.hpp"
#include <imgui.h>

namespace gen {
	//use this when passing GongEn textures to ImGui functions
	ImTextureID imguiTexture(gen::TexturePtr texture);

	void ImGui_Init(gen::Window* window, gen::GraphicsDevice* graphicsDevice, gen::ShaderPtr shader);
	void ImGui_NewFrame(const gen::CommandBufferPtr& commandBuffer);
	void ImGui_ProcessEvent(gen::Event& event);
	//call this inside render pass
	void ImGui_Render(const gen::CommandBufferPtr& commandBuffer);
}
