/**
 * @file hash.hpp
 * @brief Provides hashing functionality.
 * 
 * Adds global functions generating hashes. Supported sizes are 32-bit, 64-bit and 128-bit.
 * For consistency between x64 and x86 systems in 128-bit hashes, use the hash functions with the `_stable` suffix.
 * 
 * The hashing is based on MurmurHash3 by Austin Appleby.
 */
#pragma once

#include "export.hpp"
#include <stdint.h>
#include <stddef.h>
#include <string.h>

/**
 * @brief Defines the default hashing seed for the MurmurHash3 algorithm.
 */
#define GEN_DEFAULT_HASH_SEED 4165157

namespace gen {

/**
 * @brief A simple structure holding a 128-bit hash.
 */
struct Hash128 {
	Hash128() { this->data[0] = 0; this->data[1] = 0; }
	Hash128(const Hash128 &h) { this->data[0] = h.data[0]; this->data[1] = h.data[1]; }

	uint64_t data[2];

	size_t operator()() const {
		return (data[0] ^ data[1]);
	}

	inline bool operator==(const Hash128& rhs) const {
		return !memcmp(data, rhs.data, sizeof(uint64_t) * 2);
	}
};

/**
 * @brief Generates a 32-bit hash for the given value.
 * 
 * @param data Input data.
 * @param dataSize Input data size.
 * @param seed The seed for the MurmurHash3 algorithm.
 * @return uint32_t Hashed data.
 */
GEN_EXPORTED uint32_t hash32(const void *data, size_t dataSize, uint32_t seed = GEN_DEFAULT_HASH_SEED);

/**
 * @brief Generates a 64-bit hash for the given value.
 * 
 * @param data Input data.
 * @param dataSize Input data size.
 * @param seed The seed for the MurmurHash3 algorithm.
 * @return uint64_t Hashed data.
 */
GEN_EXPORTED uint64_t hash64(const void *data, size_t dataSize, uint32_t seed = GEN_DEFAULT_HASH_SEED);

/**
 * @brief Generates a 128-bit hash for the given value.
 * @warning This function will return different results for the same data on 64-bit and 32-bit systems. For stable, 32-bit-only version of this function, see hash128_stable().
 * 
 * @param out The output array of two 64-bit unsigned integers.
 * @param data Input data.
 * @param dataSize Input data size.
 * @param seed The seed for the MurmurHash3 algorithm.
 */
GEN_EXPORTED void hash128(uint64_t* out, const void* data, size_t dataSize, uint32_t seed = GEN_DEFAULT_HASH_SEED);

/**
 * @brief Generates a 128-bit hash for the given value.
 * 
 * This function is consistent between x64 and x86 systems, but slower than hash128() on the 64-bit ones.
 * 
 * @param out The output array of two 64-bit unsigned integers.
 * @param data Input data.
 * @param dataSize Input data size.
 * @param seed The seed for the MurmurHash3 algorithm.
 */
GEN_EXPORTED void hash128_stable(uint64_t *out, const void* data, size_t dataSize, uint32_t seed = GEN_DEFAULT_HASH_SEED);

/**
 * @brief Generates a 128-bit hash for the given value and puts it into the Hash128 structure.
 * @warning This function will return different results for the same data on 64-bit and 32-bit systems. For stable, 32-bit-only version of this function, see hash128_stable().
 * 
 * @param data Input data.
 * @param dataSize Input data size.
 * @param seed The seed for the MurmurHash3 algorithm.
 * @return Hash128 Hashed data.
 */
GEN_EXPORTED Hash128 hash128(const void* data, size_t dataSize, uint32_t seed = GEN_DEFAULT_HASH_SEED);

/**
 * @brief Generates a 128-bit hash for the given value and puts it into the Hash128 structure.
 * 
 * This function is consistent between x64 and x86 systems, but slower than hash128() on the 64-bit ones.
 * 
 * @param data Input data.
 * @param dataSize Input data size.
 * @param seed The seed for the MurmurHash3 algorithm.
 * @return Hash128 Hashed data.
 */
GEN_EXPORTED Hash128 hash128_stable(const void* data, size_t dataSize, uint32_t seed = GEN_DEFAULT_HASH_SEED);

}
