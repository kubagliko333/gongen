#pragma once

#include "export.hpp"
#include "logger.hpp"
#include <gongen_unit_loader/loader.hpp>

namespace gen {

class Core;
class Unit;
template<Derived<Unit> T>
class UnitBuilder;

typedef gen_unit_loader::UnitType UnitType;

struct GEN_EXPORTED UnitMeta {
	UnitType type;
	Identifier identifier;
	std::string name;
	bool noInstances;
};

class GEN_EXPORTED Unit {
public:
	Core* const core;
	const UnitMeta* const meta;
public:
	const size_t instanceIndex;
protected:
	const Logger* logger;

	Unit(Core* core, UnitMeta* meta, size_t instanceIndex);
	virtual ~Unit() {
	}
private:
	Unit(Core* core, UnitMeta* meta, size_t instanceIndex, Logger* logger);

	friend class Core;
};

class GEN_EXPORTED UnitRegistry : public LegalRegistry<RegisteredForeign<UnitMeta>>, private gen_unit_loader::AbstractUnitRegistry {
private:
	std::unordered_map<uint64_t, std::vector<Unit*>*> instances;
public:
	template<Derived<Unit> T>
	T* initializeUnit(Identifier identifier, UnitBuilder<T> builder) {
		this->hasOrThrow(identifier);
		UnitMeta* meta = this->get(identifier);
		if (meta->type == UnitType::CORE) {
			throw ArgumentException("Creating another CORE unit not allowed, please use another unit type");
		}
		T* unit = builder.build(this->core, meta, this->instances[meta->identifier.hash]->size() - 1);
		this->addInstance(meta, unit);
		return unit;
	}
	template<Derived<Unit> T>
	T* getInstance(Identifier identifier, size_t index) {
		this->hasOrThrow(identifier);
		UnitMeta* meta = this->get(identifier);
		if (index >= this->instances[identifier.hash]->size()) {
			throw ArgumentException("Trying to get instance of unit beyond the vector's limit");
		}
		return this->instances[identifier.hash]->at(index);
	}

private:
	UnitRegistry(Core* core, std::string id);
	void addInstance(UnitMeta* meta, Unit* unit);
	void registerUnit(gen_unit_loader::UnitMeta meta);
	void registerInternal(gen::Identifier identifier, UnitMeta* meta);

	friend class Core;
};

template<Derived<Unit> T>
class GEN_EXPORTED UnitBuilder {
protected:
	virtual T* build(Core* core, UnitMeta* meta, size_t instanceIndex) = 0;

	friend class UnitRegistry;
};

}