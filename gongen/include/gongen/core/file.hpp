/**
 * @file file.hpp
 * @brief Defines GongEn's file I/O Abstraction
 */
#pragma once

#include "export.hpp"
#include <stdint.h>
#include <string>
#include <vector>

namespace gen {
namespace internal {
	enum FileOpenModeFlags {
		FLAG_READ = 1,
		FLAG_WRITE = 2,
		FLAG_APPEND = 4
	};
}

enum class FileOpenMode {
	READ = internal::FLAG_READ,
	WRITE = internal::FLAG_WRITE,
	APPEND = internal::FLAG_WRITE | internal::FLAG_APPEND,
	READ_WRITE = internal::FLAG_READ | internal::FLAG_WRITE,
};

enum class FileMapMode {
	READ,
	READ_WRITE
};

enum class FileSeekType {
	SET,
	CUR,
	END
};

GEN_EXPORTED class FileHandle {
	FileOpenMode mode;
	void* os_handle = nullptr;
	void* mapping_handle = nullptr;
	void* mapped_addr = nullptr;
	std::string filepath;
public:
	/**
	* @brief initialize FileHandle without opening any file
	**/
	FileHandle() {}
	/**
	* @brief this constructor calls open
	**/
	FileHandle(const std::string& filepath, FileOpenMode openMode, bool throwExceptions = true);
	/**
	* @brief closes file(if opened), unmaps file(if mapped)
	**/
	~FileHandle();

	/**
	* @brief opens file using specified openMode, if throwExceptions is true, throws FileOpenException when file open fails
	**/
	void open(const std::string& filepath, FileOpenMode openMode, bool throwExceptions = true);
	/**
	* @brief closes file(if opened), unmaps file(if mapped)
	**/
	void close();

	/**
	* @brief returns true if file was opened
	**/
	bool isOpen();
	/**
	* @brief returns true if file was mapped
	**/
	bool isMapped();

	/**
	* @brief returns file size in bytes
	**/
	uint64_t size();
	/**
	* @brief changes size of the file
	**/
	void setSize(uint64_t size);
	/**
	* @brief moves file read/write cursor
	**/
	uint64_t seek(int64_t offset, FileSeekType type = FileSeekType::SET);
	/**
	* @brief returns current position of file read/write cursor
	**/
	uint64_t cursor();

	/**
	* @brief tries to reads specified amount of data, returns amount of data read
	**/
	uint64_t read(uint64_t size, void* data);
	/**
	* @brief tries to write specified amount of data, returns amount of data written
	**/
	uint64_t write(uint64_t size, void* data);
	/**
	* @brief make sure that data writen to file is really written to disc
	**/
	void flush();

	/**
	* @brief maps the file
	**/
	void* map(FileMapMode mode = FileMapMode::READ, uint64_t offset = 0, uint64_t size = 0, void* addr = nullptr, bool throwExceptions = true);
	/**
	* @brief make sure that data writen to file through mapping is really written to disc
	**/
	void mapFlush(uint64_t offset = 0, uint64_t size = 0);

	/**
	* @brief returns file creation time(as unix timestamp in secounds)
	**/
	uint64_t creationTime();
	/**
	* @brief returns file last modification time(as unix timestamp in secounds)
	**/
	uint64_t lastModificationTime();
	/**
	* @brief returns file last access time(as unix timestamp in secounds)
	**/
	uint64_t lastAccessTime();
};
 /**
  * @brief Checks if file or directory exist in specified path
  *
  * @param path Path to check
  * @return bool true if path exist, false if not
  */
GEN_EXPORTED bool pathExist(const char* path);

/**
 * @brief Checks if path is a directory
 *
 * @param path Path to check
 * @return bool true if path is a directory, false if not or if it don't exist
 */
GEN_EXPORTED bool isDir(const char *path);

/**
 * @brief Creates a directory
 * @param path Path of directory to create
 * @return bool true if directory already existed, false if it was created
 */
GEN_EXPORTED bool createDir(const char* path);

/**
 * @brief File(or directory) enumarated by gen::enumarateDir
 */
struct enumaratedFile {
	bool isDirectory;
	std::string name;
	std::string path;
};
/**
 * @brief Get list of files and directories inside a path
 * @param path Path of directory to list content of
 * @return std::vector<enumaratedFile> list of files and directories 
 */
GEN_EXPORTED std::vector<enumaratedFile> enumarateDir(const char* dir);

/**
 * @brief Get directory in which active executable is located
 */
GEN_EXPORTED std::string getExecutableDir();

/**
 * @brief Get preferencies directory
 */
GEN_EXPORTED std::string getPrefDir(const std::string &org, const std::string &app);
}
