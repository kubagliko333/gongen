/**
 * @file gongen.hpp
 * @brief Defines main GongEn's core features (mainly the Core unit itself).
 */
#pragma once

#include "logger.hpp"
#include "unit.hpp"

namespace gen {

class CoreBuilder;

/**
 * @brief The GongEn Core. Main unit of the engine.
 * 
 * Unlike other units, Core can be freely initialized with a CoreBuilder object.
 * The Core contains the entire unit system, and automatically registers itself during construction.
 * To manage units, use basic methods of this class.
 */
class GEN_EXPORTED Core : public Unit {
private:
	internal::RegistryManager registryManager;
	
	std::vector<uint64_t> globalLoggerCallbacks;
public:
	UnitRegistry* const unitRegistry;

	/**
	 * @brief Check if there's a registry with the given id in the system.
	 * 
	 * @param id Registries id.
	 * @return true if a registry with the given id exists.
	 * @return false if there's no registry with the given id.
	 */
	bool hasRegistry(std::string id)  {
		return this->registryManager.contains(id);
	}

	/**
	 * @brief Check whether the given Registry pointer points to a registry existing in the system.
	 * 
	 * @tparam T The registry type.
	 * @param registry The checked registry pointer.
	 * @return true if the registry exists in the system.
	 * @return false if the registry does not exist in the system.
	 */
	template<class T> bool hasRegistry(Registry<T>* registry) {
		if (LegalRegistry<T>* legalRegistry = dynamic_cast<LegalRegistry<T>*>(registry)) {
			return this->registryManager.contains(legalRegistry->idHash);
		}
		return false;
	}

	/**
	 * @brief Add a new standard legal registry to the system built with the RegistryBuilder.
	 * 
	 * @tparam T Type of the items stored in the registry.
	 * @param builder The builder with the final properties of the LegalRegistry.
	 * @return LegalRegistry<T>* A pointer to the created and registered Registry.
	 */
	template<class T> LegalRegistry<T>* addRegistry(LegalRegistryBuilder<T>* builder) {
		return this->registryManager.add(builder, this);
	}

	/**
	 * @brief Add a new custom legal registry to the system.
	 * You can't add standard legal registries (the basic Registry class) using this functions.
	 * 
	 * @tparam T Type of the items stored in the registry.
	 * @param registry The registry.
	 * @return LegalRegistry<T>* A pointer to the created and registered Registry.
	 */
	template<class T> LegalRegistry<T>* addRegistry(LegalRegistry<T>* registry) {
		return this->registryManager.add(registry);
	}

	/**
	 * @brief Get the registry of the given id from the system.
	 * 
	 * @tparam T Type of the items stored in the registry.
	 * @param id The id of the legal registry.
	 * @return LegalRegistry<T>* Pointer to the registry.
	 */
	template<class T> LegalRegistry<T>* getRegistry(std::string id) {
		return this->registryManager.get<T>(id);
	}

	/**
	 * @brief Build and registers a logger.
	 * 
	 * Builds the logger and adds it to the `logger` registry.
	 * 
	 * @param builder Builder for the logger.
	 * @return Logger& The built logger.
	 */
	Logger* addLogger(LoggerBuilder* builder);

	/**
	 * @brief Adds a global logger callback.
	 * 
	 * Loggers using the global callback list will also log to this callback.
	 * 
	 * @param id Id of the callback in the `gen::logger_callback` registry.
	 */
	void addLoggerCallback(const gen::Identifier& id);

private:
	Core(std::vector<uint64_t> globalLoggerCallbacks);
	Core(Core&) = delete;
	Core(const Core&) = delete;

	virtual ~Core() {
	}

	friend class CoreBuilder;
	friend class Logger;
};

/**
 * @brief Builder for the Core object.
 * 
 * Lets you set up the Core and initialize it.
 * 
 * TODO adding global logger callbacks here
 */
class GEN_EXPORTED CoreBuilder {
private:
	std::vector<uint64_t> globalLoggerCallbacks;
public:
	/**
	 * @copydoc Core::addLoggerCallback
	 */
	void addLoggerCallback(const gen::Identifier& id);

	/**
	 * @brief Build and initialize the Core instance.
	 * @return Core* Pointer to the created GongEn instance.
	 */
	Core* build();
};

}
