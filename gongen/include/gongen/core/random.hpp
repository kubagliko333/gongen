/**
 * @file random.hpp
 * @brief Adds random number generator features to gongen. The Random class.
 */
#pragma once

#include <stdint.h>
#include "export.hpp"

namespace gen {

/**
 * @brief An easy to use pseudorandom number generator based on the LCG algorithm.
 * 
 * @warning THE LCG VALUES (the multiplier and the modulus) WILL BE MODIFIED BEFORE 0.0.0.1!
 * AFTER THE CHANGE THE GENERATOR WILL GIVE DIFFERENT RESULTS WITH THE SAME SEED THAN BEFORE
 * 
 * The generator is based on the linear congruential generator algorithm.
 * Objects of this class are not related to each other. They use separate seeds and value sequences.
 * The class provides simple methods allowing you to quickly get the needed pseudorandom value.
 */
class GEN_EXPORTED Random {
private:
	uint64_t value;

	uint64_t next();
public:
	const uint64_t seed;

	/**
	 * @brief Construct a new Random object with the given seed.
	 * 
	 * Creates a new pseudorandom number generator and sets its seed to the given value.
	 * @param seed The seed (starting value) for this generator.
	 */
	Random(uint64_t seed);

	/**
	 * @brief Returns the next pseudorandom, 8-bit, unsigned integer number.
	 * 
	 * @return uint8_t Generated pseudorandom uint8.
	 */
	uint8_t nextUint8();

	/**
	 * @brief Returns the next pseudorandom, 8-bit, unsigned integer number between 0 (inclusive) and the given max (exclusive) value.
	 * 
	 * @param max The maximal value of the generated number (exclusive).
	 * @return uint8_t Generated pseudorandom uint8.
	 */
	uint8_t nextUint8(uint8_t max);

	/**
	 * @brief Returns the next pseudorandom, 8-bit, unsigned integer number between the given min (inclusive) and max values.
	 * 
	 * @param min The minimal value of the generated number (inclusive).
	 * @param max The maximal value of the generated number.
	 * @return uint8_t Generated pseudorandom uint8.
	 */
	uint8_t nextUint8(uint8_t min, uint8_t max);

	/**
	 * @brief Returns the next pseudorandom, 16-bit, unsigned integer number.
	 * 
	 * @return uint16_t Generated pseudorandom uint16.
	 */
	uint16_t nextUint16();

	/**
	 * @brief Returns the next pseudorandom, 16-bit, unsigned integer number between 0 (inclusive) and the given max (exclusive) value.
	 * 
	 * @param max The maximal value of the generated number (exclusive).
	 * @return uint16_t Generated pseudorandom uint16.
	 */
	uint16_t nextUint16(uint16_t max);

	/**
	 * @brief Returns the next pseudorandom, 16-bit, unsigned integer number between the given min (inclusive) and max (exclusive) values.
	 * 
	 * @param min The minimal value of the generated number (inclusive).
	 * @param max The maximal value of the generated number (exclusive).
	 * @return uint16_t Generated pseudorandom uint16.
	 */
	uint16_t nextUint16(uint16_t min, uint16_t max);

	/**
	 * @brief Returns the next pseudorandom, 32-bit, unsigned integer number.
	 * 
	 * @return uint32_t Generated pseudorandom uint32.
	 */
	uint32_t nextUint32();

	/**
	 * @brief Returns the next pseudorandom, 32-bit, unsigned integer number between 0 (inclusive) and the given max (exclusive) value.
	 * 
	 * @param max The maximal value of the generated number (exclusive).
	 * @return uint32_t Generated pseudorandom uint32.
	 */
	uint32_t nextUint32(uint32_t max);

	/**
	 * @brief Returns the next pseudorandom, 32-bit, unsigned integer number between the given min (inclusive) and max (exclusive) values.
	 * 
	 * @param min The minimal value of the generated number (inclusive).
	 * @param max The maximal value of the generated number (exclusive).
	 * @return uint32_t Generated pseudorandom uint32.
	 */
	uint32_t nextUint32(uint32_t min, uint32_t max);

	/**
	 * @brief Returns the next pseudorandom, 64-bit, unsigned integer number.
	 * 
	 * @return uint64_t Generated pseudorandom uint64.
	 */
	uint64_t nextUint64();

	/**
	 * @brief Returns the next pseudorandom, 64-bit, unsigned integer number between 0 (inclusive) and the given max (exclusive) value.
	 * 
	 * @param max The maximal value of the generated number (exclusive).
	 * @return uint64_t Generated pseudorandom uint64.
	 */
	uint64_t nextUint64(uint64_t max);

	/**
	 * @brief Returns the next pseudorandom, 64-bit, unsigned integer number between the given min (inclusive) and max (exclusive) values.
	 * 
	 * @param min The minimal value of the generated number (inclusive).
	 * @param max The maximal value of the generated number (exclusive).
	 * @return uint64_t Generated pseudorandom uint64.
	 */
	uint64_t nextUint64(uint64_t min, uint64_t max);

	/**
	 * @brief Returns the next pseudorandom, 8-bit, signed integer number.
	 * 
	 * @return int8_t Generated pseudorandom int8.
	 */
	int8_t nextInt8();

	/**
	 * @brief Returns the next pseudorandom, 8-bit, signed integer number between the given min (inclusive) and max (exclusive) values.
	 * 
	 * @param min The minimal value of the generated number (inclusive).
	 * @param max The maximal value of the generated number (exclusive).
	 * @return int8_t Generated pseudorandom int8.
	 */
	int8_t nextInt8(int8_t min, int8_t max);

	/**
	 * @brief Returns the next pseudorandom, 16-bit, signed integer number.
	 * 
	 * @return int16_t Generated pseudorandom int16.
	 */
	int16_t nextInt16();

	/**
	 * @brief Returns the next pseudorandom, 16-bit, signed integer number between the given min (inclusive) and max (exclusive) values.
	 * 
	 * @param min The minimal value of the generated number (inclusive).
	 * @param max The maximal value of the generated number (exclusive).
	 * @return int16_t Generated pseudorandom int16.
	 */
	int16_t nextInt16(int16_t min, int16_t max);

	/**
	 * @brief Returns the next pseudorandom, 32-bit, signed integer number.
	 * 
	 * @return int32_t Generated pseudorandom int32.
	 */
	int32_t nextInt32();

	/**
	 * @brief Returns the next pseudorandom, 32-bit, signed integer number between the given min (inclusive) and max (exclusive) values.
	 * 
	 * @param min The minimal value of the generated number (inclusive).
	 * @param max The maximal value of the generated number (exclusive).
	 * @return int32_t Generated pseudorandom int32.
	 */
	int32_t nextInt32(int32_t min, int32_t max);

	/**
	 * @brief Returns the next pseudorandom, 64-bit, signed integer number.
	 * 
	 * @return int64_t Generated pseudorandom int64.
	 */
	int64_t nextInt64();

	/**
	 * @brief Returns the next pseudorandom, 64-bit, signed integer number between the given min (inclusive) and max (exclusive) values.
	 * 
	 * @param min The minimal value of the generated number (inclusive).
	 * @param max The maximal value of the generated number (exclusive).
	 * @return int64_t Generated pseudorandom int64.
	 */
	int64_t nextInt64(int64_t min, int64_t max);

	/**
	 * @brief Returns the next pseudorandom, 32-bit float number between 0 and 1.
	 * 
	 * @return float Generated pseudorandom float.
	 */
	float nextFloat();

	/**
	 * @brief Returns the next pseudorandom, 32-bit float number between the given min and max values.
	 * 
	 * @param min The minimal value of the generated number.
	 * @param max The maximal value of the generated number.
	 * @return float Generated pseudorandom float.
	 */
	float nextFloat(float min, float max);

	/**
	 * @brief Returns the next pseudorandom, 64-bit float (double) number between 0 and 1.
	 * 
	 * @return double Generated pseudorandom double.
	 */
	double nextDouble();

	/**
	 * @brief Returns the next pseudorandom, 64-bit float (double) number between the given min and max values.
	 * 
	 * @param min The minimal value of the generated number.
	 * @param max The maximal value of the generated number.
	 * @return double Generated pseudorandom double.
	 */
	double nextDouble(double min, double max);

	/**
	 * @brief Returns the next pseudorandom boolean.
	 * 
	 * @return bool Generated pseudorandom bool.
	 */
	bool nextBoolean();
};

}
