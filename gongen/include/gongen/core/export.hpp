#pragma once

#include "platform.hpp"
#include <type_traits>

#if !defined(GEN_BUILD_STATIC)
	#ifdef PLATFORM_WIN32
		#ifdef GEN_EXPORT
			#define GEN_EXPORTED __declspec(dllexport)
		#else
			#define GEN_EXPORTED __declspec(dllimport)
		#endif
	#elif defined(__GNUC__)
		#ifdef GEN_EXPORT
			#define GEN_EXPORTED __attribute__((visibility("default")))
		#else
			#define GEN_EXPORTED
		#endif
	#endif
#else
	#define GEN_EXPORTED
#endif

namespace gen {

template<typename T, typename U>
concept Derived = std::is_base_of_v<U, T>;

};