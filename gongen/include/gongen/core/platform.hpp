#pragma once

#if defined(_WIN32)
	#define PLATFORM_WIN32
	#if defined(_WIN64)
		#define PLATFORM_WIN64
	#endif
#elif defined(__APPLE__)
	#define PLATFORM_APPLE
#elif defined(__ANDROID__)
	#define PLATFORM_ANDROID
#elif defined(__linux__)
	#define PLATFORM_LINUX
#elif defined(__unix__)
	#include <sys/param.h>
	#if defined(BSD)
		#define PLATFORM_BSD
	#endif
#endif

#if defined(__unix__)
	#define PLATFORM_UNIX
#endif
#if defined(EMSCRIPTEN)
	#define PLATFORM_EMSCRIPTEN
#endif

#if defined(PLATFORM_WIN32) || defined(PLATFORM_LINUX)
	#define PLATFORM_DESKTOP
#endif

#if !defined(PLATFORM_DESKTOP)
	#error Platform not supported
#endif

#ifndef GEN_NO_PLATFORM_FUNCTIONS
namespace gen {
void* loadLibrary(const char* name);
void unloadLibrary(void* library);
void* getProcAddress(void* library, const char* proc);
};
#endif
