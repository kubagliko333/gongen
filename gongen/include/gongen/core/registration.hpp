/**
 * @file registration.hpp
 * @brief Adds GongEn's features related to registration.
 */
#pragma once

#include <string>
#include <unordered_map>
#include <vector>
#include <functional>
#include "export.hpp"
#include "hash.hpp"
#include "exception.hpp"

namespace gen {

namespace internal {
	class IdentifierFactory;
	class RegistryManager;
	class RegistryBase {
	};
}
class Core;
template<typename T>
class Registered;
template<typename R, Derived<Registered<R>> T>
class BasicRegistry;
template<class T>
using Registry = BasicRegistry<T, Registered<T>>;
class AdaptedObject;
class AdaptedTemplate;
template<typename T>
class LegalRegistryBuilder;

const std::string identifierDelimiter = "::";

/**
 * @brief An identifier used in the registration system.
 * 
 * Identifiers are constructed using the global gen::identifier() methods.
 * 
 * An identifier generally consists of the namespace id and the item id, where the namespace is used for distinguishing where the item comes from, and the item id identifies the item itself.
 * There can only exist one item with the same id in the same namespace, but in multiple namespaces there could exist multiple items of that id.
 */
class GEN_EXPORTED Identifier {
public:
	/**
	 * @brief Hashed @link literal @endlink value used in the @link Registry Registries @endlink as keys.
	 */
	const uint64_t hash;
	/**
	 * @brief The string representation of the identifier.
	 * 
	 * The format of the string is `namespaceId::itemId`.
	 */
	const std::string literal;

	/**
	 * @brief The namespace id of the identifier.
	 */
	const std::string namespaceId;
	/**
	 * @brief The item id of the identifier within the item's namespace.
	 */
	const std::string itemId;

protected:
	Identifier(uint64_t hash, std::string literal, std::string namespaceId, std::string itemId);

private:
	friend class internal::IdentifierFactory;
};

/**
 * @brief An extension of an Identifier that also contains id of the registry.
 * 
 * BlanketIdentifiers are constructed using the global gen::blanketIdentifier() methods.
 * 
 * The registry id reffers to the registry where the item of this identifier is supposed to be stored.
 */
class GEN_EXPORTED BlanketIdentifier : public Identifier {
public:
	/**
	 * @brief The registry id of the blanket identifier.
	 */
	const std::string registryId;

private:
	BlanketIdentifier(uint64_t hash, std::string literal, std::string registryId, std::string namespaceId, std::string itemId);

public:
	/**
	 * @brief Strip the BlanketIdentifier to a regular Identifiers.
	 * 
	 * @return Identifier An identifier with the exact same namespace and item ids.
	 */
	Identifier native();

private:
	friend class internal::IdentifierFactory;
};

/**
 * @brief Creates an Identifier.
 * 
 * @param namespaceId The namespace id.
 * @param itemId The item id.
 * @return Identifier The constructed Identifier.
 */
GEN_EXPORTED const Identifier identifier(std::string namespaceId, std::string itemId);

/**
 * @brief Creates an Identifier from a literal value.
 * 
 * @param literal The literal / string representation of the identifier.
 * @return Identifier The constructed Identifier.
 */
GEN_EXPORTED const Identifier identifier(std::string literal);

/**
 * @brief Creates a BlanketIdentifier from a registry id and a native Identifier in that registry.
 * 
 * @param registryId The registry id.
 * @param native The namespace and item ids.
 * @return BlanketIdentifier The constructed BlanketIdentifier.
 */
GEN_EXPORTED const BlanketIdentifier blanketIdentifier(std::string registryId, Identifier native);

/**
 * @brief Creates a BlanketIdentifier from a registry, namespace and item ids.
 * 
 * @param registryId The registry id.
 * @param namespaceId The namespace id.
 * @param itemId The item id.
 * @return BlanketIdentifier The constructed BlanketIdentifier.
 */
GEN_EXPORTED const BlanketIdentifier blanketIdentifier(std::string registryId, std::string namespaceId, std::string itemId);

/**
 * @brief Creates a BlanketIdentifier from a literal value.
 * 
 * @param literal The literal / string representation of the identifier.
 * @return BlanketIdentifier The constructed BlanketIdentifier.
 */
GEN_EXPORTED const BlanketIdentifier blanketIdentifier(std::string literal);

template<typename T>
struct GEN_EXPORTED RegisteredInfo {
private:
	const Identifier identifier;
	Registry<T>* const registry;

	RegisteredInfo(const Identifier& identifier, Registry<T>* const registry) : identifier(identifier), registry(registry) {
	}

	friend class Registered<T>;
	friend class BasicRegistry<T, Registered<T>>;
};

template<typename T>
using RegisterCallback = std::function<T*(const RegisteredInfo<T>&)>;

template<typename T>
class GEN_EXPORTED Registered {
public:
	T* const self;
	const Identifier identifier;
	Registry<T>* const registry;
protected:
	Registered(T* const self, const RegisteredInfo<T>& info) : self(self), identifier(info.identifier), registry(info.registry) {
	}
	virtual ~Registered() = default;
};

template<typename F>
class GEN_EXPORTED RegisteredForeign : public Registered<RegisteredForeign<F>> {
public:
	F* const foreign;

private:
	RegisteredForeign(const RegisteredInfo<RegisteredForeign<F>>& registeredInfo, F* foreign) : Registered<RegisteredForeign<F>>(this, registeredInfo), foreign(foreign) {
	}

public:
	static RegisterCallback<RegisteredForeign<F>> create(F* const foreign) {
		return [foreign](const RegisteredInfo<RegisteredForeign<F>>& registeredInfo) {
			return new RegisteredForeign<F>(registeredInfo, foreign);
		};
	}
};

struct GEN_EXPORTED BasicRegistrySettings {
	bool allowRemoving;
	bool allowClosing;
};

/**
 * @brief Holds items of the type T, with an identifier assigned to each of the items.
 * 
 * @tparam T The type of the stored items.
 */
template<typename T, Derived<Registered<T>> R>
class GEN_EXPORTED BasicRegistry : protected internal::RegistryBase {
protected:
	std::unordered_map<uint64_t, R*> map;
private:
	bool closed = false;
public:
	/**
	 * @brief Whether removing items from the registry is allowed or not.
	 * 
	 * If this is false, remove() will throw an exception.
	 */
	const bool allowRemoving;

	const bool allowClosing;

public:
	/**
	 * @brief Check if the registry contains an item with the given identifier hash.
	 * 
	 * @param hash The checked identifier's hash.
	 * @return true if an item with this identifier exists.
	 * @return false if there's no item with such identifier.
	 */
	virtual bool has(uint64_t hash) {
		return this->map.contains(hash);
	}
	/**
	 * @brief Check if the registry contains an item with the given identifier.
	 * 
	 * @param identifier The checked identifier.
	 * @return true if an item with this identifier exists.
	 * @return false if there's no item with such identifier.
	 */
	bool has(Identifier identifier) {
		return this->has(identifier.hash);
	}
	/**
	 * @brief Check if the registry contains an item with the given identifier hash and throw an exception otherwise.
	 * 
	 * Throws an ArgumentException if the registry does not contain the item.
	 * 
	 * @param hash The checked identifier's hash.
	 */
	void hasOrThrow(uint64_t hash) {
		if (!has(hash)) {
			throw ArgumentException("The registry does not contain an element with the given hash (or identifier)");
		}
	}
	/**
	 * @brief Check if the registry contains an item with the given identifier and throw an exception otherwise.
	 * 
	 * Throws an ArgumentException if the registry does not contain the item.
	 * 
	 * @param identifier The checked identifier.
	 */
	void hasOrThrow(Identifier identifier) {
		hasOrThrow(identifier.hash);
	}
	/**
	 * @brief Get the item with the given identifier hash.
	 * 
	 * @param hash Identifier's hash of the requested item.
	 * @return T* Pointer to the item.
	 */
	virtual T* get(uint64_t hash) {
		hasOrThrow(hash);
		return static_cast<T*>(this->map.at(hash));
	}
	/**
	 * @brief Get the item with the given identifier.
	 * 
	 * @param identifier Identifier of the requested item.
	 * @return T* Pointer to the item.
	 */
	T* get(Identifier identifier) {
		return get(identifier.hash);
	}
	T* add(Identifier identifier, RegisterCallback<T> callback) {
		return this->addInternal(identifier.hash, callback(RegisteredInfo<T>(identifier, this)));
	}
	/**
	 * @brief Removes an item from the registry.
	 * 
	 * @param hash The identifier's hash of the removed item.
	 */
	virtual void remove(uint64_t hash) {
		if (!allowRemoving) {
			throw FunctionCallException("This registry does not allow removing items!");
		}
		this->map.erase(hash);
	}
	/**
	 * @brief Remove an item from the registry.
	 * 
	 * @param identifier The identifier of the removed item.
	 */
	void remove(Identifier identifier) {
		this->remove(identifier.hash);
	}
	/**
	 * @brief Get amount of the registered entries.
	 * 
	 * @return unit64_t Size of the registry map. 
	 */
	const uint64_t size() {
		return this->map.size();
	}
	void closeRegistration() {
		if (!this->allowClosing) {
			throw ObjectStateException("Registry does not allow closing.");
		}
		this->closed = true;
	}
	/**
	 * @brief Get a copy of the registries map.
	 * 
	 * @return const std::unordered_map<uint64_t, Registered<T>*> The map.
	 */
	const std::unordered_map<uint64_t, Registered<T>*> getMap() {
		return this->map;
	}

protected:
	virtual T* addInternal(uint64_t hash, T* item) {
		if (has(hash)) {
			throw ArgumentException("The registry already contains an element with the given hash (or identifier).");
		}
		if (this->closed) {
			throw ObjectStateException("The registry is alredy closed; Adding more more items is forbidden.");
		}
		this->map.insert({hash, static_cast<R*>(item)});
		return item;
	}

	/**
	 * @brief Construct a new BasicRegistry object
	 * 
	 * @param allowRemoving Whether removing items from the registry is allowed or not.
	 */
	BasicRegistry(const BasicRegistrySettings& settings) : allowRemoving(settings.allowRemoving), allowClosing(settings.allowClosing) {
	}
};

class GEN_EXPORTED AdaptedObjectBuildHelper {
public:
	AdaptedTemplate const* _template;

	AdaptedObjectBuildHelper* setValue(std::string parameter, void* value);

	AdaptedObjectBuildHelper(AdaptedTemplate* _template);
private:
	std::unordered_map<uint32_t, void*> data;

	std::unordered_map<uint32_t, void*> validateAndGetData();

	friend class AdaptedObject;
};

class GEN_EXPORTED AdaptedObject {
private:
	const std::unordered_map<uint32_t, void*> data;
public:
	const AdaptedTemplate* const _template;

	void* getValue(std::string parameter) const;

protected:
	AdaptedObject(AdaptedObjectBuildHelper* helper);
};

class GEN_EXPORTED AdaptedTemplate : public Registered<AdaptedTemplate> {
public:
	const AdaptedTemplate* const parent;
	const std::vector<std::string> parameters;
	const std::vector<std::string> allParameters;

	AdaptedTemplate(const RegisteredInfo<AdaptedTemplate>& registeredInfo, const AdaptedTemplate* const parent, const std::vector<std::string> parameters);
};

class GEN_EXPORTED AdaptedTemplateBuilder : public RegisterCallback<AdaptedTemplateBuilder> {
private:
	const AdaptedTemplate* const parent;
	std::vector<std::string> parameters;

public:
	AdaptedTemplateBuilder(AdaptedTemplate *parent = nullptr);

	AdaptedTemplateBuilder* addParameter(std::string parameter);

	virtual AdaptedTemplate* create(const RegisteredInfo<AdaptedTemplate>& registeredInfo) const = 0;
};

class GEN_EXPORTED AdaptedRegistry : virtual public Registry<AdaptedTemplate> {
public:
	AdaptedRegistry(const BasicRegistrySettings& basicRegistrySettings) : Registry<AdaptedTemplate>(basicRegistrySettings) {
	}
};

/**
 * @brief A Registry managed by the Core.
 * 
 * Constructed using LegalRegistryBuilder.
 * 
 * @tparam T The type of the stored items.
 */
template<typename T>
class GEN_EXPORTED LegalRegistry : virtual public Registry<T> {
public:
	Core* core;
	/**
	 * @brief The id of the registry for the registries system.
	 * 
	 * This id is used in Core to distinguish the registries.
	 * It's also the value that the BlanketIdentifier refers to.
	 */
	const std::string id;

	/**
	 * @brief Hash of the @link id registry id @endlink.
	 */
	const uint32_t idHash;
protected:
	LegalRegistry(Core* core, std::string id, const BasicRegistrySettings& basicRegistrySettings) : Registry<T>(basicRegistrySettings), core(core), id(id), idHash(hash32((void*) id.c_str(), id.size())) {
	}

private:
	friend class internal::RegistryManager;
	friend class LegalRegistryBuilder<T>;
};

class GEN_EXPORTED LegalAdaptedRegistry : public AdaptedRegistry, public LegalRegistry<AdaptedTemplate> {
protected:
	LegalAdaptedRegistry(Core* core, std::string id, const BasicRegistrySettings& basicRegistrySettings) : Registry<AdaptedTemplate>(basicRegistrySettings), LegalRegistry<AdaptedTemplate>(core, id, basicRegistrySettings), AdaptedRegistry(basicRegistrySettings) {
	}

private:
	friend class LegalAdaptedRegistryBuilder;
};

/**
 * @brief A builder used to construct a LegalRegistry.
 * 
 * Pass this builder to the Core to build the registry and add it to that Core.
 * 
 * @tparam T Type of the Registry items.
 */
template<typename T>
class GEN_EXPORTED LegalRegistryBuilder {
protected:
	const std::string id;
	bool removingAllowed = true;
	bool closingAllowed = false;

public:
	/**
	 * @brief Create a new LegalRegistryBuilder.
	 * 
	 * @param id Registry id of the in-construction Registry.
	 */
	LegalRegistryBuilder(const std::string& id) : id(id) {
	}

	/**
	 * @brief Restrict removing items from the registry.
	 */
	LegalRegistryBuilder* restrictRemoving() {
		this->removingAllowed = false;
		return this;
	}

	/**
	 * @brief Allow removing items from the registry.
	 */
	LegalRegistryBuilder* allowRemoving() {
		this->removingAllowed = true;
		return this;
	}

	/**
	 * @brief Allow closing the registry.
	 * 
	 * This allows the use of BasicRegistry#close()
	 */
	LegalRegistryBuilder* allowClosing() {
		this->closingAllowed = true;
		return this;
	}

protected:
	BasicRegistrySettings makeBasicRegistrySettings() const {
		return BasicRegistrySettings{ .allowRemoving = this->removingAllowed, .allowClosing = this->closingAllowed };
	}

	virtual LegalRegistry<T>* build(Core* core) const {
		return new LegalRegistry<T>(core, this->id, this->makeBasicRegistrySettings());
	}

private:
	friend class internal::RegistryManager;
};

class GEN_EXPORTED LegalAdaptedRegistryBuilder : public LegalRegistryBuilder<AdaptedTemplate> {
public:
	LegalAdaptedRegistryBuilder(const std::string& id) : LegalRegistryBuilder<AdaptedTemplate>(id) {
	}
protected:
	virtual LegalAdaptedRegistry* build(Core* core) const {
		return new LegalAdaptedRegistry(core, this->id, this->makeBasicRegistrySettings());
	}
};

namespace internal {
	class RegistryManager {
	public:
		std::unordered_map<uint32_t, void*> registries;
		bool contains(uint32_t idHash);
		bool contains(std::string id);
		template<class T> LegalRegistry<T>* add(LegalRegistry<T>* registry) {
			if (contains(registry->idHash)) {
				throw ArgumentException("The registry manager already contains a registry with the given identifier!");
			}
			this->registries[registry->idHash] = registry;
			return registry;
		}
		template<class T> LegalRegistry<T>* add(LegalRegistryBuilder<T>* builder, Core* core) {
			return this->add(builder->build(core));
		}
		template<class T> LegalRegistry<T>* get(std::string id) {
			uint32_t idHash = hash32((void*) id.c_str(), id.size());
			if (!contains(idHash)) {
				throw ArgumentException("There's no such registry in the registry manager!");
			}
			return static_cast<LegalRegistry<T>*>(registries[idHash]);
		}
	};
}

}
