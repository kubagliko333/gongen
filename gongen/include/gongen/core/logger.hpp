/**
 * @file logger.hpp
 * @brief Adds logger functionality.
 */
#pragma once

#include "registration.hpp"
#include <vector>

namespace gen {

class Core;
class Logger;

/**
 * @brief Level of the logged lines.
 * 
 * Can be converted to string using logLevelName().
 */
enum class LogLevel {
	/**
	 * @brief Detailed information that might be meaningful for developers or experienced users.
	 */
	DEBUG,
	/**
	 * @brief Regular information intended for normal user in need of knownledge about general events.
	 */
	INFO,
	/**
	 * @brief Reporting potentially harmful events or situations.
	 */
	WARNING,
	/**
	 * @brief General error meaning something went wrong, but the application should be able to continue working.
	 */
	ERROR,
	/**
	 * @brief Severe error that might cause the application to crash.
	 */
	FATAL
};

/**
 * @brief Gets the literal version of a LogLevel.
 * 
 * The string is lowercase with the first letter uppercase.
 *
 * Result table:
 * DEBUG:	Debug
 * INFO:	Info
 * WARNING:	Warn
 * ERROR:	Error
 * FATAL:	Fatal
 *
 * @param level The level.
 * @return The literal adequate of the level.
 */
std::string logLevelName(LogLevel level);

/**
 * @brief A structure holding information about a logged line.
 */
struct GEN_EXPORTED LogInfo {
public:
	/**
	 * @brief Identifier of the logger used for this row.
	 */
	Identifier logger;
	/**
	 * @brief The time this row was created.
	 */
	int64_t timestamp;
	/**
	 * @brief Logging level of this line.
	 */
	LogLevel level;
	/**
	 * @brief The line's main text. The actual content.
	 */
	std::string text;
};

/**
 * @brief A callback interface for Logger to call when a line is logged.
 * 
 * The callbacks shall be registered in the `logger_callback` Registry.
 * The default logger callback is `gen::standard_out` which dumps out the logged lines to the standard output stream.
 * To create a logger you need to list identifiers of the callbacks that should register logged lines.
 */
class GEN_EXPORTED LoggerCallback {
public:
	/**
	 * @brief Will be called when a log.
	 * 
	 * @param info Info for the logged line.
	 * @param logger Logger that logs the line.
	 */
	virtual void onLog(const LogInfo& info, const Logger* logger) = 0;
};

/**
 * @brief Simple utility class supposed to log information about events in the application.
 * 
 * The logged lines are announced to every LoggerCallback, the callbacks's role is to compose the final messages and output/print/store them.
 * 
 * Loggers should be registered in the `logger` registry.
 * You can add build and add a logger using Engine::addLogger().
 */
class GEN_EXPORTED Logger : public Registered<Logger> {
public:
	/**
	 * @brief Related GongEn Core instance.
	 */
	Core* core;
	/**
	 * @brief Name of the logger.
	 */
	const std::string name;
private:
	const bool useGlobalCallbacks = true;
	const std::vector<uint64_t> callbacks;

public:
	/**
	 * @brief Logs a line.
	 * 
	 * @param level The level.
	 * @param string The content.
	 */
	void log(LogLevel level, const std::string& string) const;
	/**
	 * @brief Logs a debug line.
	 * 
	 * @param string The content.
	 */
	void debug(const std::string& string) const;
	/**
	 * @brief Logs an info line.
	 * 
	 * @param string The content.
	 */
	void info(const std::string& string) const;
	/**
	 * @brief Logs a warning line.
	 * 
	 * @param string The content.
	 */
	void warning(const std::string& string) const;
	/**
	 * @brief Logs an error line.
	 * 
	 * @param string The content.
	 */
	void error(const std::string& string) const;
	/**
	 * @brief Logs a fatal error line.
	 * 
	 * @param string The content.
	 */
	void fatal(const std::string& string) const;
private:
	Logger(const RegisteredInfo<Logger>& registeredInfo, Core* core, const std::string& name, std::vector<uint64_t> callbacks, bool useGlobalCallbacks);
	friend class LoggerBuilder;
	friend class Core;
};

/**
 * @brief A builder for the Logger class. Use in Engine::addLogger().
 */
class GEN_EXPORTED LoggerBuilder {
private:
	Core* core;
	Identifier identifier;
	std::string name;
	std::vector<uint64_t> callbacks;
	bool useGlobalCallbacks = true;
public:
	/**
	 * @brief Create a new LoggerBuilder.
	 * 
	 * @param core GongEn Core instance.
	 * @param identifier Identifier for the `logger` registry.
	 */
	LoggerBuilder(Core* core, Identifier identifier);

	/**
	 * @brief Name the logger.
	 * 
	 * Sets the name for the logger.
	 * 
	 * @param name The name.
	 * @return LoggerBuilder* Itself.
	 */
	LoggerBuilder* setName(std::string name);

	/**
	 * @brief Add a log callback.
	 * 
	 * Every callback will be called each time a line is logged.
	 * 
	 * @param callbackId Identifier from the `logger_callback` registry.
	 * @return LoggerBuilder* Itself.
	 */
	LoggerBuilder* addCallback(Identifier callbackId);

	/**
	 * @brief Set whether global callbacks (added by GongEn#addLoggerCallback()) will be used in this logger.
	 * 
	 * @param useGlobalCallbacks Whether to use global callbacks.
	 * @return LoggerBuilder* Itself.
	 */
	LoggerBuilder* setUseGlobalCallbacks(bool useGlobalCallbacks);

private:
	const RegisterCallback<Logger> build() const;
	friend class Core;
};

}