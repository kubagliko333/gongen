#pragma once

#include <stdint.h>
#include <gongen/graphics_api/graphics_device.hpp>
#include <limits.h>

#define PART_INVALID_ID UINT32_MAX

namespace gen {
	struct PartType {
		uint32_t id;
		uint32_t indexOffset;
		uint32_t vertexOffset;
		uint32_t indexCount;
	};

	template<typename V, typename P>
	class PartRenderer {
		bool needToUpload = false;
		std::vector<PartType> partTypes;
		std::vector<V> verticies;
		std::vector<uint32_t> indicies;

		gen::BufferPtr vertexBuffer;
		gen::BufferPtr indexBuffer;

		PartType getPartType(uint32_t id) {
			for (auto& p : partTypes) {
				if (p.id == id) return p;
			}
			PartType p;
			p.id = PART_INVALID_ID;
			return p;
		}

		void upload(gen::GraphicsDevice* device, const gen::CommandBufferPtr& commandBuffer) {
			gen::BufferDesc desc;
			desc.type = gen::BufferType::STATIC;
			desc.bindFlags = gen::BufferBindFlag::BUFFER_BIND_VERTEX_BUFFER;
			desc.size = verticies.size() * sizeof(V);

			vertexBuffer = device->createBuffer(desc);
			commandBuffer->updateBuffer(vertexBuffer, 0, desc.size, verticies.data());

			desc.bindFlags = gen::BufferBindFlag::BUFFER_BIND_INDEX_BUFFER;
			desc.size = indicies.size() * sizeof(uint32_t);

			indexBuffer = device->createBuffer(desc);
			commandBuffer->updateBuffer(indexBuffer, 0, desc.size, indicies.data());

			needToUpload = false;
		}

		std::vector<uint32_t> partsToRender_IDs;
		std::vector<P> partsToRender_instanceData;
	public:
		gen::VertexInputDesc inputLayout;

		void clearPartTypes() {
			partTypes.clear();
			verticies.clear();
			indicies.clear();
		}

		void clear() {
			partsToRender_IDs.clear();
			partsToRender_instanceData.clear();
		}

		void addPartType(uint32_t typeID, V* verticies, uint32_t verticiesAmount, uint32_t* indicies, uint32_t indiciesAmount) {
			PartType p;
			p.vertexOffset = this->verticies.size();
			p.indexOffset = this->indicies.size();
			p.indexCount = indiciesAmount;

			this->verticies.resize(this->verticies.size() + verticiesAmount);
			this->indicies.resize(this->indicies.size() + indiciesAmount);

			memcpy(&this->verticies.data()[p.vertexOffset], verticies, verticiesAmount * sizeof(V));
			memcpy(&this->indicies.data()[p.vertexOffset], indicies, indiciesAmount * sizeof(uint32_t));

			partTypes.push_back(p);
			needToUpload = true;
		}

		void addPart(uint32_t typeID, const P& partData) {
			partsToRender_IDs.push_back(typeID);
			partsToRender_instanceData.push_back(partData);
		}

		//TODO Batching and draw Indirect
		void draw(gen::GraphicsDevice *device, const gen::CommandBufferPtr& commandBuffer) {
			if (needToUpload) upload(commandBuffer);

			uint32_t instanceDataSize = partsToRender_IDs.size() * sizeof(P);
			if (instanceDataSize != 0) {
				gen::BufferPtr instanceDataBuffer = commandBuffer->createDynamicBuffer(gen::BufferBindFlag::BUFFER_BIND_VERTEX_BUFFER, instanceDataSize);
				commandBuffer->updateBuffer(instanceDataBuffer, 0, instanceDataSize, partsToRender_instanceData.data());
				
				for (uint32_t i = 0; i < partsToRender_IDs.size(); i++) {
					PartType type = getPartType(partsToRender_IDs[i]);
					if (type.id != PART_INVALID_ID) {
						commandBuffer->drawIndexed(type.indexCount, type.indexOffset, type.vertexOffset, 1, i);
					}
				}

				partsToRender_IDs.clear();
				partsToRender_instanceData.clear();
			}
		}
	};

#pragma pack(push, 1)
	struct vectorVertex {
		float x;
		float y;
		float zpos;
		uint16_t splineData1; //Normalized
		uint16_t splineData2; //Normalized
		uint16_t splineData3; //Normalized
		uint8_t type;
	};

	struct vectorPartInstanceData {
		float transformation[9]; //3x3 Matrix
		float gradientPoint[4];
		uint8_t colors[8];
		uint32_t flags;
	};
#pragma pack(pop)

	class VectorPartRenderer : public PartRenderer<vectorVertex, vectorPartInstanceData> {
	public:
		VectorPartRenderer() {
			inputLayout.bindingsAmount = 2;
			inputLayout.bindings[0].perInstance = false;
			inputLayout.bindings[0].stride = sizeof(gen::vectorVertex);
			inputLayout.bindings[1].perInstance = true;
			inputLayout.bindings[1].stride = sizeof(gen::vectorPartInstanceData);

			inputLayout.attributesAmount = 9;
			//Vertex Attributes
			inputLayout.attributes[0].binding = 0;
			inputLayout.attributes[0].format = gen::GPUFormat::RGB32_SFLOAT;
			inputLayout.attributes[0].offset = offsetof(gen::vectorVertex, x);

			inputLayout.attributes[1].binding = 0;
			inputLayout.attributes[1].format = gen::GPUFormat::RGB16_UNORM;
			inputLayout.attributes[1].offset = offsetof(gen::vectorVertex, splineData1);

			inputLayout.attributes[2].binding = 0;
			inputLayout.attributes[2].format = gen::GPUFormat::R8_UINT;
			inputLayout.attributes[2].offset = offsetof(gen::vectorVertex, type);

			//Per-Instance Attributes
			inputLayout.attributes[3].binding = 1;
			inputLayout.attributes[3].format = gen::GPUFormat::RGB32_SFLOAT;
			inputLayout.attributes[3].offset = offsetof(gen::vectorPartInstanceData, transformation[0]);

			inputLayout.attributes[4].binding = 1;
			inputLayout.attributes[4].format = gen::GPUFormat::RGB32_SFLOAT;
			inputLayout.attributes[4].offset = offsetof(gen::vectorPartInstanceData, transformation[3]);

			inputLayout.attributes[5].binding = 1;
			inputLayout.attributes[5].format = gen::GPUFormat::RGB32_SFLOAT;
			inputLayout.attributes[5].offset = offsetof(gen::vectorPartInstanceData, transformation[6]);

			inputLayout.attributes[6].binding = 1;
			inputLayout.attributes[6].format = gen::GPUFormat::RGBA32_SFLOAT;
			inputLayout.attributes[6].offset = offsetof(gen::vectorPartInstanceData, gradientPoint);

			inputLayout.attributes[7].binding = 1;
			inputLayout.attributes[7].format = gen::GPUFormat::RGBA8_UNORM;
			inputLayout.attributes[7].offset = offsetof(gen::vectorPartInstanceData, colors[0]);

			inputLayout.attributes[8].binding = 1;
			inputLayout.attributes[8].format = gen::GPUFormat::RGBA8_UNORM;
			inputLayout.attributes[8].offset = offsetof(gen::vectorPartInstanceData, colors[4]);

			inputLayout.attributes[8].binding = 1;
			inputLayout.attributes[8].format = gen::GPUFormat::R32_UINT;
			inputLayout.attributes[8].offset = offsetof(gen::vectorPartInstanceData, flags);
		}
	};
}
