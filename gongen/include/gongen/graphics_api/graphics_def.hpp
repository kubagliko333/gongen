#pragma once

#include <stdint.h>
#include <string.h>
#include <memory>
#include <vector>
#include <utility>
#include <string>

namespace gen {
	typedef std::shared_ptr<void> GPUResource;

	//Constants
	const static uint32_t MAX_DESCRIPTOR_SETS = 4;
	const static uint32_t MAX_DESCRIPTOR_SLOTS = 16;
	const static uint32_t MAX_COLOR_ATTACHMENTS = 8;

	//Buffer
	enum BufferBindFlag {
		BUFFER_BIND_TRANSFER_SRC = 1,
		BUFFER_BIND_TRANSFER_DST = 2,
		BUFFER_BIND_UNIFORM_BUFFER = 4,
		BUFFER_BIND_STORAGE_BUFFER = 8,
		BUFFER_BIND_INDEX_BUFFER = 16,
		BUFFER_BIND_VERTEX_BUFFER = 32,
		BUFFER_BIND_INDIRECT_BUFFER = 64
	};

	enum class BufferType {
		STATIC,
		READBACK,
		STAGING
	};

	enum class IndexType {
		UINT16,
		UINT32
	};

	struct BufferDesc {
		uint32_t bindFlags = 0;
		uint64_t size = 0;
		BufferType type = gen::BufferType::STATIC;
	};

	typedef GPUResource BufferPtr;

	//Texture
	enum TextureBindFlag {
		TEXTURE_BIND_TRANSFER_SRC = 1,
		TEXTURE_BIND_TRANSFER_DST = 2,
		TEXTURE_BIND_SAMPLED = 4,
		TEXTURE_BIND_STORAGE = 8,
		TEXTURE_BIND_COLOR_ATTACHMENT = 16,
		TEXTURE_BIND_DEPTH_STENCIL_ATTACHMENT = 32
	};

	enum class GPUFormat {
		R8_UNORM,
		R8_SNORM,
		R8_UINT,
		R8_SINT,

		R16_UNORM,
		R16_SNORM,
		R16_UINT,
		R16_SINT,
		R16_SFLOAT,

		R32_UINT,
		R32_SINT,
		R32_SFLOAT,

		RG8_UNORM,
		RG8_SNORM,
		RG8_UINT,
		RG8_SINT,

		RG16_UNORM,
		RG16_SNORM,
		RG16_UINT,
		RG16_SINT,
		RG16_SFLOAT,

		RG32_UINT,
		RG32_SINT,
		RG32_SFLOAT,

		RGBA8_UNORM_SRGB,
		RGBA8_UNORM,
		RGBA8_SNORM,
		RGBA8_UINT,
		RGBA8_SINT,

		RGBA16_UNORM,
		RGBA16_SNORM,
		RGBA16_UINT,
		RGBA16_SINT,
		RGBA16_SFLOAT,

		RGBA32_UINT,
		RGBA32_SINT,
		RGBA32_SFLOAT,

		D16_UNORM,
		D24_UNORM_S8_UINT,
		D32_SFLOAT,

		BC1_UNORM,
		BC1_SRGB,
		BC3_UNORM,
		BC3_SRGB,
		BC4_UNORM,
		BC4_SNORM,
		BC5_UNORM,
		BC5_SNORM,
		BC6H_UFLOAT,
		BC6H_SFLOAT,
		BC7_UNORM,
		BC7_SRGB,

		ETC2_RGB_UNORM,
		ETC2_RGB_SRGB,
		ETC2_RGBA_UNORM,
		ETC2_RGBA_SRGB,

		EAC_R_UNORM,
		EAC_R_SNORM,
		EAC_RG_UNORM,
		EAC_RG_SNORM,

		//TODO ASTC

		BGRA8_UNORM,
		BGRA8_SRGB,

		RGB8_UNORM_SRGB,
		RGB8_UNORM,
		RGB8_SNORM,
		RGB8_UINT,
		RGB8_SINT,

		RGB16_UNORM,
		RGB16_SNORM,
		RGB16_UINT,
		RGB16_SINT,
		RGB16_SFLOAT,

		RGB32_UINT,
		RGB32_SINT,
		RGB32_SFLOAT,

		TOTAL_TEXTURE_FORMATS
	};

	enum class TextureType {
		TEXTURE_1D,
		TEXTURE_2D,
		TEXTURE_3D,
		TEXTURE_CUBE_MAP
	};

	enum class TextureSamples {
		SAMPLE_COUNT_1 = 1,
		SAMPLE_COUNT_2 = 2,
		SAMPLE_COUNT_4 = 4,
		SAMPLE_COUNT_8 = 8,
		SAMPLE_COUNT_16 = 16,
		SAMPLE_COUNT_32 = 32,
		SAMPLE_COUNT_64 = 64
	};

	struct TextureDesc {
		uint32_t bindFlags = 0;
		GPUFormat format = gen::GPUFormat::RGBA8_UNORM;
		TextureType type = gen::TextureType::TEXTURE_2D;

		uint32_t width = 1;
		uint32_t height = 1;
		uint32_t depth = 1;

		uint32_t layers = 1;
		uint32_t mipLevels = 1;
		TextureSamples samples = TextureSamples::SAMPLE_COUNT_1;
	};

	typedef GPUResource TexturePtr;

	//RenderPass
	enum class RenderPassLoadOp {
		CLEAR,
		LOAD,
		DONT_CARE
	};

	enum class RenderPassStoreOp {
		STORE,
		DONT_CARE
	};

	struct RenderPassAttachment {
		TexturePtr texture = nullptr;
		RenderPassLoadOp loadOp = gen::RenderPassLoadOp::DONT_CARE;
		RenderPassStoreOp storeOp = gen::RenderPassStoreOp::DONT_CARE;
		float clearR, clearG, clearB, clearA = 0.0f;
		float clearD = 0.0f;
	};

	struct RenderPassDesc {
		RenderPassAttachment colorAttachments[gen::MAX_COLOR_ATTACHMENTS];
		RenderPassAttachment depthStencilAttachment;
	};

	//Shader(Common)
	enum class ShaderStage {
		VERTEX_SHADER,
		HULL_SHADER,
		DOMAIN_SHADER,
		GEOMETRY_SHADER,
		PIXEL_SHADER,
		COMPUTE_SHADER,
		TOTAL_SHADER_STAGES
	};

	enum ShaderStageFlag {
		VERTEX_SHADER = 1,
		HULL_SHADER = 2,
		DOMAIN_SHADER = 4,
		GEOMETRY_SHADER = 8,
		PIXEL_SHADER = 16,
		COMPUTE_SHADER = 32
	};

	enum class ShaderType {
		SPIRV = 1,
		GLSL330 = 2,
		ELSL300 = 4,
		DXBC = 8
	};

	//Shader(Metadata)
	enum class SPIRVDescriptorType {
		INVALID = 0,
		SAMPLER = 1,
		SEPARATE_IMAGE = 2,
		STORAGE_IMAGE = 3,
		UNIFORM_BUFFER = 4,
		STORAGE_BUFFER = 5,

		TOTAL_SPIRV_DESCRIPTOR_TYPES
	};

	const static uint8_t INVALID_SHADER_SLOT = UINT8_MAX;

	struct CombinedImageSamplerMetadata {
		uint8_t imageBinding;
		uint8_t samplerBinding;
		uint32_t spirvIndex;
	};

	struct ShaderMetadata {
		SPIRVDescriptorType descriptors[MAX_DESCRIPTOR_SETS][MAX_DESCRIPTOR_SLOTS];
		//Used by OpenGL and DirectX 11 backends
		uint8_t remapTable[MAX_DESCRIPTOR_SETS][MAX_DESCRIPTOR_SLOTS];
		//Used by OpenGL backend
		CombinedImageSamplerMetadata textureCombiningTable[MAX_DESCRIPTOR_SLOTS];
		uint32_t descriptorSPIRVIndexes[MAX_DESCRIPTOR_SETS][MAX_DESCRIPTOR_SLOTS];

		ShaderMetadata() {
			memset(descriptors, 0, MAX_DESCRIPTOR_SETS * MAX_DESCRIPTOR_SLOTS * sizeof(gen::SPIRVDescriptorType));
			memset(descriptorSPIRVIndexes, INVALID_SHADER_SLOT, MAX_DESCRIPTOR_SETS * MAX_DESCRIPTOR_SLOTS * sizeof(uint32_t));
			memset(remapTable, INVALID_SHADER_SLOT, MAX_DESCRIPTOR_SETS * MAX_DESCRIPTOR_SLOTS * sizeof(uint8_t));
			memset(textureCombiningTable, INVALID_SHADER_SLOT, MAX_DESCRIPTOR_SLOTS * sizeof(CombinedImageSamplerMetadata));
		}
	};
	//Shaders
	struct ShaderStage_t {
		ShaderMetadata metadata;
		std::vector<uint32_t> spirv;
		std::vector<uint8_t> dxbc;
		std::string glslCode;
	};

	struct ShaderDesc {
		ShaderType type = gen::ShaderType::SPIRV;
		uint32_t stageFlags = 0;

		ShaderStage_t stages[(size_t)gen::ShaderStage::TOTAL_SHADER_STAGES];
	};

	typedef GPUResource ShaderPtr;

	//Sampler

	enum class TextureFilter {
		NEAREST,
		LINEAR
	};

	enum class WrapMode {
		REPEAT,
		MIRRORED_REPEAT,
		CLAMP_TO_EDGE
	};

	struct SamplerDesc {
		TextureFilter magFiler = TextureFilter::LINEAR;
		TextureFilter minFiler = TextureFilter::LINEAR;
		TextureFilter mipFilter = TextureFilter::LINEAR;

		WrapMode wrapX = WrapMode::CLAMP_TO_EDGE;
		WrapMode wrapY = WrapMode::CLAMP_TO_EDGE;
		WrapMode wrapZ = WrapMode::CLAMP_TO_EDGE;

		bool anisotropicFiltering = false;
		float anisotropicLevel = 1.0f;
	};

	//Pipeline

	const uint32_t MAX_VERTEX_BINDINGS = 16;
	const uint32_t MAX_VERTEX_ATTRIBUTES = 16;
	const uint32_t MAX_VERTEX_BUFFERS = 8;

	struct VertexBinding {
		uint32_t stride;
		bool perInstance;
	};

	struct VertexAttribute {
		uint32_t binding;
		gen::GPUFormat format;
		uint32_t offset;
	};

	struct VertexInputDesc {
		uint32_t bindingsAmount = 1;
		VertexBinding bindings[MAX_VERTEX_BINDINGS];
		uint32_t attributesAmount = 1;
		VertexAttribute attributes[MAX_VERTEX_ATTRIBUTES];
	};

	enum class PrimitiveTopology {
		POINT_LIST,
		LINE_LIST,
		LINE_STRIP,
		TRIANGLE_LIST,
		TRIANGLE_STRIP
	};

	struct InputAssemblyDesc {
		PrimitiveTopology primitiveTopology = PrimitiveTopology::TRIANGLE_LIST;
		bool primitiveRestart = false;
		uint32_t tesselationControlPoints = 0;
	};

	struct ViewportDesc {
		uint32_t x = 0;
		uint32_t y = 0;
		uint32_t width = 0;
		uint32_t height = 0;
		float minDepth = 0.0f;
		float maxDepth = 1.0f;
	};

	struct ScissorDesc {
		uint32_t x = 0;
		uint32_t y = 0;
		uint32_t width = 0;
		uint32_t height = 0;
	};

	enum class PolygonMode {
		FILL,
		LINE,
		POINT
	};

	enum class CullMode {
		NONE,
		BACK,
		FRONT,
		FRONT_AND_BACK
	};

	struct RasterizerDesc {
		bool depthClamp = false;
		PolygonMode polygonMode = PolygonMode::FILL;
		CullMode cullMode = CullMode::NONE;
		bool frontFaceCCW = true;
	};

	enum class CompareOp {
		NEVER,
		LESS,
		EQUAL,
		LESS_OR_EQUAL,
		GREATER,
		NOT_EQUAL,
		GREATER_OR_EQUAL,
		ALWAYS
	};

	struct DepthStencilDesc {
		bool depthTest = false;
		bool depthWrite = true;
		CompareOp compareOp = CompareOp::LESS;
	};

	enum class BlendFactor {
		ZERO,
		ONE,

		SRC_COLOR,
		ONE_MINUS_SRC_COLOR,
		DST_COLOR,
		ONE_MINUS_DST_COLOR,

		SRC_ALPHA,
		ONE_MINUS_SRC_ALPHA,
		DST_ALPHA,
		ONE_MINUS_DST_ALPHA,

		CONSTANT_COLOR,
		ONE_MINUS_CONSTANT_COLOR,
		CONSTANT_ALPHA,
		ONE_MINUS_CONSTANT_ALPHA,

		SRC_ALPHA_SATURATE,
		SRC1_COLOR,
		ONE_MINUS_SRC1_COLOR,
		SRC1_ALPHA,
		ONE_MINUS_SRC1_ALPHA
	};

	struct BlendAttachmentDesc {
		bool enabled = false;
		BlendFactor srcColor = BlendFactor::SRC_ALPHA;
		BlendFactor dstColor = BlendFactor::ONE_MINUS_SRC_ALPHA;
		BlendFactor srcAlpha = BlendFactor::ONE;
		BlendFactor dstAlpha = BlendFactor::ZERO;
		bool writeR = true;
		bool writeG = true;
		bool writeB = true;
		bool writeA = true;
	};

	struct BlendDesc {
		BlendAttachmentDesc attachments[gen::MAX_COLOR_ATTACHMENTS];
		float blendConstantR = 0.0f;
		float blendConstantG = 0.0f;
		float blendConstantB = 0.0f;
		float blendConstantA = 0.0f;
	};

	struct PipelineDesc {
		VertexInputDesc vertexInput;
		InputAssemblyDesc inputAssembly;
		ViewportDesc viewport;
		ScissorDesc scissor;
		RasterizerDesc rasterizer;
		DepthStencilDesc depthStencil;
		BlendDesc blend;
		ShaderPtr shader;
		bool dynamicScissor = false;
		bool dynamicViewport = false;
	};
}
