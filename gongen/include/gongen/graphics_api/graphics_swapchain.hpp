#pragma once

#include "graphics_def.hpp"

namespace gen {
	class CommandBuffer;

	class Swapchain {
	public:
		virtual ~Swapchain() {}
		//virtual void display() = 0;
		//virtual TexturePtr getTexture(bool depthTexture) = 0;
		virtual gen::RenderPassDesc& getRenderPass(gen::CommandBuffer* commandBuffer) = 0;
		virtual void handleResize(uint32_t x, uint32_t y) = 0;
		virtual bool isMinimized() = 0;
	};
}
