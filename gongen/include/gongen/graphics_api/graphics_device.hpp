#pragma once

#include "graphics_def.hpp"
#include "graphics_swapchain.hpp"

namespace gen {
	struct TextureOffset {
		uint32_t level = 0;
		uint32_t layerOffset = 0;
		uint32_t layers = 1;
		uint32_t x = 0;
		uint32_t y = 0;
		uint32_t z = 0;
	};

#pragma pack(push, 1)
	struct drawIndirectCommand {
		uint32_t vertexCount;
		uint32_t instanceCount;
		uint32_t firstVertex;
		uint32_t firstInstance;
	};

	struct drawIndexedIndirectCommand {
		uint32_t indexCount;
		uint32_t instanceCount;
		uint32_t firstIndex;
		int32_t vertexOffset;
		uint32_t firstInstance;
	};
#pragma pack(pop)

	class CommandBuffer {
	public:
		virtual ~CommandBuffer() {}

		virtual BufferPtr createDynamicBuffer(uint32_t bindFlags, uint64_t size, bool gpuAccessOptimal = true) = 0;
		virtual void updateBuffer(const BufferPtr &buffer, uint64_t offset, uint64_t size, void* data) = 0;
		virtual void updateTexture(const TexturePtr &texture, TextureOffset offset, uint32_t sizeX, uint32_t sizeY, uint32_t sizeZ, void* data) = 0;

		virtual void beginRenderPass(const gen::RenderPassDesc& desc) = 0;
		virtual void endRenderPass() = 0;

		virtual void bindPipeline(const gen::PipelineDesc& desc) = 0;
		virtual void bindVertexBuffers(const gen::BufferPtr& vertexBuffer, uint32_t slot, uint64_t offset) = 0;
		virtual void bindIndexBuffer(const gen::BufferPtr& indexBuffer, uint64_t offset, gen::IndexType type) = 0;
		virtual void setScissor(const gen::ScissorDesc& desc) = 0;
		virtual void setViewport(const gen::ViewportDesc& desc) = 0;

		virtual void setUniformBuffer(uint32_t set, uint32_t slot, const gen::BufferPtr& buffer, uint64_t offset, uint64_t size) = 0;
		virtual void setTexture(uint32_t set, uint32_t slot, const gen::TexturePtr& texture) = 0;
		virtual void setSampler(uint32_t set, uint32_t slot, const gen::SamplerDesc& desc) = 0;

		virtual void draw(uint32_t vertexCount, uint32_t firstVertex = 0, uint32_t instanceCount = 1, uint32_t firstInstance = 0) = 0;
		virtual void drawIndexed(uint32_t indexCount, uint32_t firstIndex = 0, int32_t vertexOffset = 0, uint32_t instanceCount = 1, uint32_t firstInstance = 0) = 0;
		virtual void drawIndirect(const gen::BufferPtr& buffer, uint32_t drwaCount, uint32_t offset = 0) = 0;
		virtual void drawIndexedIndirect(const gen::BufferPtr& buffer, uint32_t drwaCount, uint32_t offset = 0) = 0;

		virtual void execute(CommandBuffer* waitFor) = 0;
		virtual bool executionDone() = 0;
		virtual void waitForExecution() = 0;

		virtual void display(Swapchain* swapchian) = 0;
	};

	typedef std::shared_ptr<CommandBuffer> CommandBufferPtr;

	class GraphicsDevice {
	public:
		virtual BufferPtr createBuffer(const gen::BufferDesc& desc) = 0;
		virtual TexturePtr createTexture(const gen::TextureDesc& desc) = 0;
		virtual ShaderPtr createShader(const gen::ShaderDesc& desc) = 0;

		virtual CommandBufferPtr getCommandBuffer() = 0;
	};

	/*struct BindedResource {
		uint32_t slot = 0;
		gen::SPIRVDescriptorType type = gen::SPIRVDescriptorType::INVALID;

		BufferPtr buffer;
		TexturePtr texture;
		SamplerPtr sampler;
	};

	struct BindingState {
		std::vector<BindedResource> resources[gen::MAX_DESCRIPTOR_SETS];

		void clear() {
			for(uint32_t i = 0; i < gen::MAX_DESCRIPTOR_SETS; i++) resources[i].clear();
		}
		void bindUniformBuffer(uint32_t set, uint32_t slot, BufferPtr buffer) {
			BindedResource res = { slot, gen::SPIRVDescriptorType::UNIFORM_BUFFER };
			res.buffer = buffer;
			resources[set].push_back(res);
		}
		void bindSampler(uint32_t set, uint32_t slot, SamplerPtr sampler) {
			BindedResource res = { slot, gen::SPIRVDescriptorType::SAMPLER };
			res.sampler = sampler;
			resources[set].push_back(res);
		}
		void bindTexture(uint32_t set, uint32_t slot, TexturePtr texture) {
			BindedResource res = { slot, gen::SPIRVDescriptorType::SEPARATE_IMAGE };
			res.texture = texture;
			resources[set].push_back(res);
		}
	};

	class GraphicsContext {
	public:
		virtual void clear() = 0;
		virtual uint64_t execute(uint64_t waitFor) = 0;
		virtual bool isDone(uint64_t execution) = 0;
		virtual void waitUntilIsDone(uint64_t execution) = 0;

		//RenderPass
		virtual void beginRenderPass(const RenderPassPtr &renderPass) = 0;
		virtual void endRenderPass() = 0;

		//Transfer(can't be used inside render pass)
		virtual void copyBuffer(const BufferPtr &srcBuffer, const BufferPtr &dstBuffer, uint64_t srcOffset, uint64_t dstOffset, uint64_t size) = 0;
		virtual void copyTexture(const TexturePtr &srcTexture, const TexturePtr &dstTexture, TextureOffset srcOffset, TextureOffset dstOffset, uint32_t sizeX, uint32_t sizeY, uint32_t sizeZ) = 0;
		virtual void copyTextureToBuffer(const TexturePtr &srcTexture, const BufferPtr &dstBuffer, TextureOffset srcOffset, uint64_t dstOffset, uint32_t sizeX, uint32_t sizeY, uint32_t sizeZ) = 0;
		virtual void copyBufferToTexture(const BufferPtr &srcBuffer, const TexturePtr &dstTexture, uint64_t srcOffset, TextureOffset dstOffset, uint32_t sizeX, uint32_t sizeY, uint32_t sizeZ) = 0;

		//Updating(updateBuffer and updateTexture can't be used inside render pass)
		virtual void updateBuffer(const BufferPtr &buffer, uint64_t offset, uint64_t size, void* data) = 0;
		virtual void* map(BufferPtr& buffer, MapType mapType) = 0;
		virtual void unmap(const BufferPtr &buffer) = 0;
		virtual void updateTexture(const TexturePtr &texture, TextureOffset offset, uint32_t sizeX, uint32_t sizeY, uint32_t sizeZ, void* data) = 0;

		//Setting rendering state(only insider render pass)
		virtual void setVertexBuffer(const BufferPtr &buffer, uint64_t slot = 0, uint64_t offset = 0) = 0;
		virtual void setIndexBuffer(const BufferPtr& buffer, IndexType indexType) = 0;
		virtual void setPipeline(const PipelinePtr &pipeline) = 0;
		virtual void setBindingState(const BindingState &state) = 0;
		virtual void setScissor(const ScissorDesc &scissor) = 0;
		virtual void setViewport(const ViewportDesc& viewport) = 0;

		//Drawing(only inside render pass)
		virtual void draw(uint32_t vertexCount, uint32_t firstVertex = 0, uint32_t instanceCount = 1, uint32_t firstInstance = 0) = 0;
		virtual void drawIndexed(uint32_t indexCount, uint32_t firstIndex = 0, uint32_t firstVertex = 0, uint32_t instanceCount = 1, uint32_t firstInstance = 0) = 0;
		virtual void drawIndirect(const BufferPtr &buffer, uint32_t drawAmount, uint32_t offset = 0) = 0;
		virtual void drawIndexedIndirect(const BufferPtr &buffer, uint32_t drawAmount, uint32_t offset = 0) = 0;
	};

	class GraphicsDevice {
	public:
		virtual BufferPtr createBuffer(const gen::BufferDesc& desc) = 0;
		virtual TexturePtr createTexture(const gen::TextureDesc& desc) = 0;
		virtual ShaderPtr createShader(const gen::ShaderDesc& desc) = 0;
		virtual RenderPassPtr createRenderPass(const gen::RenderPassDesc& desc) = 0;
		virtual SamplerPtr createSampler(const gen::SamplerDesc& desc) = 0;
		virtual PipelinePtr createPipeline(const gen::PipelineDesc& desc) = 0;

		virtual GraphicsContext* getMainContext() = 0;
	};*/

	//Return 0 for formats with 1/2 BBP, doesn't work for ASTC Formats
	uint64_t getGPUFormatBPP(gen::GPUFormat format);

	//Get texture size in bytes
	uint64_t getTextureSize(gen::GPUFormat format, uint32_t width = 1, uint32_t height = 1, uint32_t depth = 1, uint32_t layers = 1);

	//Get amount of mip levels for texture of fallowing size and format
	uint64_t getAmountOfTextureMipLevels(gen::GPUFormat format, uint32_t width = 1, uint32_t height = 1);

	//Check if GPUFormat is compressed
	bool isFormatCompressed(gen::GPUFormat format);
}
