#pragma once

#include "graphics_device.hpp"
#include "graphics_swapchain.hpp"
#include <string>
#include <vector>
#include <gongen/window_api/window.hpp>

namespace gen {
	struct GraphicsDeviceLimits {
		uint32_t maxTextureDimension1D;
		uint32_t maxTextureDimension2D;
		uint32_t maxTextureDimension3D;
		uint32_t maxTextureDimensionCube;
		uint32_t maxTextureArrayLayers;

		uint32_t maxUniformBufferRange;
		uint32_t maxStorageBufferRange;

		uint32_t maxColorAttachments;

		uint32_t maxComputeWorkGroupCount[3];
		uint32_t maxDrawIndirectCount;

		uint32_t maxViewports;
	};

	struct GraphicsDeviceFeatures {
		bool geometryShader = false;
		bool tesselationShader = false;
		bool computeShader = false;

		bool multiDrawIndirect = false;
		bool drawIndirectFirstInstance = false;
		bool firstInstance = false;
		bool firstVertex = false;
		bool depthClamp = false;
		bool independentBlend = false;
		bool multiViewport = false;

		bool vertexPipelineStoresAndAtomics = false;
		bool fragmentStoresAndAtomics = false;

		bool samplerAnisotropy = false;
		bool imageCubeArray = false;

		bool textureCompressionETC2 = false;
		bool textureCompressionASTC_LDR = false;
		bool textureCompressionBC1_3 = false;
		bool textureCompressionBC4_5 = false;
		bool textureCompressionBC6_7 = false;

		bool shaderFloat64 = false;
		bool shaderInt64 = false;
		bool shaderInt16 = false;
	};

	enum class GraphicsDeviceType {
		UNKNOWN,
		INTEGRATED,
		DISCRETE,
		VIRTUAL,
		SOFTWARE
	};

	struct GraphicsDeviceInfo {
		std::string name;
		GraphicsDeviceType type;
		bool defferedContexts;
		bool multipleSwapchains;
		GraphicsDeviceLimits limits;
		GraphicsDeviceFeatures features;
		uint32_t vram;
		uint64_t reserved;
	};

	struct GraphicsAPICreateInfo {
		bool debugMode = false;
		bool waitAfterExecute = false;
	};

	class GraphicsAPI {
	public:
		std::vector<GraphicsDeviceInfo> devices;

		virtual ShaderType getShaderType() = 0;

		virtual ~GraphicsAPI() {}
		virtual void init(gen::WindowAPI* windowAPI, GraphicsAPICreateInfo createInfo) = 0;
		virtual GraphicsDevice* createDevice(GraphicsDeviceInfo* deviceInfo, Window* window, Swapchain** swaphain) = 0;
	};
}
