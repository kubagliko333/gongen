/**
 * @file gcf.hpp
 * @brief GongEn Container Format(GCF) defines nad functions to open and create gcf files
 */
#pragma once

#define GCF_MAGIC 1179009863 //"GCFF"
#define GCF_VERSION 1
#define GCF_COMPATIBLE_VERSION 1

#include <gongen/core/export.hpp>
#include <stdint.h>
#include <tinyxml2.h>
#include <gongen/core/hash.hpp>
#include <gongen/core/file.hpp>
#include <string>
#include <gongen/vfs/vfs.hpp>

namespace gen {
	enum GCFXMLFlag {
		GCF_XML_ZSTD_COMPRESSION = 1
	};

#pragma pack(push, 1)
	struct GCFHeader1 {
		uint32_t magic = GCF_MAGIC;
		uint32_t headerSize = sizeof(GCFHeader1);
		uint16_t version = GCF_VERSION;
		uint16_t compatibleVersion = GCF_COMPATIBLE_VERSION;
		uint16_t flags = 0;
		uint16_t xmlFlags = 0;

		uint64_t xmlOffset = 0;
		uint64_t xmlUncompressedSize = 0;
		uint64_t xmlCompressedSize = 0;
	};

	struct GCFHeader1WithChecksum {
		GCFHeader1 header;
		Hash128 checksum;
	};
#pragma pack(pop)
	enum class CompressionMode {
		UNCOMPRESSED = 0,
		ZSTD = 1
	};

	struct GCFHandle {
		GCFHandle() {}

		bool useVFS = false;
		gen::FileHandle fhandle;
		//gen::VFSHandlePtr vfsHandle;

		uint8_t* mappedFile = nullptr;
		bool loaded = false;
		GCFHeader1WithChecksum header;
		tinyxml2::XMLDocument xmlDoc;
		//Only when loading
		uint8_t* data = nullptr;
		//Only when saving
		uint8_t* writtenData = nullptr;
		uint64_t writtenDataSize = 0;
		uint64_t writtenDataPtr = 0;
		//Temp stuff
		uint8_t* tempMemory = nullptr;
		uint64_t tempMemorySize = 0;
	};

	GEN_EXPORTED bool openGCF(GCFHandle& handle, const std::string& filepath);

	GEN_EXPORTED void closeGCF(GCFHandle& handle);

	GEN_EXPORTED void freeGCF(GCFHandle& handle);

	GEN_EXPORTED void gcfGetData(GCFHandle& handle, void* data, uint64_t offset, uint64_t compressedSize, uint64_t uncompressedSize, gen::CompressionMode compressionMode = CompressionMode::UNCOMPRESSED);

	GEN_EXPORTED uint64_t gcfAppendData(GCFHandle& handle, void *data, uint64_t dataSize, uint64_t *dataSizeOut, gen::CompressionMode compressionMode = CompressionMode::UNCOMPRESSED);

	GEN_EXPORTED void saveGCF(GCFHandle& handle, const std::string& filepath);
}
