/**
 * @file window.hpp
 * @brief Adds Window API.
 */
#pragma once

#include <gongen/core/export.hpp>
#include <stdint.h>
#include <string>
#include <vector>
#include <functional>

namespace gen {

enum class WindowType {
	WINDOWED,
	FULLSCREEN
};

struct WindowCreateInfo {
	WindowType type = WindowType::WINDOWED;
	uint16_t width = 800;
	uint16_t height = 600;
	uint32_t windowID = 0;
	std::string title = "GongEn Window";
	bool decorations = true;
};

enum class KeyButton {
	SPACE,
	APOSTROPHE,
	COMMA,
	MINUS,
	PERIOD,
	SLASH,
	KEY_0,
	KEY_1,
	KEY_2,
	KEY_3,
	KEY_4,
	KEY_5,
	KEY_6,
	KEY_7,
	KEY_8,
	KEY_9,
	SEMICOLON,
	EQUAL,
	A,
	B,
	C,
	D,
	E,
	F,
	G,
	H,
	I,
	J,
	K,
	L,
	M,
	N,
	O,
	P,
	Q,
	R,
	S,
	T,
	U,
	V,
	W,
	X,
	Y,
	Z,
	LEFT_BRACKET,
	BACKSLASH,
	RIGHT_BRACKET,
	GRAVE_ACCENT,
	ESCAPE,
	ENTER,
	TAB,
	BACKSPACE,
	INSERT,
	DELETE,
	RIGHT,
	LEFT,
	DOWN,
	UP,
	PAGE_UP,
	PAGE_DOWN,
	HOME,
	END,
	CAPS_LOCK,
	SCROLL_LOCK,
	NUM_LOCK,
	PRINT_SCREEN,
	PAUSE,
	F1,
	F2,
	F3,
	F4,
	F5,
	F6,
	F7,
	F8,
	F9,
	F10,
	F11,
	F12,
	F13,
	F14,
	F15,
	F16,
	F17,
	F18,
	F19,
	F20,
	F21,
	F22,
	F23,
	F24,
	F25,
	KP_0,
	KP_1,
	KP_2,
	KP_3,
	KP_4,
	KP_5,
	KP_6,
	KP_7,
	KP_8,
	KP_9,
	KP_DECIMAL,
	KP_DIVIDE,
	KP_MULTIPLY,
	KP_SUBTRACT,
	KP_ADD,
	KP_ENTER,
	KP_EQUAL,
	LEFT_SHIFT,
	LEFT_CONTROL,
	LEFT_ALT,
	RIGHT_SHIFT,
	RIGHT_CONTROL,
	RIGHT_ALT,
	INVALID,

	TOTAL_KEYS
};

enum class MouseButton {
	LEFT,
	RIGHT,
	MIDDLE,
	INVALID,

	TOTAL_BUTTONS
};

enum class KeyAction {
	PRESS,
	RELEASE,
	REPEAT,
	INVALID
};

enum KeyMods {
	KEY_MOD_SHIFT = 1,
	KEY_MOD_CONTROL = 2,
	KEY_MOD_ALT = 4
};

enum class EventType {
	KEY,
	CHARACTER,

	MOUSE_POSITION,
	MOUSE_BUTTON,
	MOUSE_WHEEL,

	FILE_DROP,

	WINDOW_SIZE,
	WINDOW_FOCUS,
	WINDOW_CLOSE
};

enum class Cursor {
	ARROW,
	IBEAM,
	CROSSHAIR,
	HAND,
	RESIZE_EW,
	RESIZE_NS,
	RESIZE_NWSE,
	RESIZE_NESW,
	RESIZE_ALL,
	NOT_ALLOWED
};

namespace internal {
	struct keyEvent {
		KeyButton key;
		KeyAction action;
		uint32_t mods;
	};
	struct characterEvent {
		uint32_t codepoint;
	};
	struct mousePositionEvent {
		float x, y;
	};
	struct mouseButtonEvent {
		MouseButton button;
		KeyAction action;
	};
	struct mouseWheelEvent {
		float x, y;
	};
	struct resizeEvent {
		uint32_t w, h;
	};
	struct focusEvent {
		bool focus;
	};
	struct windowEvent {
		uint32_t windowID;
		union {
			resizeEvent resize;
			focusEvent focus;
		};
	};
	struct fileDropEvent {
		std::vector<std::string> paths;
	};
}

struct Event {
	EventType type;
	union {
		gen::internal::keyEvent key;
		gen::internal::characterEvent character;
		gen::internal::mousePositionEvent mousePosition;
		gen::internal::mouseButtonEvent mouseButton;
		gen::internal::mouseWheelEvent mouseWheel;
		gen::internal::windowEvent window;
	};
	gen::internal::fileDropEvent fileDrop;
};

enum class WindowGraphicsFunction {
	VK_GET_REQUIRED_EXTENSIONS = 1,//WindowAPI
	VK_CREATE_SURFACE = 2, //Window
	VK_QUEUE_PRESENTATION_SUPPORT = 3, //WindowAPI

	DX_GET_HWND = 100, //Window

	GET_FRAMEBUFFER_SIZE = 1000 //Window
};

class GEN_EXPORTED WindowAPI;

/**
 * @brief Represents a window in the WindowAPI.
 */
class GEN_EXPORTED Window {
public:
	virtual ~Window() {}
	/**
	 * @brief Check whether the window is open.
	 * 
	 * @return true if the window is open.
	 * @return false if the window is closed.
	 */
	virtual bool isOpen() = 0;
	/**
	 * @brief Close the window.
	 */
	virtual void close() = 0;
	/**
	 * @brief Change the window title.
	 * 
	 * @param title The new title.
	 */
	virtual void setTitle(const std::string& title) = 0;
	/**
	 * @brief Change the window size.
	 * 
	 * @param width The new width.
	 * @param height The new height.
	 */
	virtual void resize(uint16_t width, uint16_t height) = 0;
	/**
	 * @brief Change the text in clipboard.
	 *
	 * @param content New clipboard content.
	 */
	virtual void setClipboard(const std::string& content) = 0;
	/**
	 * @brief Get current text in clipboard.
	 *
	 * @return Clipboard content
	 */
	virtual std::string getClipboard() = 0;

	virtual uint64_t graphicsFunction(WindowGraphicsFunction func, uint64_t userData, uint64_t userData2, uint64_t userData3, uint64_t& out2) = 0;
	
	virtual gen::WindowAPI* getWindowAPI() = 0;

	virtual uint32_t getWidth() = 0;

	virtual uint32_t getHeight() = 0;

	virtual void setCursor(gen::Cursor cursor) = 0;

	virtual void setMousePosition(uint32_t x, uint32_t y) = 0;
};

/**
 * @brief The Window API, an API that allows you to manage windows.
 */
class GEN_EXPORTED WindowAPI {
public:
	/**
	 * @brief After calling poolEvents, events are placed there
	 */
	std::vector<gen::Event> events;

	virtual ~WindowAPI() {}
	/**
	 * @brief Create a new window.
	 * 
	 * @param info Window creation data.
	 * @return Window* Pointer to the window.
	 */
	virtual Window* createWindow(const WindowCreateInfo& info) = 0;
	/**
	 * @brief Process all events and populate events vector
	 */
	virtual void poolEvents() = 0;
	/**
	 * @brief Wait for events then process them and populate events vector
	 */
	virtual void waitEvents() = 0;

	virtual uint64_t graphicsFunction(WindowGraphicsFunction func, uint64_t userData, uint64_t userData2, uint64_t userData3, uint64_t& out2) = 0;
};

}
