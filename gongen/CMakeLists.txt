# GongEn directories
set_property(GLOBAL PROPERTY GEN_SRC_DIR "${CMAKE_CURRENT_SOURCE_DIR}")
set_property(GLOBAL PROPERTY GEN_BIN_DIR "${CMAKE_CURRENT_SOURCE_DIR}")

# GongEn Implementation dependencies
include(../cmake/utils.cmake)

function(add_gongen LIB_SUFFIX)
	get_property(GEN_SRC_DIR GLOBAL PROPERTY GEN_SRC_DIR)
    get_property(GEN_BIN_DIR GLOBAL PROPERTY GEN_BIN_DIR)

	set(multi_value_args UNITS)
	cmake_parse_arguments(add_gongen "" "" "${multi_value_args}" "${ARGN}")

	# Target creation
	file(GLOB_RECURSE GEN_CORE_SOURCES "${GEN_SRC_DIR}/src/core/*.cpp" "${GEN_SRC_DIR}/src/core/*.hpp")
	add_library("GEN_GEN_CORE_${LIB_SUFFIX}" STATIC ${GEN_CORE_SOURCES})
	target_compile_definitions("GEN_GEN_CORE_${LIB_SUFFIX}" PUBLIC GEN_EXPORT GEN_BUILD_STATIC)
	target_include_directories("GEN_GEN_CORE_${LIB_SUFFIX}" PUBLIC SYSTEM "${GEN_SRC_DIR}/include")
	if(UNIX)
		target_link_libraries("GEN_GEN_CORE_${LIB_SUFFIX}" dl pthread)
	endif()
	if(WIN32)
		target_link_libraries("GEN_GEN_CORE_${LIB_SUFFIX}" Shlwapi)
	endif()

	# Register built-in units
	macro(gen_define_unit_internal_macro type item_id name src_folder)
		file(GLOB_RECURSE "SOURCES" "${GEN_SRC_DIR}/src/${src_folder}/*.cpp" "${GEN_SRC_DIR}/src/${src_folder}/*.hpp")
		string(TOLOWER ${item_id} item_id_lower)
		gen_define_unit(
			${LIB_SUFFIX}
			TYPE ${type}
			ID "gen::${item_id_lower}"
			NAME ${name}
			SOURCES ${SOURCES}
			${ARGN}
		)
	endmacro()
	if("gen::gcf" IN_LIST add_gongen_UNITS)
		gen_add_dependency(tinyxml2)
		gen_add_dependency(zstd)
		gen_define_unit_internal_macro(MODULE "GCF" "GongEn Container Format" "gcf" NO_INSTANCES)
		target_link_libraries("GEN_GEN_GCF_${LIB_SUFFIX}" tinyxml2 libzstd_static)
		target_include_directories("GEN_GEN_GCF_${LIB_SUFFIX}" PUBLIC SYSTEM ${TINYXML2_INCLUDE_DIR} "${zstd_SOURCE_DIR}/lib")
	endif()
	if("gen::window_api" IN_LIST add_gongen_UNITS)
		gen_define_unit_internal_macro(API "WINDOW_API" "GongEn Window API" "window_api" NO_INSTANCES)
	endif()
	if("gen::window_glfw_impl" IN_LIST add_gongen_UNITS)
		gen_add_dependency(glfw)
		gen_add_dependency(vulkan_headers)
		gen_add_dependency(volk)
		gen_define_unit_internal_macro(IMPL "WINDOW_GLFW_IMPL" "GongEn GLFW Implementation for GongEn Window API" "window_glfw_impl")
		target_link_libraries("GEN_GEN_WINDOW_GLFW_IMPL_${LIB_SUFFIX}" glfw)
		target_include_directories("GEN_GEN_WINDOW_GLFW_IMPL_${LIB_SUFFIX}" PUBLIC SYSTEM ${volk_SOURCE_DIR} "${vulkan_headers_SOURCE_DIR}/include" ${GLFW_INCLUDE_DIR})
	endif()
	if("gen::vfs" IN_LIST add_gongen_UNITS)
		gen_define_unit_internal_macro(MODULE "VFS" "GongEn's virtual file system" "vfs" NO_INSTANCES)
		target_link_libraries("GEN_GEN_VFS_${LIB_SUFFIX}" tinyxml2)
	endif()
	if("gen::graphics_api" IN_LIST add_gongen_UNITS)
		gen_define_unit_internal_macro(API "GRAPHICS_API" "GongEn Graphics API" "graphics_api" NO_INSTANCES)
	endif()
	if("gen::graphics_common" IN_LIST add_gongen_UNITS)
		gen_define_unit_internal_macro(MODULE "GRAPHICS_COMMON" "GongEn Graphics Impelemntation Utility Functions" "graphics_common" NO_INSTANCES)
	endif()
	if("gen::graphics_vulkan_impl" IN_LIST add_gongen_UNITS)
		gen_add_dependency(vulkan_headers)
		gen_add_dependency(volk)
		gen_add_dependency(vkmemalloc)
		gen_define_unit_internal_macro(IMPL "GRAPHICS_VULKAN_IMPL" "GongEn Vulkan Implementation for GongEn Graphics API" "graphics_vulkan_impl")
		target_include_directories("GEN_GEN_GRAPHICS_VULKAN_IMPL_${LIB_SUFFIX}" PUBLIC SYSTEM ${volk_SOURCE_DIR} "${vulkan_headers_SOURCE_DIR}/include" "${vkmemalloc_SOURCE_DIR}/include")
	endif()
	if("gen::imgui" IN_LIST add_gongen_UNITS)
		gen_add_dependency(imgui)
		gen_define_unit_internal_macro(MODULE "IMGUI" "GongEn Dear ImGui Integration" "imgui" NO_INSTANCES)
		target_include_directories("GEN_GEN_IMGUI_${LIB_SUFFIX}" PUBLIC SYSTEM "${imgui_SOURCE_DIR}" "${imgui_SOURCE_DIR}/misc/cpp" NO_INSTANCES)
	endif()

	gen_generate_unit_loader(${LIB_SUFFIX} UNITS ${add_gongen_UNITS})
	target_link_libraries("GEN_GEN_CORE_${LIB_SUFFIX}" "GongEnUnitLoader_${LIB_SUFFIX}")
endfunction()

function(target_add_gongen TARGET LIB_SUFFIX)
	set(multi_value_args UNITS)
	cmake_parse_arguments(target_add_gongen "" "" "${multi_value_args}" "${ARGN}")

	add_gongen(${LIB_SUFFIX} UNITS ${target_add_gongen_UNITS})
	list(APPEND libraries "GEN_GEN_CORE_${LIB_SUFFIX}")
	foreach(unit IN LISTS target_add_gongen_UNITS)
		gen_split_identifier(${unit} namespace_id item_id)
		string(TOUPPER ${namespace_id} namespace_id_upper)
		string(TOUPPER ${item_id} item_id_upper)
		list(APPEND libraries "GEN_${namespace_id_upper}_${item_id_upper}_${LIB_SUFFIX}")
	endforeach()
	target_link_libraries(${TARGET} ${libraries})
endfunction()
