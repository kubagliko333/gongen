#include <gongen/imgui/imgui.hpp>

struct ImGui_ImplGongEn_Data {
	gen::Window* window;
	gen::GraphicsDevice* graphicsDevice;

	gen::TexturePtr fontTexture;
	gen::PipelineDesc pipeline;
	gen::SamplerDesc sampler;

	gen::BufferDesc vertexBufferDesc;
	gen::BufferDesc indexBufferDesc;

	std::string clipboard;

	std::vector<gen::TexturePtr> textures;

	bool fontTextureUpdated = false;
};

static ImGui_ImplGongEn_Data* ImGui_ImplGongEn_GetBackendData()
{
	return ImGui::GetCurrentContext() ? (ImGui_ImplGongEn_Data*)ImGui::GetIO().BackendPlatformUserData : NULL;
}

static const char* ImGui_ImplGongEn_GetClipboardText(void*)
{
	ImGui_ImplGongEn_Data* bd = ImGui_ImplGongEn_GetBackendData();
	bd->clipboard = bd->window->getClipboard();
	return bd->clipboard.c_str();
}

static void ImGui_ImplGongEn_SetClipboardText(void*, const char* text)
{
	ImGui_ImplGongEn_Data* bd = ImGui_ImplGongEn_GetBackendData();
	bd->window->setClipboard(text);
}

void ImGui_InitKeyConversionTables();

void gen::ImGui_Init(gen::Window *window, gen::GraphicsDevice *graphicsDevice, gen::ShaderPtr shader) {
	ImGui_InitKeyConversionTables();
	ImGuiContext *ctx = ImGui::CreateContext();
	ImGui::SetCurrentContext(ctx);

	ImGuiIO& io = ImGui::GetIO();
	io.BackendPlatformUserData = new ImGui_ImplGongEn_Data();
	io.BackendPlatformName = "GongEn";
	io.BackendRendererUserData = nullptr;
	io.BackendRendererName = "GongEn";

	io.BackendFlags = ImGuiBackendFlags_HasMouseCursors | ImGuiBackendFlags_HasSetMousePos | ImGuiBackendFlags_RendererHasVtxOffset;
	io.ConfigFlags = ImGuiConfigFlags_DockingEnable | ImGuiConfigFlags_NavEnableKeyboard | ImGuiConfigFlags_NavEnableSetMousePos;
	io.ConfigDockingWithShift = true;
	
	io.GetClipboardTextFn = ImGui_ImplGongEn_GetClipboardText;
	io.SetClipboardTextFn = ImGui_ImplGongEn_SetClipboardText;
	io.ClipboardUserData = nullptr;

	ImGui_ImplGongEn_Data* bd = ImGui_ImplGongEn_GetBackendData();
	bd->window = window;
	bd->graphicsDevice = graphicsDevice;

	uint32_t windowW = window->getWidth();
	uint32_t windowH = window->getHeight();

	io.DisplaySize.x = windowW;
	io.DisplaySize.y = windowH;

	uint8_t* pixelData;
	int w, h;
	io.Fonts->GetTexDataAsRGBA32(&pixelData, &w, &h);
	gen::TextureDesc fontTextureDesc;
	fontTextureDesc.bindFlags = gen::TextureBindFlag::TEXTURE_BIND_SAMPLED | gen::TextureBindFlag::TEXTURE_BIND_TRANSFER_DST;
	fontTextureDesc.width = w;
	fontTextureDesc.height = h;

	bd->fontTexture = graphicsDevice->createTexture(fontTextureDesc);

	bd->vertexBufferDesc.bindFlags = gen::BufferBindFlag::BUFFER_BIND_VERTEX_BUFFER;
	bd->indexBufferDesc.bindFlags = gen::BufferBindFlag::BUFFER_BIND_INDEX_BUFFER;

	gen::PipelineDesc pipeline_desc;
	pipeline_desc.vertexInput.bindings[0].stride = sizeof(ImDrawVert);
	pipeline_desc.vertexInput.bindings[0].perInstance = false;
	pipeline_desc.vertexInput.attributesAmount = 3;

	pipeline_desc.vertexInput.attributes[0].binding = 0;
	pipeline_desc.vertexInput.attributes[0].format = gen::GPUFormat::RG32_SFLOAT;
	pipeline_desc.vertexInput.attributes[0].offset = offsetof(ImDrawVert, pos);

	pipeline_desc.vertexInput.attributes[1].binding = 0;
	pipeline_desc.vertexInput.attributes[1].format = gen::GPUFormat::RG32_SFLOAT;
	pipeline_desc.vertexInput.attributes[1].offset = offsetof(ImDrawVert, uv);

	pipeline_desc.vertexInput.attributes[2].binding = 0;
	pipeline_desc.vertexInput.attributes[2].format = gen::GPUFormat::RGBA8_UNORM;
	pipeline_desc.vertexInput.attributes[2].offset = offsetof(ImDrawVert, col);

	pipeline_desc.blend.attachments[0].enabled = true;
	pipeline_desc.shader = shader;
	pipeline_desc.dynamicScissor = true;
	pipeline_desc.dynamicViewport = true;

	bd->pipeline = pipeline_desc;

	gen::BufferDesc uniformBufferDesc;
	uniformBufferDesc.bindFlags = gen::BufferBindFlag::BUFFER_BIND_UNIFORM_BUFFER;
	uniformBufferDesc.size = sizeof(float) * 16;
}

void gen::ImGui_NewFrame(const gen::CommandBufferPtr& commandBuffer) {
	ImGuiIO& io = ImGui::GetIO();

	ImGui_ImplGongEn_Data* bd = ImGui_ImplGongEn_GetBackendData();
	if (!bd->fontTextureUpdated) {
		ImGuiIO& io = ImGui::GetIO();
		uint8_t* pixelData;
		int w, h;
		io.Fonts->GetTexDataAsRGBA32(&pixelData, &w, &h);
		bd->fontTextureUpdated = true;
		commandBuffer->updateTexture(bd->fontTexture, {}, w, h, 1, pixelData);
	}

	ImGuiMouseCursor imgui_cursor = ImGui::GetMouseCursor();
	if (!(imgui_cursor == ImGuiMouseCursor_None)) {
		if (imgui_cursor == ImGuiMouseCursor_Arrow) bd->window->setCursor(gen::Cursor::ARROW);
		else if (imgui_cursor == ImGuiMouseCursor_TextInput) bd->window->setCursor(gen::Cursor::IBEAM);
		else if (imgui_cursor == ImGuiMouseCursor_ResizeNS) bd->window->setCursor(gen::Cursor::RESIZE_NS);
		else if (imgui_cursor == ImGuiMouseCursor_ResizeEW) bd->window->setCursor(gen::Cursor::RESIZE_EW);
		else if (imgui_cursor == ImGuiMouseCursor_Hand) bd->window->setCursor(gen::Cursor::HAND);
		else if (imgui_cursor == ImGuiMouseCursor_ResizeAll) bd->window->setCursor(gen::Cursor::RESIZE_ALL);
		else if (imgui_cursor == ImGuiMouseCursor_ResizeNESW) bd->window->setCursor(gen::Cursor::RESIZE_NESW);
		else if (imgui_cursor == ImGuiMouseCursor_ResizeNWSE) bd->window->setCursor(gen::Cursor::RESIZE_NWSE);
		else if (imgui_cursor == ImGuiMouseCursor_NotAllowed) bd->window->setCursor(gen::Cursor::NOT_ALLOWED);
	}

	if (io.WantSetMousePos) {
		bd->window->setMousePosition((uint32_t)io.MousePos.x, (uint32_t)io.MousePos.y);
	}

	ImGui::NewFrame();
}

ImGuiKey keyConversionTable[(size_t)gen::KeyButton::TOTAL_KEYS];
ImGuiMouseButton buttonConversionTable[(size_t)gen::MouseButton::TOTAL_BUTTONS];

void gen::ImGui_ProcessEvent(gen::Event& event) {
	ImGuiIO& io = ImGui::GetIO();
	ImGui_ImplGongEn_Data* bd = ImGui_ImplGongEn_GetBackendData();

	if (event.type == gen::EventType::MOUSE_POSITION) {
		io.AddMousePosEvent(event.mousePosition.x, event.mousePosition.y);
	}
	else if (event.type == gen::EventType::MOUSE_WHEEL) {
		io.AddMouseWheelEvent(event.mouseWheel.x, event.mouseWheel.y);
	}
	else if (event.type == gen::EventType::MOUSE_BUTTON) {
		int button = buttonConversionTable[(size_t)event.mouseButton.button];
		if (button != UINT32_MAX) io.AddMouseButtonEvent(buttonConversionTable[(size_t)event.mouseButton.button], (event.mouseButton.action == gen::KeyAction::PRESS));
	}
	else if (event.type == gen::EventType::KEY) {
		int key = keyConversionTable[(size_t)event.key.key];
		io.AddKeyEvent(ImGuiKey_ModAlt, event.key.mods & gen::KeyMods::KEY_MOD_ALT);
		io.AddKeyEvent(ImGuiKey_ModCtrl, event.key.mods & gen::KeyMods::KEY_MOD_CONTROL);
		io.AddKeyEvent(ImGuiKey_ModShift, event.key.mods & gen::KeyMods::KEY_MOD_SHIFT);
		if(key != UINT32_MAX) io.AddKeyEvent(keyConversionTable[(size_t)event.key.key], (event.key.action == gen::KeyAction::PRESS));
	}
	else if (event.type == gen::EventType::WINDOW_FOCUS) {
		io.AddFocusEvent(event.window.focus.focus);
	}
	else if (event.type == gen::EventType::CHARACTER) {
		io.AddInputCharacter(event.character.codepoint);
	}
}

gen::TexturePtr imguiGetTexture(ImTextureID texID) {
	ImGui_ImplGongEn_Data* bd = ImGui_ImplGongEn_GetBackendData();
	if (!texID) return bd->fontTexture;
	for (auto& t : bd->textures) {
		if (t.get() == texID) return t;
	}
	return bd->fontTexture;
}

ImTextureID gen::imguiTexture(gen::TexturePtr texture) {
	ImGui_ImplGongEn_Data* bd = ImGui_ImplGongEn_GetBackendData();
	bd->textures.push_back(texture);
	return texture.get();
}

void gen::ImGui_Render(const gen::CommandBufferPtr& commandBuffer) {
	ImGui::Render();

	ImGui_ImplGongEn_Data* bd = ImGui_ImplGongEn_GetBackendData();
	ImDrawData* draw_data = ImGui::GetDrawData();

	int fb_width = (int)(draw_data->DisplaySize.x);
	int fb_height = (int)(draw_data->DisplaySize.y);
	
	gen::ViewportDesc viewport;
	viewport.width = fb_width;
	viewport.height = fb_height;

	ImVec2 clip_off = draw_data->DisplayPos;

	float L = draw_data->DisplayPos.x;
	float R = draw_data->DisplayPos.x + draw_data->DisplaySize.x;
	float T = draw_data->DisplayPos.y;
	float B = draw_data->DisplayPos.y + draw_data->DisplaySize.y;

	float mvp[4][4] =
	{
		2.0f / (R - L),
	  0.0f,
	  0.0f,
	  0.0f,

	  0.0f,
	  2.0f / (B - T),
	  0.0f,
	  0.0f,

	  0.0f,
	  0.0f,
	  1.0f / (0.0f - 1.0f),
	  0.0f,

	  -(R + L) / (R - L),
	  -(B + T) / (B - T),
	  0.0f / (0.0f - 1.0f),
	  1.0f
	};

	gen::BufferPtr uniformBuffer = commandBuffer->createDynamicBuffer(gen::BufferBindFlag::BUFFER_BIND_UNIFORM_BUFFER, sizeof(float) * 16);
	commandBuffer->updateBuffer(uniformBuffer, 0, sizeof(float) * 16, mvp);

	commandBuffer->setUniformBuffer(0, 0, uniformBuffer, 0, 0);
	commandBuffer->setSampler(0, 2, bd->sampler);

	commandBuffer->bindPipeline(bd->pipeline);
	commandBuffer->setViewport(viewport);
	for (int n = 0; n < draw_data->CmdListsCount; n++) {
		const ImDrawList* cmd_list = draw_data->CmdLists[n];
		const ImDrawVert* vtx_buffer = cmd_list->VtxBuffer.Data;
		const ImDrawIdx* idx_buffer = cmd_list->IdxBuffer.Data;

		uint32_t vtx_size = cmd_list->VtxBuffer.Size * sizeof(ImDrawVert);
		uint32_t idx_size = cmd_list->IdxBuffer.Size * sizeof(ImDrawIdx);

		gen::BufferPtr vertexBuffer = commandBuffer->createDynamicBuffer(gen::BufferBindFlag::BUFFER_BIND_VERTEX_BUFFER, vtx_size);
		gen::BufferPtr indexBuffer = commandBuffer->createDynamicBuffer(gen::BufferBindFlag::BUFFER_BIND_INDEX_BUFFER, idx_size);
		commandBuffer->updateBuffer(vertexBuffer, 0, vtx_size, (void*)vtx_buffer);
		commandBuffer->updateBuffer(indexBuffer, 0, idx_size, (void*)idx_buffer);

		commandBuffer->bindVertexBuffers(vertexBuffer, 0, 0);
		commandBuffer->bindIndexBuffer(indexBuffer, 0, gen::IndexType::UINT32);

		for (int cmd_i = 0; cmd_i < cmd_list->CmdBuffer.Size; cmd_i++) {
			const ImDrawCmd* pcmd = &cmd_list->CmdBuffer[cmd_i];

			ImVec2 clip_min((pcmd->ClipRect.x - clip_off.x), (pcmd->ClipRect.y - clip_off.y));
			ImVec2 clip_max((pcmd->ClipRect.z - clip_off.x), (pcmd->ClipRect.w - clip_off.y));
			if (clip_min.x < 0.0f) { clip_min.x = 0.0f; }
			if (clip_min.y < 0.0f) { clip_min.y = 0.0f; }
			if (clip_max.x > fb_width) { clip_max.x = (float)fb_width; }
			if (clip_max.y > fb_height) { clip_max.y = (float)fb_height; }

			commandBuffer->setTexture(0, 1, imguiGetTexture(pcmd->GetTexID()));
			commandBuffer->setScissor({ (uint32_t)clip_min.x, (uint32_t)clip_min.y, (uint32_t)(clip_max.x - clip_min.x), (uint32_t)(clip_max.y - clip_min.y) });
			commandBuffer->drawIndexed(pcmd->ElemCount, pcmd->IdxOffset);
		}
	}
}


void ImGui_InitKeyConversionTables() {
	memset(keyConversionTable, UINT32_MAX, sizeof(ImGuiKey) * (size_t)gen::KeyButton::TOTAL_KEYS);
	memset(buttonConversionTable, UINT32_MAX, sizeof(ImGuiMouseButton) * (size_t)gen::MouseButton::TOTAL_BUTTONS);

	buttonConversionTable[(size_t)gen::MouseButton::LEFT] = ImGuiMouseButton_Left;
	buttonConversionTable[(size_t)gen::MouseButton::RIGHT] = ImGuiMouseButton_Right;
	buttonConversionTable[(size_t)gen::MouseButton::MIDDLE] = ImGuiMouseButton_Middle;

	keyConversionTable[(size_t)gen::KeyButton::LEFT] = ImGuiKey_LeftArrow;
	keyConversionTable[(size_t)gen::KeyButton::RIGHT] = ImGuiKey_RightArrow;
	keyConversionTable[(size_t)gen::KeyButton::UP] = ImGuiKey_UpArrow;
	keyConversionTable[(size_t)gen::KeyButton::DOWN] = ImGuiKey_DownArrow;
	keyConversionTable[(size_t)gen::KeyButton::PAGE_UP] = ImGuiKey_PageUp;
	keyConversionTable[(size_t)gen::KeyButton::PAGE_DOWN] = ImGuiKey_PageDown;
	keyConversionTable[(size_t)gen::KeyButton::HOME] = ImGuiKey_Home;
	keyConversionTable[(size_t)gen::KeyButton::END] = ImGuiKey_End;
	keyConversionTable[(size_t)gen::KeyButton::INSERT] = ImGuiKey_Insert;
	keyConversionTable[(size_t)gen::KeyButton::DELETE] = ImGuiKey_Delete;
	keyConversionTable[(size_t)gen::KeyButton::BACKSPACE] = ImGuiKey_Backspace;
	keyConversionTable[(size_t)gen::KeyButton::SPACE] = ImGuiKey_Space;
	keyConversionTable[(size_t)gen::KeyButton::ENTER] = ImGuiKey_Enter;
	keyConversionTable[(size_t)gen::KeyButton::ESCAPE] = ImGuiKey_Escape;

	keyConversionTable[(size_t)gen::KeyButton::KEY_0] = ImGuiKey_0;
	keyConversionTable[(size_t)gen::KeyButton::KEY_1] = ImGuiKey_1;
	keyConversionTable[(size_t)gen::KeyButton::KEY_2] = ImGuiKey_2;
	keyConversionTable[(size_t)gen::KeyButton::KEY_3] = ImGuiKey_3;
	keyConversionTable[(size_t)gen::KeyButton::KEY_4] = ImGuiKey_4;
	keyConversionTable[(size_t)gen::KeyButton::KEY_5] = ImGuiKey_5;
	keyConversionTable[(size_t)gen::KeyButton::KEY_6] = ImGuiKey_6;
	keyConversionTable[(size_t)gen::KeyButton::KEY_7] = ImGuiKey_7;
	keyConversionTable[(size_t)gen::KeyButton::KEY_8] = ImGuiKey_8;
	keyConversionTable[(size_t)gen::KeyButton::KEY_9] = ImGuiKey_9;
	keyConversionTable[(size_t)gen::KeyButton::A] = ImGuiKey_A;
	keyConversionTable[(size_t)gen::KeyButton::B] = ImGuiKey_B;
	keyConversionTable[(size_t)gen::KeyButton::C] = ImGuiKey_C;
	keyConversionTable[(size_t)gen::KeyButton::D] = ImGuiKey_D;
	keyConversionTable[(size_t)gen::KeyButton::E] = ImGuiKey_E;
	keyConversionTable[(size_t)gen::KeyButton::F] = ImGuiKey_F;
	keyConversionTable[(size_t)gen::KeyButton::G] = ImGuiKey_G;
	keyConversionTable[(size_t)gen::KeyButton::H] = ImGuiKey_H;
	keyConversionTable[(size_t)gen::KeyButton::I] = ImGuiKey_I;
	keyConversionTable[(size_t)gen::KeyButton::J] = ImGuiKey_J;
	keyConversionTable[(size_t)gen::KeyButton::K] = ImGuiKey_K;
	keyConversionTable[(size_t)gen::KeyButton::L] = ImGuiKey_L;
	keyConversionTable[(size_t)gen::KeyButton::M] = ImGuiKey_M;
	keyConversionTable[(size_t)gen::KeyButton::N] = ImGuiKey_N;
	keyConversionTable[(size_t)gen::KeyButton::O] = ImGuiKey_O;
	keyConversionTable[(size_t)gen::KeyButton::P] = ImGuiKey_P;
	keyConversionTable[(size_t)gen::KeyButton::Q] = ImGuiKey_Q;
	keyConversionTable[(size_t)gen::KeyButton::R] = ImGuiKey_R;
	keyConversionTable[(size_t)gen::KeyButton::S] = ImGuiKey_S;
	keyConversionTable[(size_t)gen::KeyButton::T] = ImGuiKey_T;
	keyConversionTable[(size_t)gen::KeyButton::U] = ImGuiKey_U;
	keyConversionTable[(size_t)gen::KeyButton::V] = ImGuiKey_V;
	keyConversionTable[(size_t)gen::KeyButton::W] = ImGuiKey_W;
	keyConversionTable[(size_t)gen::KeyButton::X] = ImGuiKey_X;
	keyConversionTable[(size_t)gen::KeyButton::Y] = ImGuiKey_Y;
	keyConversionTable[(size_t)gen::KeyButton::Z] = ImGuiKey_Z;

	keyConversionTable[(size_t)gen::KeyButton::F1] = ImGuiKey_F1;
	keyConversionTable[(size_t)gen::KeyButton::F2] = ImGuiKey_F2;
	keyConversionTable[(size_t)gen::KeyButton::F3] = ImGuiKey_F3;
	keyConversionTable[(size_t)gen::KeyButton::F4] = ImGuiKey_F4;
	keyConversionTable[(size_t)gen::KeyButton::F5] = ImGuiKey_F5;
	keyConversionTable[(size_t)gen::KeyButton::F6] = ImGuiKey_F6;
	keyConversionTable[(size_t)gen::KeyButton::F7] = ImGuiKey_F7;
	keyConversionTable[(size_t)gen::KeyButton::F8] = ImGuiKey_F8;
	keyConversionTable[(size_t)gen::KeyButton::F9] = ImGuiKey_F9;
	keyConversionTable[(size_t)gen::KeyButton::F10] = ImGuiKey_F10;
	keyConversionTable[(size_t)gen::KeyButton::F11] = ImGuiKey_F11;
	keyConversionTable[(size_t)gen::KeyButton::F12] = ImGuiKey_F12;

	keyConversionTable[(size_t)gen::KeyButton::LEFT_CONTROL] = ImGuiKey_LeftCtrl;
	keyConversionTable[(size_t)gen::KeyButton::LEFT_SHIFT] = ImGuiKey_LeftShift;
	keyConversionTable[(size_t)gen::KeyButton::LEFT_ALT] = ImGuiKey_LeftAlt;
	keyConversionTable[(size_t)gen::KeyButton::RIGHT_CONTROL] = ImGuiKey_RightCtrl;
	keyConversionTable[(size_t)gen::KeyButton::RIGHT_SHIFT] = ImGuiKey_RightShift;
	keyConversionTable[(size_t)gen::KeyButton::RIGHT_ALT] = ImGuiKey_RightAlt;

	keyConversionTable[(size_t)gen::KeyButton::APOSTROPHE] = ImGuiKey_Apostrophe;
	keyConversionTable[(size_t)gen::KeyButton::COMMA] = ImGuiKey_Comma;
	keyConversionTable[(size_t)gen::KeyButton::MINUS] = ImGuiKey_Minus;
	keyConversionTable[(size_t)gen::KeyButton::PERIOD] = ImGuiKey_Period;
	keyConversionTable[(size_t)gen::KeyButton::SLASH] = ImGuiKey_Slash;
	keyConversionTable[(size_t)gen::KeyButton::SEMICOLON] = ImGuiKey_Semicolon;
	keyConversionTable[(size_t)gen::KeyButton::EQUAL] = ImGuiKey_Equal;
	keyConversionTable[(size_t)gen::KeyButton::LEFT_BRACKET] = ImGuiKey_LeftBracket;
	keyConversionTable[(size_t)gen::KeyButton::BACKSLASH] = ImGuiKey_Backslash;
	keyConversionTable[(size_t)gen::KeyButton::RIGHT_BRACKET] = ImGuiKey_RightBracket;
	keyConversionTable[(size_t)gen::KeyButton::GRAVE_ACCENT] = ImGuiKey_GraveAccent;
	keyConversionTable[(size_t)gen::KeyButton::CAPS_LOCK] = ImGuiKey_CapsLock;
	keyConversionTable[(size_t)gen::KeyButton::NUM_LOCK] = ImGuiKey_NumLock;
	keyConversionTable[(size_t)gen::KeyButton::PRINT_SCREEN] = ImGuiKey_PrintScreen;
	keyConversionTable[(size_t)gen::KeyButton::PAUSE] = ImGuiKey_Pause;

	keyConversionTable[(size_t)gen::KeyButton::KP_0] = ImGuiKey_Keypad0;
	keyConversionTable[(size_t)gen::KeyButton::KP_1] = ImGuiKey_Keypad1;
	keyConversionTable[(size_t)gen::KeyButton::KP_2] = ImGuiKey_Keypad2;
	keyConversionTable[(size_t)gen::KeyButton::KP_3] = ImGuiKey_Keypad3;
	keyConversionTable[(size_t)gen::KeyButton::KP_4] = ImGuiKey_Keypad4;
	keyConversionTable[(size_t)gen::KeyButton::KP_5] = ImGuiKey_Keypad5;
	keyConversionTable[(size_t)gen::KeyButton::KP_6] = ImGuiKey_Keypad6;
	keyConversionTable[(size_t)gen::KeyButton::KP_7] = ImGuiKey_Keypad7;
	keyConversionTable[(size_t)gen::KeyButton::KP_8] = ImGuiKey_Keypad8;
	keyConversionTable[(size_t)gen::KeyButton::KP_9] = ImGuiKey_Keypad9;

	keyConversionTable[(size_t)gen::KeyButton::KP_DECIMAL] = ImGuiKey_KeypadDecimal;
	keyConversionTable[(size_t)gen::KeyButton::KP_DIVIDE] = ImGuiKey_KeypadDivide;
	keyConversionTable[(size_t)gen::KeyButton::KP_MULTIPLY] = ImGuiKey_KeypadMultiply;
	keyConversionTable[(size_t)gen::KeyButton::KP_SUBTRACT] = ImGuiKey_KeypadSubtract;
	keyConversionTable[(size_t)gen::KeyButton::KP_ADD] = ImGuiKey_KeypadAdd;
	keyConversionTable[(size_t)gen::KeyButton::KP_ENTER] = ImGuiKey_KeypadEnter;
	keyConversionTable[(size_t)gen::KeyButton::KP_EQUAL] = ImGuiKey_KeypadEqual;
}
