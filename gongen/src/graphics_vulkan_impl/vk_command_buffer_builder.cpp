#include "vk_graphics.hpp"

VkImageSubresourceLayers vkTextureOffset(gen::GPUFormat format, gen::TextureOffset offset) {
	VkImageSubresourceLayers out;
	if ((format == gen::GPUFormat::D16_UNORM) || (format == gen::GPUFormat::D32_SFLOAT)) {
		out.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
	}
	else {
		out.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	}
	out.mipLevel = offset.level;
	out.layerCount = offset.layers;
	out.baseArrayLayer = offset.layerOffset;
	return out;
}

void gen::vkCommandBuffer::setSync(gen::vkSynchronizedResource* resource, VkPipelineStageFlags srcStage, VkAccessFlags srcAccess) {
	resource->syncSet = true;
	resource->srcAccessMask = srcAccess;
	resource->srcStageMask = srcStage;
}

VkImageMemoryBarrier getImageBarrier(gen::vkTexture* texture, VkImageLayout imageLayout, VkAccessFlags dstAccess) {
	VkImageMemoryBarrier barrier = { VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER, NULL };
	barrier.srcAccessMask = texture->srcAccessMask;
	barrier.dstAccessMask = dstAccess;
	barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.image = texture->image;
	barrier.oldLayout = texture->currentLayout;
	barrier.newLayout = imageLayout;
	if (texture->textureDesc.bindFlags & gen::TextureBindFlag::TEXTURE_BIND_DEPTH_STENCIL_ATTACHMENT) barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
	else barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	if (texture->textureDesc.format == gen::GPUFormat::D24_UNORM_S8_UINT) {
		barrier.subresourceRange.aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
	}
	barrier.subresourceRange.baseArrayLayer = 0;
	barrier.subresourceRange.baseMipLevel = 0;
	barrier.subresourceRange.layerCount = texture->textureDesc.layers;
	barrier.subresourceRange.levelCount = texture->textureDesc.mipLevels;
	return barrier;
}

void gen::vkCommandBuffer::waitSyncBuffer(gen::vkBuffer* buffer, VkPipelineStageFlags dstStage, VkAccessFlags dstAccess) {
	VkBufferMemoryBarrier barrier = { VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER, NULL };
	barrier.srcAccessMask = buffer->srcAccessMask;
	barrier.dstAccessMask = dstAccess;
	barrier.srcQueueFamilyIndex = device->queueFamily;
	barrier.dstQueueFamilyIndex = device->queueFamily;
	barrier.buffer = buffer->buffer;
	barrier.offset = 0;
	barrier.size = VK_WHOLE_SIZE;
	vkCmdPipelineBarrier(data->commandBuffer, buffer->srcStageMask, dstStage, 0, 0, nullptr, 1, &barrier, 0, nullptr);
	buffer->syncSet = false;
}

void gen::vkCommandBuffer::waitSyncImage(gen::vkTexture* texture, VkPipelineStageFlags dstStage, VkAccessFlags dstAccess, VkImageLayout layout) {
	VkImageMemoryBarrier barrier = getImageBarrier(texture, layout, dstAccess);
	if ((dstStage == VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT) || (dstStage == VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT)) {
		barrier.dstAccessMask = 0;
	}
	vkCmdPipelineBarrier(data->commandBuffer, texture->srcStageMask, dstStage, 0, 0, nullptr, 0, nullptr, 1, &barrier);
	texture->syncSet = false;
	texture->currentLayout = layout;
}

void gen::vkCommandBuffer::processResourceAccess(gen::vkResourceAccess* access) {
	bool accessImage = access->image;
	bool accessWrite = access->write;

	gen::vkSynchronizedResource* resource;

	if (accessImage) resource = access->texture;
	else resource = access->buffer;

	bool syncSet = resource->syncSet;
	bool imageLayoutChange = false;
	if (accessImage) {
		if (access->imageLayout != access->texture->currentLayout) imageLayoutChange = true;
	}

	if (accessWrite) {
		if (syncSet) {
			if (accessImage) waitSyncImage(access->texture, access->srcStage, access->srcAccess, access->imageLayout);
			else waitSyncBuffer(access->buffer, access->srcStage, access->srcAccess);
		}
		else if (imageLayoutChange) {
			setSync(access->texture, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, 0);
			waitSyncImage(access->texture, access->srcStage, access->srcAccess, access->imageLayout);
		}
		setSync(resource, access->srcStage, access->srcAccess);
	}
	else {
		if (syncSet) {
			if (accessImage) waitSyncImage(access->texture, access->dstStage, access->dstAccess, access->imageLayout);
			else waitSyncBuffer(access->buffer, access->dstStage, access->dstAccess);
		}
		else if (imageLayoutChange) {
			setSync(access->texture, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, 0);
			waitSyncImage(access->texture, access->dstStage, access->dstAccess, access->imageLayout);
		}
	}
}

void gen::vkCommandBuffer::buildCommandBuffer() {
	VkCommandBufferBeginInfo beginInfo = { VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO, NULL };
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
	beginInfo.pInheritanceInfo = nullptr;

	VK_ERROR_TEST(vkResetCommandBuffer(data->commandBuffer, 0), gen::GraphicsCommandExecutionException, "Can't Reset Command Buffer")
	
	VK_ERROR_TEST(vkBeginCommandBuffer(data->commandBuffer, &beginInfo), gen::GraphicsCommandExecutionException, "Can't Begin Command Buffer")

	bool insideRenderPass = false;
	std::vector<gen::vkResourceAccess*> renderPassResourceAccess;
	size_t cmdSize = data->commands.size();

	for (uint32_t i = 0; i < cmdSize; i++) {
		gen::vkCommand* cmd = &data->commands[i];
		for (auto& r : cmd->resourceAccesses) {
			if (!insideRenderPass) {
				processResourceAccess(&r);
			}
		}
		if (cmd->type == gen::vkCommandType::BEGIN_RENDER_PASS) {
			renderPassResourceAccess.clear();
			for (uint32_t i2 = i; i2 < cmdSize; i2++) {
				vkCommand* cmd = &data->commands[i2];
				if (cmd->type != gen::vkCommandType::END_RENDER_PASS) {
					for (auto& r : cmd->resourceAccesses) {
						if (!r.write) processResourceAccess(&r);
					}
				}
				else {
					break;
				}
			}
			VkRenderPassBeginInfo beginInfo = { VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO, NULL };
			beginInfo.renderPass = cmd->renderPass.renderPass;
			beginInfo.framebuffer = cmd->renderPass.framebuffer;
			beginInfo.renderArea = cmd->renderPass.renderArea;
			beginInfo.clearValueCount = cmd->renderPass.colorAttachments + ((uint32_t)cmd->renderPass.depthStencilAttachment);
			beginInfo.pClearValues = cmd->renderPass.clearValue;
			vkCmdBeginRenderPass(data->commandBuffer, &beginInfo, VK_SUBPASS_CONTENTS_INLINE);
			data->activeRenderPass = cmd->renderPass;
			insideRenderPass = true;

			if (cmd->renderPass.swapchain) {
				bool exist = false;
				for (auto& s : data->swapchains) {
					if (s == cmd->renderPass.swapchain) exist = true;
				}
				if (!exist) {
					data->swapchains.push_back(cmd->renderPass.swapchain);
				}
			}
		}
		else if (cmd->type == gen::vkCommandType::END_RENDER_PASS) {
			vkCmdEndRenderPass(data->commandBuffer);
			for (auto& r : cmd->resourceAccesses) {
				processResourceAccess(&r);
			}
			/*for (uint32_t i = 0; i < data->activeRenderPass.colorAttachments; i++) {
				gen::vkTexture* vkTexture = data->activeRenderPass.textures[i];
				vkTexture->currentLayout = data->activeRenderPass->endLayout[i];
			}*/
			insideRenderPass = false;
		}
		else if (cmd->type == gen::vkCommandType::COPY_BUFFER) {
			gen::vkBuffer* vkBuffer = cmd->copyBuffer.srcBuffer;
			gen::vkBuffer* vkBuffer2 = cmd->copyBuffer.dstBuffer;

			VkBufferCopy copy;
			copy.srcOffset = cmd->copyBuffer.srcOffset;
			copy.dstOffset = cmd->copyBuffer.dstOffset;
			copy.size = cmd->copyBuffer.size;

			vkCmdCopyBuffer(data->commandBuffer, vkBuffer->buffer, vkBuffer2->buffer, 1, &copy);
		}
		else if (cmd->type == gen::vkCommandType::COPY_BUFFER_TO_TEXTURE) {
			gen::vkBuffer* vkBuffer = (gen::vkBuffer*)cmd->copyBufferToTexture.srcBuffer;
			gen::vkTexture* vkTexture = (gen::vkTexture*)cmd->copyBufferToTexture.dstTexture;

			VkBufferImageCopy copy;
			copy.bufferImageHeight = 0;
			copy.bufferRowLength = 0;
			copy.bufferOffset = cmd->copyBufferToTexture.srcOffset;
			copy.imageSubresource = vkTextureOffset(vkTexture->textureDesc.format, cmd->copyBufferToTexture.dstOffset);
			copy.imageOffset = { (int)cmd->copyBufferToTexture.dstOffset.x, (int)cmd->copyBufferToTexture.dstOffset.y, (int)cmd->copyBufferToTexture.dstOffset.z };
			copy.imageExtent = { (uint32_t)cmd->copyBufferToTexture.sizeX, (uint32_t)cmd->copyBufferToTexture.sizeY, (uint32_t)cmd->copyBufferToTexture.sizeZ };

			vkCmdCopyBufferToImage(data->commandBuffer, vkBuffer->buffer, vkTexture->image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &copy);
		}
		else if (cmd->type == gen::vkCommandType::SET_DESCRIPTOR_SETS) {
			vkCmdBindDescriptorSets(data->commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, data->activePipeline.layout, cmd->setDescriptorSets.firstSet, cmd->setDescriptorSets.setsAmount, cmd->setDescriptorSets.sets, 0, nullptr);
		}
		else if (cmd->type == gen::vkCommandType::SET_VERTEX_BUFFER) {
			gen::vkBuffer* vkBuffer = cmd->setVertexBuffer.buffer;
			VkDeviceSize offset = cmd->setVertexBuffer.offset;
			vkCmdBindVertexBuffers(data->commandBuffer, cmd->setVertexBuffer.slot, 1, &vkBuffer->buffer, &offset);
		}
		else if (cmd->type == gen::vkCommandType::SET_INDEX_BUFFER) {
			gen::vkBuffer* vkBuffer = cmd->setIndexBuffer.buffer;
			if (cmd->setIndexBuffer.indexType == gen::IndexType::UINT16) vkCmdBindIndexBuffer(data->commandBuffer, vkBuffer->buffer, 0, VK_INDEX_TYPE_UINT16);
			else if (cmd->setIndexBuffer.indexType == gen::IndexType::UINT32) vkCmdBindIndexBuffer(data->commandBuffer, vkBuffer->buffer, 0, VK_INDEX_TYPE_UINT32);
		}
		else if (cmd->type == gen::vkCommandType::SET_PIPELINE) {
			vkCmdBindPipeline(data->commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, cmd->pipeline.pipeline);
			data->activePipeline = cmd->pipeline;
		}
		else if (cmd->type == gen::vkCommandType::SET_SCISSOR) {
			vkCmdSetScissor(data->commandBuffer, 0, 1, &cmd->scissor);
		}
		else if (cmd->type == gen::vkCommandType::SET_VIEWPORT) {
			vkCmdSetViewport(data->commandBuffer, 0, 1, &cmd->viewport);
		}
		else if (cmd->type == gen::vkCommandType::DRAW) {
			vkCmdDraw(data->commandBuffer, cmd->draw.vertexCount, cmd->draw.instanceCount, cmd->draw.firstVertex, cmd->draw.firstInstance);
		}
		else if (cmd->type == gen::vkCommandType::DRAW_INDEXED) {
			vkCmdDrawIndexed(data->commandBuffer, cmd->draw.indexCount, cmd->draw.instanceCount, cmd->draw.firstIndex, cmd->draw.vertexOffset, cmd->draw.firstInstance);
		}
		else if (cmd->type == gen::vkCommandType::DRAW_INDIRECT) {
			vkCmdDrawIndirect(data->commandBuffer, cmd->draw.indirectBuffer->buffer, cmd->draw.offset, cmd->draw.drawAmount, sizeof(gen::drawIndirectCommand));
		}
		else if (cmd->type == gen::vkCommandType::DRAW_INDEXED_INDIRECT) {
			vkCmdDrawIndexedIndirect(data->commandBuffer, cmd->draw.indirectBuffer->buffer, cmd->draw.offset, cmd->draw.drawAmount, sizeof(gen::drawIndexedIndirectCommand));
		}
	}

	VK_ERROR_TEST(vkEndCommandBuffer(data->commandBuffer), gen::GraphicsCommandExecutionException, "Can't End Command Buffer")
}
