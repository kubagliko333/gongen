#include "vk_graphics.hpp"

#define COMMAND_BUFFER_DESCRIPTORS_SIZE (sizeof(gen::vkDescriptor) * gen::MAX_DESCRIPTOR_SETS * gen::MAX_DESCRIPTOR_SLOTS)

gen::vkSemaphore::vkSemaphore(vkGraphicsDevice* device) {
	this->device = device;
	VkSemaphoreCreateInfo createInfo = { VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO, NULL };
	createInfo.flags = 0;
	VK_ERROR_TEST(vkCreateSemaphore(device->device, &createInfo, nullptr, &semaphore), gen::GraphicsRuntimeException, "Can't Create Vulkan Semaphore")
}

gen::vkSemaphore::~vkSemaphore() {
	vkDestroySemaphore(this->device->device, semaphore, nullptr);
}

gen::vkCommandBuffer::vkCommandBuffer(vkCommandBufferData* data, vkGraphicsDevice* device) {
	this->data = data;
	this->device = device;
	memset(descriptor, 0, COMMAND_BUFFER_DESCRIPTORS_SIZE);
	memset(descriptorSetDirtyFlag, 0, gen::MAX_DESCRIPTOR_SETS * sizeof(bool));
}

gen::vkCommandBuffer::~vkCommandBuffer() {
}

gen::vkCommand* gen::vkCommandBuffer::nextCommand(gen::vkCommandType type) {
	data->commands.push_back(gen::vkCommand());
	data->commands.back().type = type;
	return &data->commands.back();
}

gen::BufferPtr gen::vkCommandBuffer::createDynamicBuffer(uint32_t bindFlags, uint64_t size, bool gpuAccessOptimal) {
	gen::BufferDesc desc;
	desc.bindFlags = bindFlags;
	desc.size = size;
	desc.type = gen::BufferType::STATIC;
	VmaPool pool = data->dynamicBufferPool_CPUToGPU;
	if (!gpuAccessOptimal) pool = data->dynamicBufferPool_CPU;
	gen::BufferPtr buffer = device->createBuffer_internal(desc, true, pool);
	data->dynamicBuffers.push_back(buffer);
	return buffer;
}

void gen::vkCommandBuffer::beginRenderPass(const gen::RenderPassDesc& desc) {
	vkCommand* cmd = nextCommand(gen::vkCommandType::BEGIN_RENDER_PASS);
	cmd->renderPass = device->getRenderPass(desc);

	for (uint32_t i = 0; i < gen::MAX_COLOR_ATTACHMENTS; i++) {
		gen::vkTexture* vkTexture = (gen::vkTexture*)desc.colorAttachments[i].texture.get();
		if (vkTexture) {
			vkResourceAccess* access = cmd->nextResourceAccess();
			access->write = true;
			access->image = true;
			access->texture = vkTexture;
			access->imageLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
			access->srcStage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
			access->srcAccess = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		}
	}
	gen::vkTexture* vkTexture = (gen::vkTexture*)desc.depthStencilAttachment.texture.get();
	if (vkTexture) {
		vkResourceAccess* access = cmd->nextResourceAccess();
		access->write = true;
		access->image = true;
		access->texture = vkTexture;
		access->imageLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
		access->srcStage = VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;
		access->srcAccess = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
	}

	data->activeRenderPass = device->getRenderPass(desc);
}

void gen::vkCommandBuffer::endRenderPass() {
	vkCommand* cmd = nextCommand(gen::vkCommandType::END_RENDER_PASS);
	for (uint32_t i = 0; i < data->activeRenderPass.colorAttachments; i++) {
		gen::vkTexture* vkTexture = data->activeRenderPass.textures[i];
		if (vkTexture) {
			if (vkTexture->swapchain) {
				vkResourceAccess* access = cmd->nextResourceAccess();
				access->write = false;
				access->image = true;
				access->texture = vkTexture;
				access->imageLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
				access->dstStage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
				access->dstAccess = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT;
			}
		}
	}
}

void gen::vkCommandBuffer::updateBuffer(const BufferPtr& buffer, uint64_t offset, uint64_t size, void* data) {
	if (buffer.get()) {
		gen::vkBuffer* vkBuffer = (gen::vkBuffer*)buffer.get();
		if (!vkBuffer) {
			throw gen::GraphicsCommandException("Invalid Buffer");
		}
		if (!vkBuffer->dynamicBuffer) {
			vkCommand* cmd = nextCommand(gen::vkCommandType::COPY_BUFFER);

			gen::vkBuffer* tempBuffer = (gen::vkBuffer*)createDynamicBuffer(gen::BufferBindFlag::BUFFER_BIND_TRANSFER_SRC, size, false).get();
			memcpy(&((uint8_t*)tempBuffer->allocationInfo.pMappedData)[offset], data, size);

			cmd->copyBuffer.srcOffset = 0;
			cmd->copyBuffer.srcBuffer = tempBuffer;
			cmd->copyBuffer.size = size;
			cmd->copyBuffer.dstOffset = offset;
			cmd->copyBuffer.dstBuffer = vkBuffer;

			vkResourceAccess* access = cmd->nextResourceAccess();
			access->write = true;
			access->image = false;
			access->buffer = cmd->copyBuffer.dstBuffer;
			access->srcStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
			access->srcAccess = VK_ACCESS_TRANSFER_WRITE_BIT;
		}
		else {
			memcpy(&((uint8_t*)vkBuffer->allocationInfo.pMappedData)[offset], data, size);
		}
	}
}

void gen::vkCommandBuffer::updateTexture(const TexturePtr& texture, TextureOffset offset, uint32_t sizeX, uint32_t sizeY, uint32_t sizeZ, void* data) {
	if (texture.get()) {
		gen::vkTexture *vkTexture = (gen::vkTexture*)texture.get();
		if (!vkTexture) {
			throw gen::GraphicsCommandException("Invalid Texture");
		}

		vkCommand* cmd = nextCommand(gen::vkCommandType::COPY_BUFFER_TO_TEXTURE);

		uint64_t dataSize = gen::getTextureSize(vkTexture->textureDesc.format, sizeX, sizeY, sizeZ, offset.layers);

		gen::vkBuffer* tempBuffer = (gen::vkBuffer*)createDynamicBuffer(gen::BufferBindFlag::BUFFER_BIND_TRANSFER_SRC, dataSize, false).get();
		memcpy(tempBuffer->allocationInfo.pMappedData, data, dataSize);

		cmd->copyBufferToTexture.srcBuffer = tempBuffer;
		cmd->copyBufferToTexture.dstTexture = vkTexture;
		cmd->copyBufferToTexture.dstOffset = offset;
		cmd->copyBufferToTexture.srcOffset = 0;
		cmd->copyBufferToTexture.sizeX = sizeX;
		cmd->copyBufferToTexture.sizeY = sizeY;
		cmd->copyBufferToTexture.sizeZ = sizeZ;

		vkResourceAccess* access = cmd->nextResourceAccess();
		access->write = true;
		access->image = true;
		access->texture = vkTexture;
		access->imageLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
		access->srcStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
		access->srcAccess = VK_ACCESS_TRANSFER_WRITE_BIT;
	}
}
void gen::vkCommandBuffer::bindVertexBuffers(const BufferPtr& vertexBuffer, uint32_t slot, uint64_t offset) {
	if (vertexBuffer.get()) {
		gen::vkBuffer* vkBuffer = (gen::vkBuffer*)vertexBuffer.get();
		if (!vkBuffer) {
			throw gen::GraphicsCommandException("Invalid Buffer");
		}

		vkCommand* cmd = nextCommand(gen::vkCommandType::SET_VERTEX_BUFFER);
		cmd->setVertexBuffer.buffer = (gen::vkBuffer*)vertexBuffer.get();
		cmd->setVertexBuffer.slot = slot;
		cmd->setVertexBuffer.offset = offset;

		vkResourceAccess* access = cmd->nextResourceAccess();
		access->write = false;
		access->image = false;
		access->buffer = (gen::vkBuffer*)vertexBuffer.get();;
		access->dstStage = VK_PIPELINE_STAGE_VERTEX_INPUT_BIT;
		access->dstAccess = VK_ACCESS_INDEX_READ_BIT;
	}
}

void gen::vkCommandBuffer::bindIndexBuffer(const gen::BufferPtr& indexBuffer, uint64_t offset, gen::IndexType type) {
	if (indexBuffer.get()) {
		gen::vkBuffer* vkBuffer = (gen::vkBuffer*)indexBuffer.get();
		if (!vkBuffer) {
			throw gen::GraphicsCommandException("Invalid Buffer");
		}

		vkCommand* cmd = nextCommand(gen::vkCommandType::SET_INDEX_BUFFER);
		cmd->setIndexBuffer.buffer = (gen::vkBuffer*)indexBuffer.get();;
		cmd->setIndexBuffer.indexType = type;
		cmd->setIndexBuffer.offset = offset;

		vkResourceAccess* access = cmd->nextResourceAccess();
		access->write = false;
		access->image = false;
		access->buffer = (gen::vkBuffer*)indexBuffer.get();;;
		access->dstStage = VK_PIPELINE_STAGE_VERTEX_INPUT_BIT;
		access->dstAccess = VK_ACCESS_INDEX_READ_BIT;
	}
}

void gen::vkCommandBuffer::bindPipeline(const gen::PipelineDesc& desc) {
	memset(descriptorSetDirtyFlag, 0, gen::MAX_DESCRIPTOR_SETS * sizeof(bool));
	vkCommand* cmd2 = nextCommand(gen::vkCommandType::SET_PIPELINE);
	cmd2->pipeline = device->getPipeline(desc, &data->activeRenderPass);
	data->activePipeline = cmd2->pipeline;
}

void gen::vkCommandBuffer::setScissor(const gen::ScissorDesc& scissor) {
	vkCommand* cmd2 = nextCommand(gen::vkCommandType::SET_SCISSOR);
	VkRect2D rect;
	rect.offset = { (int)scissor.x, (int)scissor.y };
	rect.extent = { scissor.width, scissor.height };
	cmd2->scissor = rect;
}

void gen::vkCommandBuffer::setViewport(const gen::ViewportDesc& viewport) {
	vkCommand* cmd2 = nextCommand(gen::vkCommandType::SET_VIEWPORT);
	cmd2->viewport.x = viewport.x;
	cmd2->viewport.y = viewport.y;
	cmd2->viewport.width = viewport.width;
	cmd2->viewport.height = viewport.height;
	cmd2->viewport.minDepth = viewport.minDepth;
	cmd2->viewport.maxDepth = viewport.maxDepth;
}

VkPipelineStageFlags getEarlistStageForBinding(gen::vkShader* shader, uint32_t set, uint32_t slot) {
	for (uint32_t i = 0; i < (size_t)gen::ShaderStage::TOTAL_SHADER_STAGES; i++) {
		if (shader->metadata[i].descriptors[set][slot] != gen::SPIRVDescriptorType::INVALID) {
			if (i == (size_t)gen::ShaderStage::VERTEX_SHADER) return VK_PIPELINE_STAGE_VERTEX_SHADER_BIT;
			else if (i == (size_t)gen::ShaderStage::HULL_SHADER) return VK_PIPELINE_STAGE_TESSELLATION_CONTROL_SHADER_BIT;
			else if (i == (size_t)gen::ShaderStage::DOMAIN_SHADER) return VK_PIPELINE_STAGE_TESSELLATION_EVALUATION_SHADER_BIT;
			else if (i == (size_t)gen::ShaderStage::GEOMETRY_SHADER) return VK_PIPELINE_STAGE_GEOMETRY_SHADER_BIT;
			else if (i == (size_t)gen::ShaderStage::PIXEL_SHADER) return VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
			else if (i == (size_t)gen::ShaderStage::COMPUTE_SHADER) return VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT;
		}
	}
	return VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
}

void gen::vkCommandBuffer::processDescritporSetsChange() {
	for (uint32_t i = 0; i < gen::MAX_DESCRIPTOR_SETS; i++) {
		if (descriptorSetDirtyFlag[i]) {
			vkCommand* cmd = nextCommand(gen::vkCommandType::SET_DESCRIPTOR_SETS);
			cmd->setDescriptorSets.setsAmount = 1;
			cmd->setDescriptorSets.firstSet = i;
			cmd->setDescriptorSets.sets[0] = data->activePipeline.shader->getDescriptorSet(this->descriptor[i], i, data->commandBuffer);
			data->shaders.push_back(data->activePipeline.shader);

			for (uint32_t i2 = 0; i2 < gen::MAX_DESCRIPTOR_SLOTS; i2++) {
				if (descriptor[i][i2].type == gen::SPIRVDescriptorType::SEPARATE_IMAGE) {
					vkResourceAccess* access = cmd->nextResourceAccess();
					access->image = true;
					access->write = false;
					access->dstStage = getEarlistStageForBinding(data->activePipeline.shader, i, i2);
					access->dstAccess = VK_ACCESS_SHADER_READ_BIT;
					access->imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
					access->texture = descriptor[i][i2].texture;
				}
			}
		}
	}
	memset(descriptorSetDirtyFlag, 0, gen::MAX_DESCRIPTOR_SETS * sizeof(bool));
}

void gen::vkCommandBuffer::setUniformBuffer(uint32_t set, uint32_t slot, const gen::BufferPtr& buffer, uint64_t offset, uint64_t size) {
	gen::vkBuffer* vkBuffer = (gen::vkBuffer*)buffer.get();
	if (!vkBuffer) {
		throw gen::GraphicsCommandException("Invalid Buffer");
	}

	gen::vkDescriptor oldDescriptor = descriptor[set][slot];

	descriptor[set][slot].type = gen::SPIRVDescriptorType::UNIFORM_BUFFER;
	descriptor[set][slot].buffer = (gen::vkBuffer*)buffer.get();
	descriptor[set][slot].offset = offset;
	if (size == 0) descriptor[set][slot].size = VK_WHOLE_SIZE;
	else descriptor[set][slot].size = size;

	if (memcmp(&oldDescriptor, &descriptor[set][slot], sizeof(gen::vkDescriptor))) descriptorSetDirtyFlag[set] = true;
}

void gen::vkCommandBuffer::setTexture(uint32_t set, uint32_t slot, const gen::TexturePtr& texture) {
	gen::vkTexture* vkTexture = (gen::vkTexture*)texture.get();
	if (!vkTexture) {
		throw gen::GraphicsCommandException("Invalid Texture");
	}

	gen::vkDescriptor oldDescriptor = descriptor[set][slot];

	descriptor[set][slot].type = gen::SPIRVDescriptorType::SEPARATE_IMAGE;
	descriptor[set][slot].texture = (gen::vkTexture*)texture.get();

	if (memcmp(&oldDescriptor, &descriptor[set][slot], sizeof(gen::vkDescriptor))) descriptorSetDirtyFlag[set] = true;
}

void gen::vkCommandBuffer::setSampler(uint32_t set, uint32_t slot, const gen::SamplerDesc& desc) {
	gen::vkDescriptor oldDescriptor = descriptor[set][slot];

	descriptor[set][slot].type = gen::SPIRVDescriptorType::SAMPLER;
	descriptor[set][slot].sampler = device->getSampler(desc);

	if (memcmp(&oldDescriptor, &descriptor[set][slot], sizeof(gen::vkDescriptor))) descriptorSetDirtyFlag[set] = true;
}

void gen::vkCommandBuffer::draw(uint32_t vertexCount, uint32_t firstVertex, uint32_t instanceCount, uint32_t firstInstance) {
	processDescritporSetsChange();

	vkCommand* cmd2 = nextCommand(gen::vkCommandType::DRAW);
	cmd2->draw.vertexCount = vertexCount;
	cmd2->draw.firstVertex = firstVertex;
	cmd2->draw.instanceCount = instanceCount;
	cmd2->draw.firstInstance = firstInstance;
}

void gen::vkCommandBuffer::drawIndexed(uint32_t indexCount, uint32_t firstIndex, int32_t vertexOffset, uint32_t instanceCount, uint32_t firstInstance) {
	processDescritporSetsChange();

	vkCommand* cmd2 = nextCommand(gen::vkCommandType::DRAW_INDEXED);
	cmd2->draw.indexCount = indexCount;
	cmd2->draw.firstIndex = firstIndex;
	cmd2->draw.vertexOffset = vertexOffset;
	cmd2->draw.instanceCount = instanceCount;
	cmd2->draw.firstInstance = firstInstance;
}

void gen::vkCommandBuffer::drawIndirect(const gen::BufferPtr& buffer, uint32_t drwaCount, uint32_t offset) {
	processDescritporSetsChange();

	gen::vkBuffer* vkBuffer = (gen::vkBuffer*)buffer.get();

	vkCommand* cmd2 = nextCommand(gen::vkCommandType::DRAW_INDIRECT);
	cmd2->draw.indirectBuffer = vkBuffer;
	cmd2->draw.drawAmount = drwaCount;
	cmd2->draw.offset = offset;

	vkResourceAccess* access = cmd2->nextResourceAccess();
	access->write = false;
	access->image = false;
	access->buffer = vkBuffer;
	access->srcStage = VK_PIPELINE_STAGE_DRAW_INDIRECT_BIT;
	access->srcStage = VK_ACCESS_INDIRECT_COMMAND_READ_BIT;
}

void gen::vkCommandBuffer::drawIndexedIndirect(const gen::BufferPtr& buffer, uint32_t drwaCount, uint32_t offset) {
	processDescritporSetsChange();

	gen::vkBuffer* vkBuffer = (gen::vkBuffer*)buffer.get();

	vkCommand* cmd2 = nextCommand(gen::vkCommandType::DRAW_INDEXED_INDIRECT);
	cmd2->draw.indirectBuffer = vkBuffer;
	cmd2->draw.drawAmount = drwaCount;
	cmd2->draw.offset = offset;

	vkResourceAccess* access = cmd2->nextResourceAccess();
	access->write = false;
	access->image = false;
	access->buffer = vkBuffer;
	access->srcStage = VK_PIPELINE_STAGE_DRAW_INDIRECT_BIT;
	access->srcStage = VK_ACCESS_INDIRECT_COMMAND_READ_BIT;
}

void gen::vkCommandBuffer::execute(CommandBuffer* waitFor) {
	for (auto& s : data->swapchains) {
		if (s->minimized) {
			return;
		}
	}

	if (waitFor) {
		data->waitSempahores.push_back(((gen::vkCommandBuffer*)waitFor)->getSignalSemaphore());
	}
	buildCommandBuffer();

	std::vector<VkSemaphore> waitSemaphores;
	std::vector<VkPipelineStageFlags> waitDstStages;
	std::vector<VkSemaphore> signalSemaphores;

	for (auto& s : data->waitSempahores) {
		waitSemaphores.push_back(s->semaphore);
		waitDstStages.push_back(VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT);
	}

	if (data->signalSemaphore) {
		signalSemaphores.push_back(data->signalSemaphore->semaphore);
	}

	VkSubmitInfo submitInfo = { VK_STRUCTURE_TYPE_SUBMIT_INFO, NULL };
	submitInfo.waitSemaphoreCount = waitSemaphores.size();
	submitInfo.pWaitSemaphores = waitSemaphores.data();
	submitInfo.pWaitDstStageMask = waitDstStages.data();
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &data->commandBuffer;
	submitInfo.signalSemaphoreCount = signalSemaphores.size();
	submitInfo.pSignalSemaphores = signalSemaphores.data();

	VK_ERROR_TEST(vkQueueSubmit(device->queue, 1, &submitInfo, data->fence), gen::GraphicsCommandExecutionException, "Graphics Execution Error")
}

bool gen::vkCommandBuffer::executionDone() {
	return (vkGetFenceStatus(device->device, data->fence) == VK_SUCCESS);
}

void gen::vkCommandBuffer::waitForExecution() {
	vkWaitForFences(device->device, 1, &data->fence, VK_TRUE, UINT64_MAX);
}

void gen::vkCommandBuffer::display(Swapchain* swapchian) {
	gen::vkSwapchain* vkSwapchain = (gen::vkSwapchain*)swapchian;
	if (!vkSwapchain->minimized) {
		VkResult presentResult;

		VkPresentInfoKHR presentInfo = { VK_STRUCTURE_TYPE_PRESENT_INFO_KHR, NULL };
		presentInfo.waitSemaphoreCount = 1;
		presentInfo.pWaitSemaphores = &data->signalSemaphore->semaphore;
		presentInfo.swapchainCount = 1;
		presentInfo.pSwapchains = &vkSwapchain->swapchain;
		presentInfo.pImageIndices = &vkSwapchain->activeImage;
		presentInfo.pResults = &presentResult;

		VkResult result = vkQueuePresentKHR(device->queue, &presentInfo);
		if (result != VK_SUCCESS) {
			if (result == VK_SUBOPTIMAL_KHR) {
			}
			else if (result == VK_ERROR_OUT_OF_DATE_KHR) {
				vkSwapchain->createSwapchain();
			}
			else {

			}
		}

		if (presentResult != VK_SUCCESS) {

		}

		vkSwapchain->nextFrame();
	}
}

gen::vkSemaphorePtr gen::vkCommandBuffer::addWaitSemaphore() {
	data->waitSempahores.push_back(std::make_shared<gen::vkSemaphore>(device));
	return data->waitSempahores.back();
}

gen::vkSemaphorePtr gen::vkCommandBuffer::getSignalSemaphore() {
	return data->signalSemaphore;
}
