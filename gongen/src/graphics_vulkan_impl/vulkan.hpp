#pragma once

#include <gongen/core/platform.hpp>
#ifdef PLATFORM_WIN32
	#define VK_USE_PLATFORM_WIN32_KHR
#endif

#ifdef PLATFORM_APPLE
	#define VK_USE_PLATFORM_METAL_EXT
#endif

#ifdef PLATFORM_ANDROID
	#define VK_USE_PLATFORM_ANDROID_KHR
#endif

#ifdef PLATFORM_LINUX
	#define VK_USE_PLATFORM_WAYLAND_KHR
#endif

#if defined(PLATFORM_UNIX) && !defined(PLATFORM_ANDROID)
	#define VK_USE_PLATFORM_XLIB_KHR
#endif

#include <volk.h>
