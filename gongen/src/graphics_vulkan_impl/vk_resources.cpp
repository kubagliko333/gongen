#include "vk_graphics.hpp"

extern VkFormat formatConversionTable[(size_t)gen::GPUFormat::TOTAL_TEXTURE_FORMATS];

gen::BufferPtr gen::vkGraphicsDevice::createBuffer_internal(const gen::BufferDesc& desc, bool dynamicBuffer, VmaPool pool) {
	gen::vkBuffer *buffer = new gen::vkBuffer();

	uint32_t usageFlags = 0;
	if (desc.bindFlags & gen::BufferBindFlag::BUFFER_BIND_TRANSFER_SRC) usageFlags |= VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
	if (desc.bindFlags & gen::BufferBindFlag::BUFFER_BIND_TRANSFER_DST) usageFlags |= VK_BUFFER_USAGE_TRANSFER_DST_BIT;
	if (desc.bindFlags & gen::BufferBindFlag::BUFFER_BIND_UNIFORM_BUFFER) usageFlags |= VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
	if (desc.bindFlags & gen::BufferBindFlag::BUFFER_BIND_STORAGE_BUFFER) usageFlags |= VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
	if (desc.bindFlags & gen::BufferBindFlag::BUFFER_BIND_INDEX_BUFFER) usageFlags |= VK_BUFFER_USAGE_INDEX_BUFFER_BIT;
	if (desc.bindFlags & gen::BufferBindFlag::BUFFER_BIND_VERTEX_BUFFER) usageFlags |= VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
	if (desc.bindFlags & gen::BufferBindFlag::BUFFER_BIND_INDIRECT_BUFFER) usageFlags |= VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT;
	if (desc.type == gen::BufferType::STATIC) usageFlags |= VK_BUFFER_USAGE_TRANSFER_DST_BIT;

	buffer->usageFlags = usageFlags;
	VkBufferCreateInfo createInfo = { VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO, NULL };
	createInfo.flags = 0;
	createInfo.size = desc.size;
	createInfo.usage = usageFlags;
	createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	createInfo.queueFamilyIndexCount = 0;
	createInfo.pQueueFamilyIndices = NULL;

	VmaAllocationCreateInfo allocInfo;
	allocInfo.flags = 0;
	if (dynamicBuffer) allocInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT;
	if (desc.type == gen::BufferType::STAGING) allocInfo.usage = VMA_MEMORY_USAGE_CPU_ONLY;
	if (desc.type == gen::BufferType::STATIC) allocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;
	if (desc.type == gen::BufferType::READBACK) allocInfo.usage = VMA_MEMORY_USAGE_GPU_TO_CPU;
	allocInfo.requiredFlags = 0;
	allocInfo.preferredFlags = 0;
	allocInfo.memoryTypeBits = 0;
	allocInfo.pool = pool;
	allocInfo.pUserData = nullptr;
	allocInfo.priority = 1.0f;

	buffer->device = this;
	buffer->lastFence = 0;
	buffer->dynamicBuffer = dynamicBuffer;
	VK_ERROR_TEST(vmaCreateBuffer(allocator, &createInfo, &allocInfo, &buffer->buffer, &buffer->allocation, &buffer->allocationInfo), gen::GraphicsResourceCreationException, "Buffer Creation Failed")
	return std::shared_ptr<void>(buffer);
}

gen::BufferPtr gen::vkGraphicsDevice::createBuffer(const gen::BufferDesc& desc) {
	return createBuffer_internal(desc, false, nullptr);
}

gen::TexturePtr gen::vkGraphicsDevice::createTexture(const gen::TextureDesc& desc) {
	gen::TexturePtr texture2 = std::shared_ptr<void>(new gen::vkTexture());
	gen::vkTexture* texture = (gen::vkTexture*)texture2.get();

	uint32_t usageFlags = 0;
	if (desc.bindFlags & gen::TextureBindFlag::TEXTURE_BIND_TRANSFER_SRC) usageFlags |= VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
	if (desc.bindFlags & gen::TextureBindFlag::TEXTURE_BIND_TRANSFER_DST) usageFlags |= VK_IMAGE_USAGE_TRANSFER_DST_BIT;
	if (desc.bindFlags & gen::TextureBindFlag::TEXTURE_BIND_SAMPLED) usageFlags |= VK_IMAGE_USAGE_SAMPLED_BIT;
	if (desc.bindFlags & gen::TextureBindFlag::TEXTURE_BIND_STORAGE) usageFlags |= VK_IMAGE_USAGE_STORAGE_BIT;
	if (desc.bindFlags & gen::TextureBindFlag::TEXTURE_BIND_COLOR_ATTACHMENT) usageFlags |= VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	if (desc.bindFlags & gen::TextureBindFlag::TEXTURE_BIND_DEPTH_STENCIL_ATTACHMENT) usageFlags |= VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;

	VkImageCreateInfo createInfo = { VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO, NULL };
	createInfo.flags = 0;
	if (desc.type == gen::TextureType::TEXTURE_CUBE_MAP) createInfo.flags = VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;
	if (desc.type == gen::TextureType::TEXTURE_1D) createInfo.imageType = VK_IMAGE_TYPE_1D;
	else if (desc.type == gen::TextureType::TEXTURE_2D) createInfo.imageType = VK_IMAGE_TYPE_2D;
	else if (desc.type == gen::TextureType::TEXTURE_3D) createInfo.imageType = VK_IMAGE_TYPE_3D;
	else if (desc.type == gen::TextureType::TEXTURE_CUBE_MAP) createInfo.imageType = VK_IMAGE_TYPE_2D;
	createInfo.format = formatConversionTable[(size_t)desc.format];
	createInfo.extent.width = desc.width;
	createInfo.extent.height = desc.height;
	createInfo.extent.depth = desc.depth;
	createInfo.mipLevels = desc.mipLevels;
	createInfo.arrayLayers = desc.layers;
	if (desc.samples == gen::TextureSamples::SAMPLE_COUNT_1) createInfo.samples = VK_SAMPLE_COUNT_1_BIT;
	else if (desc.samples == gen::TextureSamples::SAMPLE_COUNT_2) createInfo.samples = VK_SAMPLE_COUNT_2_BIT;
	else if (desc.samples == gen::TextureSamples::SAMPLE_COUNT_4) createInfo.samples = VK_SAMPLE_COUNT_4_BIT;
	else if (desc.samples == gen::TextureSamples::SAMPLE_COUNT_8) createInfo.samples = VK_SAMPLE_COUNT_8_BIT;
	else if (desc.samples == gen::TextureSamples::SAMPLE_COUNT_16) createInfo.samples = VK_SAMPLE_COUNT_16_BIT;
	else if (desc.samples == gen::TextureSamples::SAMPLE_COUNT_32) createInfo.samples = VK_SAMPLE_COUNT_32_BIT;
	else if (desc.samples == gen::TextureSamples::SAMPLE_COUNT_64) createInfo.samples = VK_SAMPLE_COUNT_64_BIT;
	createInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
	createInfo.usage = usageFlags;
	createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	createInfo.queueFamilyIndexCount = 0;
	createInfo.pQueueFamilyIndices = NULL;
	createInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

	VmaAllocationCreateInfo allocInfo;
	allocInfo.flags = 0;
	allocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;
	allocInfo.requiredFlags = 0;
	allocInfo.preferredFlags = 0;
	allocInfo.memoryTypeBits = 0;
	allocInfo.pool = 0;
	allocInfo.pUserData = nullptr;
	allocInfo.priority = 1.0f;
	
	VK_ERROR_TEST(vmaCreateImage(allocator, &createInfo, &allocInfo, &texture->image, &texture->allocation, &texture->allocationInfo), gen::GraphicsResourceCreationException, "Texture Creation Failed")

	VkImageViewCreateInfo viewCreateInfo = { VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO, NULL };
	viewCreateInfo.flags = 0;
	viewCreateInfo.image = texture->image;
	if ((desc.type == gen::TextureType::TEXTURE_1D) && (desc.layers == 1)) viewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_1D;
	else if ((desc.type == gen::TextureType::TEXTURE_1D) && (desc.layers > 1)) viewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_1D_ARRAY;
	else if ((desc.type == gen::TextureType::TEXTURE_2D) && (desc.layers == 1)) viewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
	else if ((desc.type == gen::TextureType::TEXTURE_2D) && (desc.layers > 1)) viewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D_ARRAY;
	else if ((desc.type == gen::TextureType::TEXTURE_3D) && (desc.layers == 1)) viewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_3D;
	else if ((desc.type == gen::TextureType::TEXTURE_CUBE_MAP) && (desc.layers == 1)) viewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_CUBE;
	else if ((desc.type == gen::TextureType::TEXTURE_CUBE_MAP) && (desc.layers > 1)) viewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_CUBE_ARRAY;
	viewCreateInfo.format = createInfo.format;
	viewCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
	viewCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
	viewCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
	viewCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
	viewCreateInfo.subresourceRange.baseArrayLayer = 0;
	viewCreateInfo.subresourceRange.baseMipLevel = 0;
	viewCreateInfo.subresourceRange.layerCount = desc.layers;
	viewCreateInfo.subresourceRange.levelCount = desc.mipLevels;
	if (desc.bindFlags & gen::TextureBindFlag::TEXTURE_BIND_DEPTH_STENCIL_ATTACHMENT) {
		viewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
	}
	else {
		viewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	}
	
	VK_ERROR_TEST(vkCreateImageView(device, &viewCreateInfo, nullptr, &texture->imageView), gen::GraphicsResourceCreationException, "Texture View Creation Failed")

	texture->textureDesc = desc;
	texture->samples = createInfo.samples;
	texture->usageFlags = usageFlags;
	texture->device = this;
	texture->lastFence = 0;
	texture->swapchain = nullptr;
	texture->swapchainImage = false;
	texture->currentLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	return texture2;
}

gen::vkBuffer::~vkBuffer() {
	if (!this->dynamicBuffer) {
		if(this->buffer) device->addBufferToDelete(this);
	}
}

gen::vkTexture::~vkTexture() {
	if(this->image) device->addTextureToDelete(this);
}

VkDescriptorType descriptorType(gen::SPIRVDescriptorType type) {
	if (type == gen::SPIRVDescriptorType::SAMPLER) return VK_DESCRIPTOR_TYPE_SAMPLER;
	else if (type == gen::SPIRVDescriptorType::SEPARATE_IMAGE) return VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
	else if (type == gen::SPIRVDescriptorType::STORAGE_IMAGE) return VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
	else if (type == gen::SPIRVDescriptorType::UNIFORM_BUFFER) return VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	else if (type == gen::SPIRVDescriptorType::STORAGE_BUFFER) return VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
	return VK_DESCRIPTOR_TYPE_SAMPLER;
}

gen::ShaderPtr gen::vkGraphicsDevice::createShader(const gen::ShaderDesc& desc) {
	vkShader shader;
	for (uint32_t i = 0; i < (size_t)gen::ShaderStage::TOTAL_SHADER_STAGES; i++) {
		shader.metadata[i] = desc.stages[i].metadata;
	}

	bool descriptorSet[gen::MAX_DESCRIPTOR_SETS] = { false, false, false, false };
	VkDescriptorSetLayoutCreateInfo createInfo[gen::MAX_DESCRIPTOR_SETS];
	std::vector<VkDescriptorSetLayoutBinding> bindings[gen::MAX_DESCRIPTOR_SETS];

	gen::SPIRVDescriptorType shaderDescriptors[gen::MAX_DESCRIPTOR_SETS][gen::MAX_DESCRIPTOR_SLOTS];
	memset(shaderDescriptors, (size_t)gen::SPIRVDescriptorType::INVALID, sizeof(gen::SPIRVDescriptorType) * gen::MAX_DESCRIPTOR_SETS * gen::MAX_DESCRIPTOR_SLOTS);

	for (uint32_t i = 0; i < (size_t)gen::ShaderStage::TOTAL_SHADER_STAGES; i++) {
		if ((1 << i) & desc.stageFlags) {
			VkShaderModuleCreateInfo moduleInfo = { VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO, NULL };
			VkPipelineShaderStageCreateInfo stageInfo = { VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO, NULL };

			moduleInfo.flags = 0;
			moduleInfo.codeSize = (desc.stages[i].spirv.size() * sizeof(uint32_t));
			moduleInfo.pCode = desc.stages[i].spirv.data();
			
			VK_ERROR_TEST(vkCreateShaderModule(device, &moduleInfo, nullptr, &shader.modules[i]), gen::GraphicsResourceCreationException, "Can't Create Shader Module")

			stageInfo.flags = 0;
			stageInfo.pSpecializationInfo = nullptr;
			stageInfo.module = shader.modules[i];
			if (i == (size_t)gen::ShaderStage::VERTEX_SHADER) {
				stageInfo.pName = "vertexShader";
				stageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
			}
			else if (i == (size_t)gen::ShaderStage::HULL_SHADER) {
				stageInfo.pName = "hullShader";
				stageInfo.stage = VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
			}
			else if (i == (size_t)gen::ShaderStage::DOMAIN_SHADER) {
				stageInfo.pName = "domainShader";
				stageInfo.stage = VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
			}
			else if (i == (size_t)gen::ShaderStage::GEOMETRY_SHADER) {
				stageInfo.pName = "geometryShader";
				stageInfo.stage = VK_SHADER_STAGE_GEOMETRY_BIT;
			}
			else if (i == (size_t)gen::ShaderStage::PIXEL_SHADER) {
				stageInfo.pName = "pixelShader";
				stageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
			}
			else if (i == (size_t)gen::ShaderStage::COMPUTE_SHADER) {
				stageInfo.pName = "computeShader";
				stageInfo.stage = VK_SHADER_STAGE_COMPUTE_BIT;
			}

			shader.pipelineStageCreateInfo.push_back(stageInfo);

			for (uint32_t set = 0; set < gen::MAX_DESCRIPTOR_SETS; set++) {
				for (uint32_t slot = 0; slot < gen::MAX_DESCRIPTOR_SLOTS; slot++) {
					gen::SPIRVDescriptorType descriptor = desc.stages[i].metadata.descriptors[set][slot];
					bool isDescriptorValid = (descriptor != gen::SPIRVDescriptorType::INVALID);
					shader.descriptorExist[set][slot] = isDescriptorValid;
					if (isDescriptorValid) {
						descriptorSet[set] = true;

						auto* v = &bindings[set];
						bool bindingExist = false;
						for (auto& p : (*v)) {
							if (p.binding == slot) bindingExist = true;
						}
						if (!bindingExist) {
							VkDescriptorSetLayoutBinding binding;
							binding.binding = slot;
							binding.descriptorCount = 1;
							binding.pImmutableSamplers = nullptr;
							binding.stageFlags = stageInfo.stage;
							if (descriptor == gen::SPIRVDescriptorType::SAMPLER) binding.descriptorType = VK_DESCRIPTOR_TYPE_SAMPLER;
							else if (descriptor == gen::SPIRVDescriptorType::SEPARATE_IMAGE) binding.descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
							else if (descriptor == gen::SPIRVDescriptorType::STORAGE_BUFFER) binding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
							else if (descriptor == gen::SPIRVDescriptorType::STORAGE_IMAGE) binding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
							else if (descriptor == gen::SPIRVDescriptorType::UNIFORM_BUFFER) binding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;

							v->push_back(binding);

							shaderDescriptors[set][slot] = descriptor;
						}
					}
				}
			}
		}
	}

	shader.amountOfSetLayouts = 0;
	for (uint32_t set = 0; set < gen::MAX_DESCRIPTOR_SETS; set++) {
		if (descriptorSet[set]) {
			shader.amountOfSetLayouts++;
			createInfo[set] = { VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO, NULL };
			createInfo[set].flags = 0;
			createInfo[set].bindingCount = bindings[set].size();
			createInfo[set].pBindings = bindings[set].data();
			
			VK_ERROR_TEST(vkCreateDescriptorSetLayout(device, &createInfo[set], nullptr, &shader.setLayout[set]), gen::GraphicsResourceCreationException, "Can't Create Descriptor Set Layout")
		}
	}

	uint32_t descriptorAmount[(size_t)gen::SPIRVDescriptorType::TOTAL_SPIRV_DESCRIPTOR_TYPES];
	memset(descriptorAmount, 0, sizeof(uint32_t) * (size_t)gen::SPIRVDescriptorType::TOTAL_SPIRV_DESCRIPTOR_TYPES);
	for (uint32_t set = 0; set < gen::MAX_DESCRIPTOR_SETS; set++) {
		for (uint32_t slot = 0; slot < gen::MAX_DESCRIPTOR_SLOTS; slot++) {
			if (shaderDescriptors[set][slot] != gen::SPIRVDescriptorType::INVALID) {
				descriptorAmount[(size_t)shaderDescriptors[set][slot]]++;
			}
		}
	}
	for (uint32_t i = 0; i < (size_t)gen::SPIRVDescriptorType::TOTAL_SPIRV_DESCRIPTOR_TYPES; i++) {
		uint32_t amount = descriptorAmount[i];

		if (amount != 0) {
			VkDescriptorPoolSize size;
			size.type = descriptorType((gen::SPIRVDescriptorType)i);
			size.descriptorCount = (amount * gen::DESCRIPTOR_POOL_SIZE);
			shader.poolSizes.push_back(size);
		}
	}

	VkPipelineLayoutCreateInfo layoutInfo = { VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO, NULL };
	layoutInfo.flags = 0;
	layoutInfo.setLayoutCount = shader.amountOfSetLayouts;
	layoutInfo.pSetLayouts = shader.setLayout;
	layoutInfo.pushConstantRangeCount = 0;
	layoutInfo.pPushConstantRanges = nullptr;
	
	VK_ERROR_TEST(vkCreatePipelineLayout(device, &layoutInfo, nullptr, &shader.layout), gen::GraphicsResourceCreationException, "Can't Create Pipeline Layout")

	shader.device = this;
	gen::vkShader* shader2 = new gen::vkShader();
	*shader2 = shader;
	return std::shared_ptr<void>(shader2);
}

void gen::vkShader::resetDesciptorSets(VkCommandBuffer commandBuffer) {
	for (auto& p : pools) {
		if (p.commandBuffer == commandBuffer) {
			if (p.used != 0) {
				VK_ERROR_TEST(vkResetDescriptorPool(device->device, p.pool, 0), gen::GraphicsCommandExecutionException, "Can't Reset Descriptor Sets")
				p.used = 0;
			}
		}
	}
}

VkDescriptorSet gen::vkShader::getDescriptorSet(gen::vkDescriptor* descriptors, uint32_t set, VkCommandBuffer commandBuffer) {
	VkDescriptorPool pool = 0;
	for (auto &p : pools) {
		if (p.commandBuffer == commandBuffer) {
			if (p.used < p.size) {
				pool = p.pool;
				p.used++;
			}
		}
	}
	if (!pool) {
		VkDescriptorPoolCreateInfo createInfo = { VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO, NULL };
		createInfo.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
		createInfo.maxSets = gen::DESCRIPTOR_POOL_SIZE;
		createInfo.poolSizeCount = this->poolSizes.size();
		createInfo.pPoolSizes = this->poolSizes.data();

		vkDescriptorPool pool2;
		pool2.size = gen::DESCRIPTOR_POOL_SIZE;
		pool2.used = 1;
		pool2.commandBuffer = commandBuffer;
		VK_ERROR_TEST(vkCreateDescriptorPool(device->device, &createInfo, nullptr, &pool2.pool), gen::GraphicsCommandException, "Can't Create Descriptor Set")

		pools.push_back(pool2);
		pool = pools.back().pool;
	}
	
	VkDescriptorSet descriptorSet;

	VkDescriptorSetAllocateInfo allocInfo = { VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO, NULL };
	allocInfo.descriptorPool = pool;
	allocInfo.descriptorSetCount = this->amountOfSetLayouts;
	allocInfo.pSetLayouts = &this->setLayout[set];

	VK_ERROR_TEST(vkAllocateDescriptorSets(device->device, &allocInfo, &descriptorSet), gen::GraphicsCommandException, "Can't Allocate Descriptor Set")

	std::vector<VkWriteDescriptorSet> setWrites;
	for (uint32_t i = 0; i < gen::MAX_DESCRIPTOR_SLOTS; i++) {
		gen::vkDescriptor* r = &descriptors[i];
		if (r->type != gen::SPIRVDescriptorType::INVALID) {
			VkWriteDescriptorSet write = { VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET, NULL };
			write.dstSet = descriptorSet;
			write.dstBinding = i;
			write.dstArrayElement = 0;
			write.descriptorCount = 1;
			write.descriptorType = descriptorType(r->type);
			write.pTexelBufferView = nullptr;
			write.pImageInfo = nullptr;
			write.pBufferInfo = nullptr;

			VkDescriptorImageInfo image;
			VkDescriptorBufferInfo buffer;

			if (r->type == gen::SPIRVDescriptorType::SAMPLER) {
				image.sampler = r->sampler;
				write.pImageInfo = &image;
			}
			else if ((r->type == gen::SPIRVDescriptorType::UNIFORM_BUFFER) || (r->type == gen::SPIRVDescriptorType::STORAGE_BUFFER)) {
				buffer.buffer = r->buffer->buffer;
				buffer.offset = 0;
				buffer.range = VK_WHOLE_SIZE;
				write.pBufferInfo = &buffer;
			}
			else if ((r->type == gen::SPIRVDescriptorType::SEPARATE_IMAGE) || (r->type == gen::SPIRVDescriptorType::STORAGE_IMAGE)) {
				image.imageView = r->texture->imageView;
				image.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
				write.pImageInfo = &image;
			}

			setWrites.push_back(write);
		}
	}
	
	vkUpdateDescriptorSets(device->device, setWrites.size(), setWrites.data(), 0, nullptr);

	return descriptorSet;
}
