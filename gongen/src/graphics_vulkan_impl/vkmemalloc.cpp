#include "vulkan.hpp"
#define VMA_IMPLEMENTATION
#define VMA_RECORDING_ENABLED 0
#define VMA_USE_STL_CONTAINERS 1
#define VMA_STATIC_VULKAN_FUNCTIONS 0
#define VMA_DYNAMIC_VULKAN_FUNCTIONS 0
#define VMA_DEBUG_ALWAYS_DEDICATED_MEMORY 0
#define VMA_DEBUG_INITIALIZE_ALLOCATIONS 0
#define VMA_DEBUG_GLOBAL_MUTEX 0
#define VMA_DEBUG_DONT_EXCEED_MAX_MEMORY_ALLOCATION_COUNT 0
#include <vk_mem_alloc.h>
