#include "vk_graphics.hpp"

extern VkFormat formatConversionTable[(size_t)gen::GPUFormat::TOTAL_TEXTURE_FORMATS];

gen::vkPipeline gen::vkGraphicsDevice::getPipeline(const gen::PipelineDesc& desc, gen::vkRenderPass* vkRenderPass) {
	gen::vkPipelineDescAndRenderPass p;
	p.desc = desc;
	p.renderPass = vkRenderPass->renderPass;
	gen::Hash128 hash = gen::hash128((void*)&p, sizeof(gen::vkPipelineDescAndRenderPass));
	if (pipelineHashMap.find(hash()) != pipelineHashMap.end()) {
		return pipelineHashMap[hash()];
	}
	buildPipeline(desc, vkRenderPass, hash);
	return getPipeline(desc, vkRenderPass);
}

gen::vkRenderPass gen::vkGraphicsDevice::getRenderPass(const gen::RenderPassDesc& desc) {
	gen::Hash128 hash = gen::hash128((void*)&desc, sizeof(gen::RenderPassDesc));
	if (renderPassHashMap.find(hash()) != renderPassHashMap.end()) {
		return renderPassHashMap[hash()];
	}
	buildRenderPass(desc, hash);
	return getRenderPass(desc);
}

VkSampler gen::vkGraphicsDevice::getSampler(const gen::SamplerDesc& desc) {
	gen::Hash128 hash = gen::hash128((void*)&desc, sizeof(gen::SamplerDesc));
	if (samplersHashMap.find(hash()) != samplersHashMap.end()) {
		return samplersHashMap[hash()];
	}
	buildSampler(desc, hash);
	return getSampler(desc);
}

void gen::vkGraphicsDevice::addBufferToDelete(gen::vkBuffer* buffer) {
	gen::vkBuffer_DestroyInfo info;
	info.buffer = buffer->buffer;
	info.allocation = buffer->allocation;
	if (!buffer->lastFence) {
		destroyBuffer(info);
	}
	else {
		for (auto& c : commandBuffers) {
			if (c.fence == buffer->lastFence) {
				c.buffersToDestroy.push_back(info);
			}
		}
	}
}

void gen::vkGraphicsDevice::addTextureToDelete(gen::vkTexture* texture) {
	gen::vkTexture_DestroyInfo info;
	info.image = texture->image;
	info.view = texture->imageView;
	info.allocation = texture->allocation;
	if (!texture->lastFence) {
		destroyTexture(info);
	}
	else {
		for (auto& c : commandBuffers) {
			if (c.fence == texture->lastFence) {
				c.texturesToDestroy.push_back(info);
			}
		}
	}
}

void gen::vkGraphicsDevice::cleanUpResources(vkCommandBufferData* commandBuffer) {
	for (auto& b : commandBuffer->buffersToDestroy) {
		destroyBuffer(b);
	}
	for (auto& t : commandBuffer->texturesToDestroy) {
		destroyTexture(t);
	}
	commandBuffer->buffersToDestroy.clear();
	commandBuffer->texturesToDestroy.clear();
}

void gen::vkGraphicsDevice::destroyBuffer(const gen::vkBuffer_DestroyInfo& info) {
	if(info.buffer) vmaDestroyBuffer(allocator, info.buffer, info.allocation);
}

void gen::vkGraphicsDevice::destroyTexture(const gen::vkTexture_DestroyInfo& info) {
	if(info.view) vkDestroyImageView(device, info.view, nullptr);
	if(info.image && info.allocation) vmaDestroyImage(allocator, info.image, info.allocation);
}

gen::vkCommandBufferData::vkCommandBufferData(gen::vkGraphicsDevice* device) {
}

gen::CommandBufferPtr gen::vkGraphicsDevice::getCommandBuffer() {
	for (auto& c : commandBuffers) {
		if (c.active) {
			if (vkGetFenceStatus(device, c.fence) == VK_SUCCESS) {
				c.active = false;
				vkResetFences(device, 1, &c.fence);
				c.signalSemaphore = std::make_shared<gen::vkSemaphore>(this);
				c.waitSempahores.clear();
				c.commands.clear();
				for (auto& d : c.dynamicBuffers) {
					gen::vkBuffer* vkBuffer = (gen::vkBuffer*)d.get();
					vmaDestroyBuffer(allocator, vkBuffer->buffer, vkBuffer->allocation);
				}
				c.dynamicBuffers.clear();
				c.swapchains.clear();
				for (auto& s : c.shaders) {
					s->resetDesciptorSets(c.commandBuffer);
				}
				c.shaders.clear();
			}
		}
	}

	for (auto& c : commandBuffers) {
		if (!c.active) {
			c.active = true;
			return gen::CommandBufferPtr(new vkCommandBuffer(&c, this));
		}
	}

	gen::vkCommandBufferData commandBufferData(this);

	VkCommandBufferAllocateInfo allocInfo = { VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO, NULL };
	allocInfo.commandPool = commandPool;
	allocInfo.commandBufferCount = 1;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	
	VK_ERROR_TEST(vkAllocateCommandBuffers(device, &allocInfo, &commandBufferData.commandBuffer), gen::GraphicsRuntimeException, "Can't Allocate Vulkan Command Buffer")

	VkFenceCreateInfo fenceCreateInfo = { VK_STRUCTURE_TYPE_FENCE_CREATE_INFO, NULL };
	fenceCreateInfo.flags = 0;

	VK_ERROR_TEST(vkCreateFence(device, &fenceCreateInfo, nullptr, &commandBufferData.fence), gen::GraphicsRuntimeException, "Vulkan Fence Creation Failed")

	commandBufferData.active = true;
	commandBufferData.signalSemaphore = std::make_shared<gen::vkSemaphore>(this);

	poolCreateInfo.memoryTypeIndex = memoryType_CPU;
	VK_ERROR_TEST(vmaCreatePool(allocator, &poolCreateInfo, &commandBufferData.dynamicBufferPool_CPU), gen::GraphicsRuntimeException, "Vulkan Memory Pool Creation Failed")

	poolCreateInfo.memoryTypeIndex = memoryType_CPUToGPU;
	VK_ERROR_TEST(vmaCreatePool(allocator, &poolCreateInfo, &commandBufferData.dynamicBufferPool_CPUToGPU), gen::GraphicsRuntimeException, "Vulkan Memory Pool Creation Failed")

	commandBuffers.push_back(commandBufferData);
	return std::make_shared<gen::vkCommandBuffer>(&commandBuffers.back(), this);
}

VkSamplerAddressMode addressMode(gen::WrapMode mode) {
	if (mode == gen::WrapMode::CLAMP_TO_EDGE) return VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
	else if (mode == gen::WrapMode::MIRRORED_REPEAT) return VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT;
	else if (mode == gen::WrapMode::REPEAT) return VK_SAMPLER_ADDRESS_MODE_REPEAT;
	return VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
}

void gen::vkGraphicsDevice::buildSampler(const gen::SamplerDesc& desc, gen::Hash128 hash) {
	VkSamplerCreateInfo createInfo = { VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO, NULL };
	createInfo.flags = 0;
	createInfo.mipLodBias = 0.0f;
	createInfo.anisotropyEnable = desc.anisotropicFiltering;
	createInfo.maxAnisotropy = desc.anisotropicLevel;
	createInfo.compareEnable = VK_FALSE;
	createInfo.compareOp = VK_COMPARE_OP_NEVER;
	createInfo.minLod = 0.0f;
	createInfo.maxLod = VK_LOD_CLAMP_NONE;
	createInfo.borderColor = VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK;
	createInfo.unnormalizedCoordinates = VK_FALSE;

	if (desc.mipFilter == gen::TextureFilter::LINEAR) createInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
	else if (desc.mipFilter == gen::TextureFilter::NEAREST) createInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST;

	if (desc.magFiler == gen::TextureFilter::LINEAR) createInfo.magFilter = VK_FILTER_LINEAR;
	else if (desc.magFiler == gen::TextureFilter::NEAREST) createInfo.magFilter = VK_FILTER_NEAREST;

	if (desc.minFiler == gen::TextureFilter::LINEAR) createInfo.minFilter = VK_FILTER_LINEAR;
	else if (desc.minFiler == gen::TextureFilter::NEAREST) createInfo.minFilter = VK_FILTER_NEAREST;

	createInfo.addressModeU = addressMode(desc.wrapX);
	createInfo.addressModeV = addressMode(desc.wrapY);
	createInfo.addressModeW = addressMode(desc.wrapZ);
	
	VkSampler sampler;
	VK_ERROR_TEST(vkCreateSampler(device, &createInfo, nullptr, &sampler), gen::GraphicsRuntimeException, "Vulkan Sampler Creation Failed")

	samplersHashMap[hash()] = sampler;
}

VkCompareOp compareOp(gen::CompareOp op) {
	if (op == gen::CompareOp::NEVER) return VK_COMPARE_OP_NEVER;
	else if (op == gen::CompareOp::LESS) return VK_COMPARE_OP_LESS;
	else if (op == gen::CompareOp::EQUAL) return VK_COMPARE_OP_EQUAL;
	else if (op == gen::CompareOp::LESS_OR_EQUAL) return VK_COMPARE_OP_LESS_OR_EQUAL;
	else if (op == gen::CompareOp::GREATER) return VK_COMPARE_OP_GREATER;
	else if (op == gen::CompareOp::NOT_EQUAL) return VK_COMPARE_OP_NOT_EQUAL;
	else if (op == gen::CompareOp::GREATER_OR_EQUAL) return VK_COMPARE_OP_GREATER_OR_EQUAL;
	else if (op == gen::CompareOp::ALWAYS) return VK_COMPARE_OP_ALWAYS;
	return VK_COMPARE_OP_LESS;
}

VkBlendFactor blendFactor(gen::BlendFactor factor) {
	if (factor == gen::BlendFactor::CONSTANT_ALPHA) return VK_BLEND_FACTOR_CONSTANT_ALPHA;
	else if (factor == gen::BlendFactor::CONSTANT_COLOR) return VK_BLEND_FACTOR_CONSTANT_COLOR;
	else if (factor == gen::BlendFactor::DST_ALPHA) return VK_BLEND_FACTOR_DST_ALPHA;
	else if (factor == gen::BlendFactor::DST_COLOR) return VK_BLEND_FACTOR_DST_COLOR;
	else if (factor == gen::BlendFactor::ONE) return VK_BLEND_FACTOR_ONE;
	else if (factor == gen::BlendFactor::ONE_MINUS_CONSTANT_ALPHA) return VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_ALPHA;
	else if (factor == gen::BlendFactor::ONE_MINUS_CONSTANT_COLOR) return VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_COLOR;
	else if (factor == gen::BlendFactor::ONE_MINUS_DST_ALPHA) return VK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA;
	else if (factor == gen::BlendFactor::ONE_MINUS_DST_COLOR) return VK_BLEND_FACTOR_ONE_MINUS_DST_COLOR;
	else if (factor == gen::BlendFactor::ONE_MINUS_SRC1_ALPHA) return VK_BLEND_FACTOR_ONE_MINUS_SRC1_ALPHA;
	else if (factor == gen::BlendFactor::ONE_MINUS_SRC1_COLOR) return VK_BLEND_FACTOR_ONE_MINUS_SRC1_COLOR;
	else if (factor == gen::BlendFactor::ONE_MINUS_SRC_ALPHA) return VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
	else if (factor == gen::BlendFactor::ONE_MINUS_SRC_COLOR) return VK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR;
	else if (factor == gen::BlendFactor::SRC1_ALPHA) return VK_BLEND_FACTOR_SRC1_ALPHA;
	else if (factor == gen::BlendFactor::SRC1_COLOR) return VK_BLEND_FACTOR_SRC1_COLOR;
	else if (factor == gen::BlendFactor::SRC_ALPHA) return VK_BLEND_FACTOR_SRC_ALPHA;
	else if (factor == gen::BlendFactor::SRC_ALPHA_SATURATE) return VK_BLEND_FACTOR_SRC_ALPHA_SATURATE;
	else if (factor == gen::BlendFactor::SRC_COLOR) return VK_BLEND_FACTOR_SRC_COLOR;
	else if (factor == gen::BlendFactor::ZERO) return VK_BLEND_FACTOR_ZERO;
	return VK_BLEND_FACTOR_ONE;
}

void gen::vkGraphicsDevice::buildPipeline(const gen::PipelineDesc& desc, gen::vkRenderPass* vkRenderPasss, gen::Hash128 hash) {
	gen::vkShader* vkShader = (gen::vkShader*)desc.shader.get();

	gen::vkPipeline pipeline;
	pipeline.shader = vkShader;

	VkVertexInputBindingDescription bindings[gen::MAX_VERTEX_BINDINGS];
	VkVertexInputAttributeDescription attributes[gen::MAX_VERTEX_ATTRIBUTES];
	for (uint32_t i = 0; i < desc.vertexInput.bindingsAmount; i++) {
		bindings[i].binding = i;
		bindings[i].stride = desc.vertexInput.bindings[i].stride;
		if (desc.vertexInput.bindings[i].perInstance) bindings[i].inputRate = VK_VERTEX_INPUT_RATE_INSTANCE;
		else bindings[i].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
	}
	for (uint32_t i = 0; i < desc.vertexInput.attributesAmount; i++) {
		attributes[i].binding = desc.vertexInput.attributes[i].binding;
		attributes[i].location = i;
		attributes[i].format = formatConversionTable[(size_t)desc.vertexInput.attributes[i].format];
		attributes[i].offset = desc.vertexInput.attributes[i].offset;
	}

	VkGraphicsPipelineCreateInfo createInfo = { VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO, NULL };
	createInfo.flags = 0;
	createInfo.stageCount = vkShader->pipelineStageCreateInfo.size();
	createInfo.pStages = vkShader->pipelineStageCreateInfo.data();

	VkPipelineVertexInputStateCreateInfo vertexInputInfo = { VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO, NULL };
	vertexInputInfo.pVertexAttributeDescriptions = attributes;
	vertexInputInfo.pVertexBindingDescriptions = bindings;
	vertexInputInfo.vertexAttributeDescriptionCount = desc.vertexInput.attributesAmount;
	vertexInputInfo.vertexBindingDescriptionCount = desc.vertexInput.bindingsAmount;
	createInfo.pVertexInputState = &vertexInputInfo;

	VkPipelineInputAssemblyStateCreateInfo inputAssemblyInfo = { VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO, NULL };
	inputAssemblyInfo.flags = 0;
	if (desc.inputAssembly.primitiveTopology == gen::PrimitiveTopology::LINE_LIST) inputAssemblyInfo.topology = VK_PRIMITIVE_TOPOLOGY_LINE_LIST;
	else if (desc.inputAssembly.primitiveTopology == gen::PrimitiveTopology::LINE_STRIP) inputAssemblyInfo.topology = VK_PRIMITIVE_TOPOLOGY_LINE_STRIP;
	else if (desc.inputAssembly.primitiveTopology == gen::PrimitiveTopology::POINT_LIST) inputAssemblyInfo.topology = VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
	else if (desc.inputAssembly.primitiveTopology == gen::PrimitiveTopology::TRIANGLE_LIST) inputAssemblyInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	else if (desc.inputAssembly.primitiveTopology == gen::PrimitiveTopology::TRIANGLE_STRIP) inputAssemblyInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP;
	inputAssemblyInfo.primitiveRestartEnable = desc.inputAssembly.primitiveRestart;
	createInfo.pInputAssemblyState = &inputAssemblyInfo;

	VkPipelineTessellationStateCreateInfo tesselationInfo = { VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO, NULL };
	if (desc.inputAssembly.tesselationControlPoints != 0) {
		tesselationInfo.flags = 0;
		tesselationInfo.patchControlPoints = desc.inputAssembly.tesselationControlPoints;
		createInfo.pTessellationState = &tesselationInfo;
	}
	else {
		createInfo.pTessellationState = nullptr;
	}

	VkViewport viewport;
	viewport.x = desc.viewport.x;
	viewport.y = desc.viewport.y;
	viewport.width = desc.viewport.width;
	viewport.height = desc.viewport.height;
	viewport.minDepth = desc.viewport.minDepth;
	viewport.maxDepth = desc.viewport.maxDepth;
	VkRect2D scissor;
	scissor.offset = { (int)desc.scissor.x, (int)desc.scissor.y };
	scissor.extent = { desc.scissor.width, desc.scissor.height };
	VkPipelineViewportStateCreateInfo viewportInfo = { VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO, NULL };
	viewportInfo.flags = 0;
	viewportInfo.viewportCount = 1;
	viewportInfo.pViewports = &viewport;
	viewportInfo.scissorCount = 1;
	viewportInfo.pScissors = &scissor;
	createInfo.pViewportState = &viewportInfo;

	VkPipelineRasterizationStateCreateInfo rasterizerInfo = { VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO, NULL };
	rasterizerInfo.flags = 0;
	rasterizerInfo.depthClampEnable = desc.rasterizer.depthClamp;
	rasterizerInfo.rasterizerDiscardEnable = false;
	if (desc.rasterizer.frontFaceCCW) rasterizerInfo.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
	else rasterizerInfo.frontFace = VK_FRONT_FACE_CLOCKWISE;
	if (desc.rasterizer.cullMode == gen::CullMode::BACK) rasterizerInfo.cullMode = VK_CULL_MODE_BACK_BIT;
	else if (desc.rasterizer.cullMode == gen::CullMode::FRONT) rasterizerInfo.cullMode = VK_CULL_MODE_FRONT_BIT;
	else if (desc.rasterizer.cullMode == gen::CullMode::FRONT_AND_BACK) rasterizerInfo.cullMode = VK_CULL_MODE_FRONT_AND_BACK;
	else if (desc.rasterizer.cullMode == gen::CullMode::NONE) rasterizerInfo.cullMode = 0;
	if (desc.rasterizer.polygonMode == gen::PolygonMode::FILL) rasterizerInfo.polygonMode = VK_POLYGON_MODE_FILL;
	else if (desc.rasterizer.polygonMode == gen::PolygonMode::LINE) rasterizerInfo.polygonMode = VK_POLYGON_MODE_LINE;
	else if (desc.rasterizer.polygonMode == gen::PolygonMode::POINT) rasterizerInfo.polygonMode = VK_POLYGON_MODE_POINT;
	rasterizerInfo.depthBiasEnable = false;
	rasterizerInfo.depthBiasConstantFactor = 0.0f;
	rasterizerInfo.depthBiasClamp = 0.0f;
	rasterizerInfo.depthBiasSlopeFactor = 0.0f;
	rasterizerInfo.lineWidth = 1.0f;
	createInfo.pRasterizationState = &rasterizerInfo;

	VkPipelineMultisampleStateCreateInfo multisamplingInfo = { VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO, NULL };
	multisamplingInfo.sampleShadingEnable = VK_FALSE;
	multisamplingInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	multisamplingInfo.minSampleShading = 1.0f; // Optional
	multisamplingInfo.pSampleMask = nullptr; // Optional
	multisamplingInfo.alphaToCoverageEnable = VK_FALSE; // Optional
	multisamplingInfo.alphaToOneEnable = VK_FALSE; // Optional
	createInfo.pMultisampleState = &multisamplingInfo;

	VkPipelineDepthStencilStateCreateInfo depthStencilInfo = { VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO, NULL };;
	depthStencilInfo.depthTestEnable = desc.depthStencil.depthTest;
	depthStencilInfo.depthWriteEnable = desc.depthStencil.depthWrite;
	depthStencilInfo.depthCompareOp = compareOp(desc.depthStencil.compareOp);
	depthStencilInfo.depthBoundsTestEnable = false;
	depthStencilInfo.stencilTestEnable = false;
	depthStencilInfo.minDepthBounds = 0.0f;
	depthStencilInfo.maxDepthBounds = 0.0f;
	createInfo.pDepthStencilState = &depthStencilInfo;

	VkPipelineColorBlendAttachmentState blendAttachments[gen::MAX_COLOR_ATTACHMENTS];
	VkPipelineColorBlendStateCreateInfo blendInfo = { VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO, NULL };
	blendInfo.flags = 0;
	blendInfo.logicOpEnable = false;
	blendInfo.logicOp = VK_LOGIC_OP_CLEAR;
	blendInfo.attachmentCount = vkRenderPasss->colorAttachments;
	for (uint32_t i = 0; i < vkRenderPasss->colorAttachments; i++) {
		blendAttachments[i].blendEnable = desc.blend.attachments[i].enabled;
		blendAttachments[i].srcColorBlendFactor = blendFactor(desc.blend.attachments[i].srcColor);
		blendAttachments[i].dstColorBlendFactor = blendFactor(desc.blend.attachments[i].dstColor);
		blendAttachments[i].srcAlphaBlendFactor = blendFactor(desc.blend.attachments[i].srcAlpha);
		blendAttachments[i].dstAlphaBlendFactor = blendFactor(desc.blend.attachments[i].dstAlpha);
		blendAttachments[i].colorWriteMask = 0;
		if (desc.blend.attachments[i].writeR) blendAttachments[i].colorWriteMask |= VK_COLOR_COMPONENT_R_BIT;
		if (desc.blend.attachments[i].writeG) blendAttachments[i].colorWriteMask |= VK_COLOR_COMPONENT_G_BIT;
		if (desc.blend.attachments[i].writeB) blendAttachments[i].colorWriteMask |= VK_COLOR_COMPONENT_B_BIT;
		if (desc.blend.attachments[i].writeA) blendAttachments[i].colorWriteMask |= VK_COLOR_COMPONENT_A_BIT;
		blendAttachments[i].colorBlendOp = VK_BLEND_OP_ADD;
		blendAttachments[i].alphaBlendOp = VK_BLEND_OP_ADD;
	}
	blendInfo.attachmentCount = vkRenderPasss->colorAttachments;
	blendInfo.pAttachments = blendAttachments;
	blendInfo.blendConstants[0] = desc.blend.blendConstantR;
	blendInfo.blendConstants[1] = desc.blend.blendConstantG;
	blendInfo.blendConstants[2] = desc.blend.blendConstantB;
	blendInfo.blendConstants[3] = desc.blend.blendConstantA;
	createInfo.pColorBlendState = &blendInfo;

	std::vector<VkDynamicState> dynamicStates;
	if (desc.dynamicScissor) dynamicStates.push_back(VK_DYNAMIC_STATE_SCISSOR);
	if (desc.dynamicViewport) dynamicStates.push_back(VK_DYNAMIC_STATE_VIEWPORT);
	VkPipelineDynamicStateCreateInfo dynamicStateInfo = { VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO, NULL };
	dynamicStateInfo.flags = 0;
	if (dynamicStates.empty()) {
		createInfo.pDynamicState = nullptr;
	}
	else {
		dynamicStateInfo.dynamicStateCount = dynamicStates.size();
		dynamicStateInfo.pDynamicStates = dynamicStates.data();
		createInfo.pDynamicState = &dynamicStateInfo;
	}

	pipeline.layout = vkShader->layout;

	createInfo.renderPass = vkRenderPasss->renderPass;
	createInfo.subpass = 0;
	createInfo.basePipelineHandle = 0;
	createInfo.basePipelineIndex = 0;
	createInfo.layout = pipeline.layout;
	
	VK_ERROR_TEST(vkCreateGraphicsPipelines(device, pipelineCache, 1, &createInfo, nullptr, &pipeline.pipeline), gen::GraphicsRuntimeException, "Vulkan Graphics Pipeline Creation Failed")

	pipelineHashMap[hash()] = pipeline;
}

extern VkFormat formatConversionTable[(size_t)gen::GPUFormat::TOTAL_TEXTURE_FORMATS];

void gen::vkGraphicsDevice::buildRenderPass(const gen::RenderPassDesc& desc, gen::Hash128 hash) {
	gen::vkRenderPass renderPass;

	uint32_t x;
	uint32_t y;
	VkAttachmentDescription attachments[gen::MAX_COLOR_ATTACHMENTS + 1];
	VkAttachmentReference attachmentReferences[gen::MAX_COLOR_ATTACHMENTS + 1];
	VkImageView imageViews[gen::MAX_COLOR_ATTACHMENTS + 1];
	uint32_t attachmentsCount = 0;
	uint32_t colorAttachments = 0;
	bool depthStencilAttachment = false;
	for (uint32_t i = 0; i < gen::MAX_COLOR_ATTACHMENTS; i++) {
		if (desc.colorAttachments[i].texture.get()) {
			gen::vkTexture* vkTexture = (gen::vkTexture*)desc.colorAttachments[attachmentsCount].texture.get();

			renderPass.textures[colorAttachments] = vkTexture;
			colorAttachments++;
			
			x = vkTexture->textureDesc.width;
			y = vkTexture->textureDesc.height;
			VkAttachmentDescription* vkAttachment = &attachments[attachmentsCount];

			renderPass.renderArea.offset = { 0, 0 };
			renderPass.renderArea.extent = { x, y };

			vkAttachment->flags = 0;

			vkAttachment->format = formatConversionTable[(uint32_t)vkTexture->textureDesc.format];
			vkAttachment->samples = (VkSampleCountFlagBits)1;

			vkAttachment->stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			vkAttachment->stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;

			vkAttachment->initialLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
			vkAttachment->finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

			if (desc.colorAttachments[attachmentsCount].loadOp == gen::RenderPassLoadOp::CLEAR) vkAttachment->loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
			if (desc.colorAttachments[attachmentsCount].loadOp == gen::RenderPassLoadOp::DONT_CARE) vkAttachment->loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			if (desc.colorAttachments[attachmentsCount].loadOp == gen::RenderPassLoadOp::LOAD) vkAttachment->loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;

			if (desc.colorAttachments[attachmentsCount].storeOp == gen::RenderPassStoreOp::DONT_CARE) vkAttachment->storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
			if (desc.colorAttachments[attachmentsCount].storeOp == gen::RenderPassStoreOp::STORE) vkAttachment->storeOp = VK_ATTACHMENT_STORE_OP_STORE;

			VkAttachmentReference* vkAttachmentReference = &attachmentReferences[attachmentsCount];
			vkAttachmentReference->attachment = attachmentsCount;
			vkAttachmentReference->layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

			renderPass.clearValue[attachmentsCount].color.float32[0] = desc.colorAttachments[i].clearR;
			renderPass.clearValue[attachmentsCount].color.float32[1] = desc.colorAttachments[i].clearG;
			renderPass.clearValue[attachmentsCount].color.float32[2] = desc.colorAttachments[i].clearB;
			renderPass.clearValue[attachmentsCount].color.float32[3] = desc.colorAttachments[i].clearA;

			imageViews[attachmentsCount] = vkTexture->imageView;

			attachmentsCount++;

			if (vkTexture->swapchain) {
				renderPass.swapchain = vkTexture->swapchain;
			}
		}
	}
	renderPass.colorAttachments = colorAttachments;
	renderPass.depthStencilAttachment = (bool)(desc.depthStencilAttachment.texture);
	if (desc.depthStencilAttachment.texture) {
		depthStencilAttachment = true;

		gen::vkTexture* vkTexture = (gen::vkTexture*)desc.depthStencilAttachment.texture.get();
		VkAttachmentDescription* vkAttachment = &attachments[attachmentsCount];
		x = vkTexture->textureDesc.width;
		y = vkTexture->textureDesc.height;

		vkAttachment->flags = 0;

		vkAttachment->format = formatConversionTable[(uint32_t)vkTexture->textureDesc.format];
		vkAttachment->samples = (VkSampleCountFlagBits)1;

		vkAttachment->stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		vkAttachment->stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;

		vkAttachment->initialLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
		vkAttachment->finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

		if (desc.depthStencilAttachment.loadOp == gen::RenderPassLoadOp::CLEAR) vkAttachment->loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		if (desc.depthStencilAttachment.loadOp == gen::RenderPassLoadOp::DONT_CARE) vkAttachment->loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		if (desc.depthStencilAttachment.loadOp == gen::RenderPassLoadOp::LOAD) vkAttachment->loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;

		if (desc.depthStencilAttachment.storeOp == gen::RenderPassStoreOp::DONT_CARE) vkAttachment->storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		if (desc.depthStencilAttachment.storeOp == gen::RenderPassStoreOp::STORE) vkAttachment->storeOp = VK_ATTACHMENT_STORE_OP_STORE;

		VkAttachmentReference* vkAttachmentReference = &attachmentReferences[attachmentsCount];
		vkAttachmentReference->attachment = attachmentsCount;
		vkAttachmentReference->layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;;

		renderPass.clearValue[attachmentsCount].depthStencil.depth = desc.depthStencilAttachment.clearD;

		imageViews[attachmentsCount] = vkTexture->imageView;

		attachmentsCount++;

		if (vkTexture->swapchain) renderPass.swapchain = vkTexture->swapchain;
	}

	VkSubpassDescription subpass;
	subpass.flags = 0;
	subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subpass.inputAttachmentCount = 0;
	subpass.pInputAttachments = nullptr;
	subpass.colorAttachmentCount = colorAttachments;
	subpass.pColorAttachments = &attachmentReferences[0];
	subpass.pResolveAttachments = nullptr;
	if (depthStencilAttachment) subpass.pDepthStencilAttachment = &attachmentReferences[colorAttachments];
	else subpass.pDepthStencilAttachment = nullptr;
	subpass.preserveAttachmentCount = 0;
	subpass.pPreserveAttachments = nullptr;

	VkRenderPassCreateInfo createInfo = { VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO ,NULL };
	createInfo.flags = 0;
	createInfo.attachmentCount = attachmentsCount;
	createInfo.pAttachments = &attachments[0];
	createInfo.subpassCount = 1;
	createInfo.pSubpasses = &subpass;
	createInfo.dependencyCount = 0;
	createInfo.pDependencies = nullptr;
	VK_ERROR_TEST(vkCreateRenderPass(device, &createInfo, nullptr, &renderPass.renderPass), gen::GraphicsRuntimeException, "Vulkan RenderPass Creation Failed")

	VkFramebufferCreateInfo fbCreateInfo = { VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO, NULL };
	fbCreateInfo.flags = 0;
	fbCreateInfo.renderPass = renderPass.renderPass;
	fbCreateInfo.attachmentCount = attachmentsCount;
	fbCreateInfo.pAttachments = &imageViews[0];
	fbCreateInfo.width = x;
	fbCreateInfo.height = y;
	fbCreateInfo.layers = 1;
	VK_ERROR_TEST(vkCreateFramebuffer(device, &fbCreateInfo, nullptr, &renderPass.framebuffer), gen::GraphicsRuntimeException, "Vulkan Framebuffer Creation Failed")

	renderPassHashMap[hash()] = renderPass;
}
