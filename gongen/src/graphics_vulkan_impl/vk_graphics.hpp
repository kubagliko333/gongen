#pragma once

#include "vulkan.hpp"
#include <gongen/graphics_api/graphics.hpp>
#include <vk_mem_alloc.h>
#include <gongen/core/hash.hpp>
#include <list>
#include <gongen/core/exception.hpp>
#include <string>

#define VK_ERROR_TEST(func, exc, msg) {\
VkResult result = func;\
if (result != VK_SUCCESS) {\
	throw exc(std::string(msg) + ": " + gen::vkResultMessage(result));\
}\
}

namespace gen {
	std::string vkResultMessage(VkResult result);

	struct vkDeviceInfo {
		VkPhysicalDevice device;
		VkPhysicalDeviceFeatures features;
		uint32_t queueIndex;
	};

	struct vkTexture;
	class vkGraphicsDevice;

	class vkSwapchain :public Swapchain {
		VkSurfaceKHR surface;

		std::vector<gen::vkTexture> _images;

		std::vector<gen::TexturePtr> images;
		std::vector<gen::TexturePtr> depthImages;

		uint32_t activeW;
		uint32_t activeH;
	public:
		void createSwapchain();

		uint32_t activeImage;
		VkSwapchainKHR swapchain;

		vkGraphicsDevice* device;
		gen::Window* window;
		VkSemaphore swapchainSemaphore;
		VkSemaphore waitSemaphore;
		bool imageAquired;
		bool minimized = false;

		gen::RenderPassDesc renderPass;

		vkSwapchain(gen::Window* window, gen::vkGraphicsDevice* device);
		void aquireImage(VkSemaphore semaphore);
		void nextFrame();
		void handleResize(uint32_t x, uint32_t y);
		bool isMinimized() { return minimized; }

		gen::RenderPassDesc& getRenderPass(gen::CommandBuffer* commandBuffer);
	};

	class vkGraphicsAPI :public GraphicsAPI {
	public:
		std::vector<vkDeviceInfo> vkDevices;

		VkInstance instance;
		VkDebugUtilsMessengerCreateInfoEXT debugCreateInfo;
		VkDebugUtilsMessengerEXT debugMessanger;
		GraphicsAPICreateInfo createInfo;

		ShaderType getShaderType() { return gen::ShaderType::SPIRV; }

		vkGraphicsAPI() {}
		~vkGraphicsAPI() {}
		void init(gen::WindowAPI* windowAPI, GraphicsAPICreateInfo createInfo);
		GraphicsDevice* createDevice(GraphicsDeviceInfo* deviceInfo, Window* window, Swapchain** swaphain);
	};

	struct vkResource {
		vkGraphicsDevice* device;
		VkFence lastFence;
	};

	struct vkSynchronizedResource :public vkResource {
		VkPipelineStageFlags srcStageMask = 0;
		VkAccessFlags srcAccessMask = 0;
		bool syncSet = false;
	};

	struct vkBuffer :public vkSynchronizedResource {
		uint32_t usageFlags = 0;
		VkBuffer buffer = 0 ;
		VmaAllocation allocation = 0;
		VmaAllocationInfo allocationInfo;
		bool dynamicBuffer = false;

		~vkBuffer();
	};

	struct vkTexture :public vkSynchronizedResource {
		uint32_t usageFlags;
		uint32_t samples;
		VkImage image;
		VkImageView imageView;
		VkImageLayout currentLayout;
		VmaAllocation allocation;
		VmaAllocationInfo allocationInfo;
		gen::TextureDesc textureDesc;
		bool swapchainImage;
		gen::vkSwapchain* swapchain;

		~vkTexture();
	};

	enum class vkCommandType {
		BEGIN_RENDER_PASS,
		END_RENDER_PASS,

		COPY_BUFFER,
		//COPY_TEXTURE,
		//COPY_TEXTURE_TO_BUFFER,
		COPY_BUFFER_TO_TEXTURE,

		SET_VERTEX_BUFFER,
		SET_INDEX_BUFFER,
		SET_PIPELINE,
		SET_DESCRIPTOR_SETS,
		//SET_BINDING_STATE,
		SET_SCISSOR,
		SET_VIEWPORT,

		DRAW,
		DRAW_INDEXED,
		DRAW_INDIRECT,
		DRAW_INDEXED_INDIRECT
	};

	struct vkResourceAccess {
		bool write = false;
		bool image = false;

		VkImageLayout imageLayout = VK_IMAGE_LAYOUT_UNDEFINED;

		VkPipelineStageFlags srcStage = 0;
		VkAccessFlags srcAccess = 0;

		VkPipelineStageFlags dstStage = 0;
		VkAccessFlags dstAccess = 0;

		gen::vkBuffer *buffer;
		gen::vkTexture *texture;
	};

	const static uint32_t DESCRIPTOR_POOL_SIZE = 16;

	const static uint32_t DESCIPTOR_POOL_SIZE = 16;

	struct vkDescriptorPool {
		VkCommandBuffer commandBuffer;
		VkDescriptorPool pool;
		uint32_t used;
		uint32_t size;
	};

	struct vkDescriptor {
		gen::SPIRVDescriptorType type;

		gen::vkBuffer* buffer;
		gen::vkTexture* texture;
		VkSampler sampler;

		uint64_t offset;
		uint64_t size;
	};

	class vkShader {
	public:
		vkGraphicsDevice* device;

		gen::ShaderMetadata metadata[(size_t)gen::ShaderStage::TOTAL_SHADER_STAGES];
		VkShaderModule modules[(size_t)gen::ShaderStage::TOTAL_SHADER_STAGES];
		std::vector<VkPipelineShaderStageCreateInfo> pipelineStageCreateInfo;
		VkPipelineLayout layout;
		VkDescriptorSetLayout setLayout[gen::MAX_DESCRIPTOR_SETS];
		uint32_t amountOfSetLayouts;

		bool descriptorExist[gen::MAX_DESCRIPTOR_SETS][gen::MAX_DESCRIPTOR_SLOTS];

		std::vector<VkDescriptorPoolSize> poolSizes;

		std::vector<gen::vkDescriptorPool> pools;

		vkShader() {}

		void resetDesciptorSets(VkCommandBuffer commandBuffer);
		VkDescriptorSet getDescriptorSet(gen::vkDescriptor* descriptors, uint32_t set, VkCommandBuffer commandBuffer);
	};

	struct vkPipeline {
		VkPipeline pipeline;
		VkPipelineLayout layout;
		gen::vkShader* shader;
	};

	struct vkRenderPass {
		uint32_t colorAttachments;
		bool depthStencilAttachment;

		VkRenderPass renderPass;
		VkFramebuffer framebuffer;
		VkRect2D renderArea;
		VkClearValue clearValue[gen::MAX_COLOR_ATTACHMENTS + 1];
		gen::vkSwapchain* swapchain;
		gen::vkTexture* textures[gen::MAX_COLOR_ATTACHMENTS];
	};

	struct vkCommand {
		vkCommand() {}

		std::vector<vkResourceAccess> resourceAccesses;

		vkResourceAccess* nextResourceAccess() {
			resourceAccesses.push_back(vkResourceAccess());
			return &resourceAccesses.back();
		}

		vkCommandType type;

		struct vkCommandBindingState {
			bool dirtyFlag = false;
			uint32_t firstDescriptorSet = 0;
			uint32_t descriptorSetAmount = 0;
			VkDescriptorSet descriptorSet[gen::MAX_DESCRIPTOR_SETS];
		};

		struct vkCommandDraw :public vkCommandBindingState {
			gen::vkBuffer* indirectBuffer;
			uint32_t offset;

			uint32_t vertexCount;
			int32_t vertexOffset;
			uint32_t firstVertex;
			uint32_t instanceCount;
			uint32_t firstInstance;
			uint32_t indexCount;
			uint32_t firstIndex;
			uint32_t drawAmount;
		};

		struct vkCommandCopyBuffer {
			gen::vkBuffer* dstBuffer;
			gen::vkBuffer* srcBuffer;

			uint64_t srcOffset;
			uint64_t dstOffset;
			uint64_t size;
		};

		struct vkCommandCopyBufferToTexture {
			gen::vkBuffer* srcBuffer;
			gen::vkTexture* dstTexture;

			uint64_t srcOffset;
			gen::TextureOffset dstOffset;
			uint64_t sizeX;
			uint64_t sizeY;
			uint64_t sizeZ;
		};

		struct vkCommandSetVertexBuffer {
			gen::vkBuffer* buffer;
			uint64_t offset;
			uint32_t slot;
		};

		struct vkCommandSetIndexBuffer {
			gen::vkBuffer* buffer;
			uint64_t offset;
			gen::IndexType indexType;
		};

		struct vkCommandSetDescritorSets {
			VkDescriptorSet sets[gen::MAX_DESCRIPTOR_SETS];
			uint64_t firstSet;
			uint64_t setsAmount;
		};

		gen::vkRenderPass renderPass;
		gen::vkPipeline pipeline;
		union {
			vkCommandDraw draw;

			vkCommandCopyBuffer copyBuffer;
			vkCommandCopyBufferToTexture copyBufferToTexture;

			vkCommandSetDescritorSets setDescriptorSets;
			vkCommandSetVertexBuffer setVertexBuffer;
			vkCommandSetIndexBuffer setIndexBuffer;

			VkRect2D scissor;
			VkViewport viewport;
		};
	};

	class vkGraphicsDevice;

	struct vkSemaphore {
		vkGraphicsDevice* device = 0;
		VkSemaphore semaphore = 0;

		vkSemaphore(vkGraphicsDevice* device);
		vkSemaphore() {}
		~vkSemaphore();
	};

	typedef std::shared_ptr<vkSemaphore> vkSemaphorePtr;

	struct vkBuffer_DestroyInfo {
		VkBuffer buffer;
		VmaAllocation allocation;
	};

	struct vkTexture_DestroyInfo {
		VkImage image;
		VkImageView view;
		VmaAllocation allocation;
	};

	struct vkCommandBufferData {
		vkCommandBufferData() {}

		std::vector<gen::vkSwapchain*> swapchains;

		VkCommandBuffer commandBuffer = 0;
		VkFence fence = 0;
		bool active = false;
		std::vector<vkSemaphorePtr> waitSempahores;
		vkSemaphorePtr signalSemaphore;

		VmaPool dynamicBufferPool_CPU = 0;
		VmaPool dynamicBufferPool_CPUToGPU = 0;

		std::vector<BufferPtr> dynamicBuffers;

		std::vector<vkCommand> commands;

		vkCommandBufferData(gen::vkGraphicsDevice* device);

		gen::vkRenderPass activeRenderPass;
		gen::vkPipeline activePipeline;

		std::vector<vkBuffer_DestroyInfo> buffersToDestroy;
		std::vector<vkTexture_DestroyInfo> texturesToDestroy;

		std::vector<gen::vkShader*> shaders;
	};

	class vkGraphicsDevice;

	class vkCommandBuffer :public CommandBuffer {
		vkCommandBufferData* data;
		vkGraphicsDevice* device;

		vkDescriptor descriptor[gen::MAX_DESCRIPTOR_SETS][gen::MAX_DESCRIPTOR_SLOTS];
		bool descriptorSetDirtyFlag[gen::MAX_DESCRIPTOR_SETS];

		void setSync(gen::vkSynchronizedResource *resource, VkPipelineStageFlags srcStage, VkAccessFlags srcAccess);
		void waitSyncBuffer(gen::vkBuffer* buffer, VkPipelineStageFlags dstStage, VkAccessFlags dstAccess);
		void waitSyncImage(gen::vkTexture* texture, VkPipelineStageFlags dstStage, VkAccessFlags dstAccess, VkImageLayout layout);

		void processResourceAccess(gen::vkResourceAccess* access);

		void processDescritporSetsChange();

		vkCommand* nextCommand(gen::vkCommandType type);
		void buildCommandBuffer();
	public:
		vkCommandBuffer(vkCommandBufferData* data, vkGraphicsDevice* device);
		~vkCommandBuffer();

		void addSwapchain(gen::vkSwapchain* swapchain) { data->swapchains.push_back(swapchain); }

		BufferPtr createDynamicBuffer(uint32_t bindFlags, uint64_t size, bool gpuAccessOptimal = true);
		void updateBuffer(const BufferPtr &buffer, uint64_t offset, uint64_t size, void* data);
		void updateTexture(const TexturePtr &texture, TextureOffset offset, uint32_t sizeX, uint32_t sizeY, uint32_t sizeZ, void* data);

		void beginRenderPass(const gen::RenderPassDesc& desc);
		void endRenderPass();

		void bindPipeline(const gen::PipelineDesc& desc);
		void bindVertexBuffers(const gen::BufferPtr& vertexBuffer, uint32_t slot, uint64_t offset);
		void bindIndexBuffer(const gen::BufferPtr& indexBuffer, uint64_t offset, gen::IndexType type);
		void setScissor(const gen::ScissorDesc& desc);
		void setViewport(const gen::ViewportDesc& desc);

		void setUniformBuffer(uint32_t set, uint32_t slot, const gen::BufferPtr& buffer, uint64_t offset, uint64_t size);
		void setTexture(uint32_t set, uint32_t slot, const gen::TexturePtr& texture);
		void setSampler(uint32_t set, uint32_t slot, const gen::SamplerDesc& desc);

		void draw(uint32_t vertexCount, uint32_t firstVertex = 0, uint32_t instanceCount = 1, uint32_t firstInstance = 0);
		void drawIndexed(uint32_t indexCount, uint32_t firstIndex = 0, int32_t vertexOffset = 0, uint32_t instanceCount = 1, uint32_t firstInstance = 0);
		void drawIndirect(const gen::BufferPtr& buffer, uint32_t drwaCount, uint32_t offset = 0);
		void drawIndexedIndirect(const gen::BufferPtr& buffer, uint32_t drwaCount, uint32_t offset = 0);

		void execute(CommandBuffer* waitFor);
		bool executionDone();
		void waitForExecution();

		void display(Swapchain* swapchian);

		gen::vkSemaphorePtr addWaitSemaphore();
		gen::vkSemaphorePtr getSignalSemaphore();
	};

	struct vkPipelineDescAndRenderPass {
		gen::PipelineDesc desc;
		VkRenderPass renderPass;
	};

	class vkGraphicsDevice :public GraphicsDevice {
		VmaPoolCreateInfo poolCreateInfo;

		VkPipelineCache pipelineCache;

		std::unordered_map<size_t, gen::vkPipeline> pipelineHashMap;
		std::unordered_map<size_t, gen::vkRenderPass> renderPassHashMap;
		std::unordered_map<size_t, VkSampler> samplersHashMap;

		std::list<gen::vkCommandBufferData> commandBuffers;

		void buildPipeline(const gen::PipelineDesc& desc, gen::vkRenderPass* vkRenderPass, gen::Hash128 hash);
		void buildRenderPass(const gen::RenderPassDesc& desc, gen::Hash128 hash);
		void buildSampler(const gen::SamplerDesc& desc, gen::Hash128 hash);
	public:
		void addBufferToDelete(gen::vkBuffer* buffer);
		void addTextureToDelete(gen::vkTexture* texture);
		void cleanUpResources(vkCommandBufferData* commandBuffer);

		void destroyBuffer(const gen::vkBuffer_DestroyInfo& info);
		void destroyTexture(const gen::vkTexture_DestroyInfo& info);

		BufferPtr createBuffer_internal(const gen::BufferDesc& desc, bool dynamicBuffer, VmaPool pool);

		VkInstance instance;
		VkDevice device;
		VmaAllocator allocator;
		uint32_t queueFamily;
		VkQueue queue;
		VkPhysicalDevice physicalDevice;
		VkCommandPool commandPool;
		gen::GraphicsAPICreateInfo createInfo;

		uint32_t memoryType_CPU;
		uint32_t memoryType_CPUToGPU;

		vkGraphicsDevice(VkDevice device, VmaAllocator allocator, uint32_t queueFamily, VkQueue queue, VkInstance instance, VkPhysicalDevice physicalDevice, GraphicsAPICreateInfo createInfo);

		gen::vkPipeline getPipeline(const gen::PipelineDesc& desc, gen::vkRenderPass *vkRenderPass);
		gen::vkRenderPass getRenderPass(const gen::RenderPassDesc& desc);
		VkSampler getSampler(const gen::SamplerDesc& desc);

		BufferPtr createBuffer(const gen::BufferDesc& desc);
		TexturePtr createTexture(const gen::TextureDesc& desc);
		ShaderPtr createShader(const gen::ShaderDesc& desc);

		CommandBufferPtr getCommandBuffer();
	};
}
