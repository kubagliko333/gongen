#include <gongen/core/random.hpp>
#include <limits>

#undef RAND_MAX
#define RAND_MAX std::numeric_limits<uint32_t>::max()

#define UINT_TO_INT(bits, u) (int##bits##_t)(u - std::numeric_limits<int##bits##_t>::min())
#define INT_TO_UINT(bits, i) (uint##bits##_t)(i + std::numeric_limits<int##bits##_t>::min())

gen::Random::Random(uint64_t seed) : seed(seed) {
	this->value = seed;
}

uint64_t gen::Random::next() {
	this->value = (2438952949 * this->value + 1) % std::numeric_limits<uint32_t>::max();
	return this->value;
}

uint8_t gen::Random::nextUint8() {
	return (uint8_t)(next() % std::numeric_limits<uint8_t>::max());
}

uint8_t gen::Random::nextUint8(uint8_t max) {
	return nextUint8() % max;
}

uint8_t gen::Random::nextUint8(uint8_t min, uint8_t max) {
	return nextUint8(max-min) + min;
}

uint16_t gen::Random::nextUint16() {
	return (uint16_t)(next() % std::numeric_limits<uint16_t>::max());
}

uint16_t gen::Random::nextUint16(uint16_t max) {
	return nextUint16() % max;
}

uint16_t gen::Random::nextUint16(uint16_t min, uint16_t max) {
	return nextUint16(max-min) + min;
}

uint32_t gen::Random::nextUint32() {
	return (uint32_t)(next() % std::numeric_limits<uint32_t>::max());
}

uint32_t gen::Random::nextUint32(uint32_t max) {
	return nextUint32() % max;
}

uint32_t gen::Random::nextUint32(uint32_t min, uint32_t max) {
	return nextUint32(max-min) + min;
}

uint64_t gen::Random::nextUint64() {
	return next();
}

uint64_t gen::Random::nextUint64(uint64_t max) {
	return nextUint64() % max;
}

uint64_t gen::Random::nextUint64(uint64_t min, uint64_t max) {
	return nextUint64(max-min) + min;
}

int8_t gen::Random::nextInt8() {
	return UINT_TO_INT(8, nextUint8());
}

int8_t gen::Random::nextInt8(int8_t min, int8_t max) {
	return UINT_TO_INT(8, nextUint8(INT_TO_UINT(8, min), INT_TO_UINT(8, max)));
}

int16_t gen::Random::nextInt16() {
	return UINT_TO_INT(16, nextUint16());
}

int16_t gen::Random::nextInt16(int16_t min, int16_t max) {
	return UINT_TO_INT(16, nextUint16(INT_TO_UINT(16, min), INT_TO_UINT(16, max)));
}

int32_t gen::Random::nextInt32() {
	return UINT_TO_INT(32, nextUint32());
}

int32_t gen::Random::nextInt32(int32_t min, int32_t max) {
	return UINT_TO_INT(32, nextUint32(INT_TO_UINT(32, min), INT_TO_UINT(32, max)));
}

int64_t gen::Random::nextInt64() {
	return UINT_TO_INT(64, nextUint64());
}

int64_t gen::Random::nextInt64(int64_t min, int64_t max) {
	return UINT_TO_INT(64, nextUint64(INT_TO_UINT(64, min), INT_TO_UINT(64, max)));
}

double gen::Random::nextDouble() {
	return static_cast<double>(next() / static_cast<double>(RAND_MAX));
}

double gen::Random::nextDouble(double min, double max) {
	return static_cast<double>(next() / static_cast<double>(RAND_MAX/(max-min))) + min;
}

float gen::Random::nextFloat() {
	return nextDouble();
}

float gen::Random::nextFloat(float min, float max) {
	return nextDouble(min, max);
}

bool gen::Random::nextBoolean() {
	return next() % 2;
}