#include <gongen/core/platform.hpp>
#ifdef PLATFORM_LINUX
#include <dlfcn.h>
#include <string>

#include <sys/stat.h>
#include <gongen/core/file.hpp>

#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <linux/limits.h>

std::string gen::getExecutableDir() {
	char result[PATH_MAX] = {};
	ssize_t count = readlink("/proc/self/exe", result, PATH_MAX);
	std::string str = result;
	size_t offset = str.find_last_of('/');
	std::string substr = str.substr(0, offset);
	return substr;
}

std::string gen::getPrefDir(const std::string& org, const std::string& app) {
	char* homeDir = getenv("HOME");
	std::string homeDir_str;
	if (homeDir) homeDir_str = homeDir;
	else {
		struct passwd* pw = getpwuid(getuid());
		homeDir_str = pw->pw_dir;
	}
	std::string prefPath = "" + org + "/" + app;
	if (!isDir(prefPath.c_str())) {
		createDir(prefPath.c_str());
	}
	return prefPath;
}
#endif
