#include <gongen/core/platform.hpp>
#include <gongen/core/exception.hpp>
#ifdef PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#define NOGDI
#include <Windows.h>
#include <string>

std::wstring utf8ToWideString(const std::string& str) {
	size_t size = MultiByteToWideChar(CP_UTF8, 0, str.c_str(), str.size(), NULL, 0);
	std::wstring wstr;
	wstr.resize(size);
	MultiByteToWideChar(CP_UTF8, 0, str.c_str(), str.size(), &wstr[0], wstr.size());
	return wstr;
}

std::string wideStringToUtf8(const std::wstring& wstr) {
	size_t size = WideCharToMultiByte(CP_UTF8, 0, wstr.c_str(), wstr.size(), NULL, 0, NULL, NULL);
	std::string str;
	str.resize(size);
	WideCharToMultiByte(CP_UTF8, 0, wstr.c_str(), wstr.size(), str.data(), str.size(), NULL, NULL);
	return str;
}

std::wstring win32Path(const std::string& str) {
	std::wstring wstr = utf8ToWideString(str);
	size_t size = GetFullPathNameW(wstr.c_str(), 0, NULL, NULL);
	std::wstring out;
	out.resize(size);
	GetFullPathNameW(wstr.c_str(), size, out.data(), NULL);
	return out;
}

void* gen::loadLibrary(const char* name) {
	std::wstring wLibraryName = utf8ToWideString(name);
	return LoadLibraryW(wLibraryName.c_str());
}

void gen::unloadLibrary(void* library) {
	if (library) {
		FreeLibrary((HMODULE)library);
	}
}

void* gen::getProcAddress(void* library, const char* proc) {
	if (library) {
		return GetProcAddress((HMODULE)library, proc);
	}
	else return nullptr;
}

#include <gongen/core/file.hpp>

gen::FileHandle::FileHandle(const std::string& filepath, FileOpenMode openMode, bool throwExceptions) { open(filepath, openMode, throwExceptions); }
gen::FileHandle::~FileHandle() { close(); }

void gen::FileHandle::open(const std::string& filepath, FileOpenMode openMode, bool throwExceptions) {
	if (isOpen()) close();
	std::wstring wstr = win32Path(filepath);
	DWORD dwDesiredAccess = 0;
	DWORD dwShareMode = 0;
	DWORD dwCreationDisposition = 0;
	if (openMode == gen::FileOpenMode::READ) {
		dwDesiredAccess = GENERIC_READ;
		dwShareMode = FILE_SHARE_READ;
		dwCreationDisposition = OPEN_EXISTING;
	}
	else if (openMode == gen::FileOpenMode::WRITE) {
		dwDesiredAccess = GENERIC_WRITE;
		dwShareMode = FILE_SHARE_READ;
		dwCreationDisposition = OPEN_ALWAYS;
	}
	else if (openMode == gen::FileOpenMode::APPEND) {
		dwDesiredAccess = GENERIC_WRITE | FILE_APPEND_DATA;
		dwShareMode = FILE_SHARE_READ;
		dwCreationDisposition = OPEN_ALWAYS;
	}
	else if (openMode == gen::FileOpenMode::READ_WRITE) {
		dwDesiredAccess = GENERIC_WRITE | GENERIC_READ;
		dwShareMode = FILE_SHARE_READ;
		dwCreationDisposition = OPEN_ALWAYS;
	}

	HANDLE handle = CreateFileW(wstr.c_str(), dwDesiredAccess, dwShareMode, nullptr, dwCreationDisposition, FILE_ATTRIBUTE_NORMAL, nullptr);
	if (openMode == gen::FileOpenMode::WRITE) SetEndOfFile(handle);
	DWORD e = GetLastError();

	this->filepath = filepath;
	if (handle != INVALID_HANDLE_VALUE) {
		this->mode = openMode;
		this->os_handle = (void*)handle;
	}
	else {
		if (throwExceptions) {
			throw gen::FileOpenException("Can't open file: " + filepath);
		}
	}
}
void gen::FileHandle::close() {
	if (isMapped()) {
		UnmapViewOfFile(mapped_addr);
		CloseHandle((HANDLE)mapping_handle);
		mapping_handle = nullptr;
		mapped_addr = nullptr;
	}
	if (isOpen()) {
		CloseHandle((HANDLE)os_handle);
		os_handle = nullptr;
	}
}

bool gen::FileHandle::isOpen() { return (bool)os_handle; }
bool gen::FileHandle::isMapped() { return (bool)mapped_addr; }

uint64_t gen::FileHandle::size() {
	if (!isOpen()) return 0;
	LARGE_INTEGER integer;
	GetFileSizeEx((HANDLE)os_handle, &integer);
	return integer.QuadPart;
}
void gen::FileHandle::setSize(uint64_t size) {
	if (!isOpen()) return;
	uint64_t cursorPos = cursor();
	seek(size);
	SetEndOfFile((HANDLE)os_handle);
	seek(cursorPos);
}
uint64_t gen::FileHandle::seek(int64_t offset, FileSeekType type) {
	if (!isOpen()) return 0;
	LARGE_INTEGER out;
	LARGE_INTEGER in;
	in.QuadPart = offset;
	out.QuadPart = 0;
	if (type == FileSeekType::SET) SetFilePointerEx((HANDLE)os_handle, in, &out, FILE_BEGIN);
	else if (type == FileSeekType::CUR) SetFilePointerEx((HANDLE)os_handle, in, &out, FILE_CURRENT);
	else if (type == FileSeekType::END) SetFilePointerEx((HANDLE)os_handle, in, &out, FILE_END);
	return out.QuadPart;
}
uint64_t gen::FileHandle::cursor() {
	if (!isOpen()) return 0;
	LARGE_INTEGER out;
	LARGE_INTEGER in;
	in.QuadPart = 0;
	SetFilePointerEx((HANDLE)os_handle, in, &out, FILE_CURRENT);
	return out.QuadPart;
}

uint64_t gen::FileHandle::read(uint64_t size, void* data) {
	if (!isOpen()) return 0;
	if (!((uint32_t)mode & gen::internal::FLAG_READ)) return 0;
	DWORD out;
	ReadFile((HANDLE)os_handle, data, size, &out, nullptr);
	return out;
}
uint64_t gen::FileHandle::write(uint64_t size, void* data) {
	if (!isOpen()) return 0;
	if (!((uint32_t)mode & gen::internal::FLAG_WRITE)) return 0;
	DWORD out;
	WriteFile((HANDLE)os_handle, data, size, &out, nullptr);
	return out;
}
void gen::FileHandle::flush() {
	if (!isOpen()) return;
	FlushFileBuffers((HANDLE)os_handle);
}

void* gen::FileHandle::map(FileMapMode mode, uint64_t offset, uint64_t size, void* addr, bool throwExceptions) {
	if (isMapped()) return mapped_addr;
	LARGE_INTEGER in;
	in.QuadPart = this->size();
	DWORD flProtect = 0;
	if (mode == FileMapMode::READ) flProtect = PAGE_READONLY;
	else if (mode == FileMapMode::READ_WRITE) flProtect = PAGE_READWRITE;
	mapping_handle = CreateFileMappingA((HANDLE)os_handle, nullptr, flProtect, in.HighPart, in.LowPart, nullptr);
	if (mapping_handle) {
		DWORD dwDesiredAccess = 0;
		if (mode == FileMapMode::READ) dwDesiredAccess = FILE_MAP_READ;
		else if (mode == FileMapMode::READ_WRITE) dwDesiredAccess = FILE_MAP_ALL_ACCESS;
		LARGE_INTEGER in2;
		in2.QuadPart = offset;
		void* out = MapViewOfFileEx(mapping_handle, dwDesiredAccess, in2.HighPart, in2.LowPart, size, addr);
		DWORD e = GetLastError();
		mapped_addr = out;
		return out;
	}
	else {
		if (throwExceptions) {
			throw gen::FileMapException("Can't map file: " + filepath);
		}
		return nullptr;
	}
}
void gen::FileHandle::mapFlush(uint64_t offset, uint64_t size) {
	if (isMapped()) {
		FlushViewOfFile((void*)((uint64_t)mapped_addr + offset), size);
	}
}

#define TICKS_PER_SECOND 10000000
#define EPOCH_DIFFERENCE 11644473600LL

uint64_t windowsTimeToUnixTime(FILETIME& tm) {
	ULARGE_INTEGER largeInt;
	largeInt.LowPart = tm.dwLowDateTime;
	largeInt.HighPart = tm.dwHighDateTime;
	uint64_t windowsTime = largeInt.QuadPart;
	uint64_t unixTime = windowsTime / TICKS_PER_SECOND;
	if (unixTime < EPOCH_DIFFERENCE) return 0;
	unixTime = unixTime - EPOCH_DIFFERENCE;
	return unixTime;
}

uint64_t gen::FileHandle::creationTime() {
	if (!isOpen()) return 0;
	FILETIME tm;
	GetFileTime((HANDLE)os_handle, &tm, nullptr, nullptr);
	return windowsTimeToUnixTime(tm);
}
uint64_t gen::FileHandle::lastModificationTime() {
	if (!isOpen()) return 0;
	FILETIME tm;
	GetFileTime((HANDLE)os_handle, nullptr, nullptr, &tm);
	return windowsTimeToUnixTime(tm);
}
uint64_t gen::FileHandle::lastAccessTime() {
	if (!isOpen()) return 0;
	FILETIME tm;
	GetFileTime((HANDLE)os_handle, nullptr, &tm, nullptr);
	return windowsTimeToUnixTime(tm);
}

#include <shlwapi.h>

bool gen::pathExist(const char* path) {
	std::wstring wstr = win32Path(path);
	return PathFileExistsW(wstr.c_str());
}

bool gen::isDir(const char* path) {
	std::wstring wstr = win32Path(path);
	DWORD ftyp = GetFileAttributesW(wstr.c_str());
	if (ftyp == INVALID_FILE_ATTRIBUTES) return false;
	return (ftyp & FILE_ATTRIBUTE_DIRECTORY);
}

#include <shlobj_core.h>

bool gen::createDir(const char* path) {
	std::wstring wstr = win32Path(path);
	if (isDir(path)) return true;
	else {
		SHCreateDirectoryExW(NULL, wstr.c_str(), NULL);
		return false;
	}
}

std::vector<gen::enumaratedFile> gen::enumarateDir(const char* dir) {
	std::wstring wstr;
	if(dir) wstr = win32Path(dir + std::string("/*"));
	else wstr = win32Path("*");

	WIN32_FIND_DATAW ffd;
	HANDLE hFind = INVALID_HANDLE_VALUE;

	hFind = FindFirstFileW(wstr.c_str(), &ffd);

	if (INVALID_HANDLE_VALUE == hFind) return {};

	std::vector<gen::enumaratedFile> out;
	do {
		gen::enumaratedFile f;
		f.isDirectory = (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY);
		f.name = wideStringToUtf8(ffd.cFileName);
		if (dir) f.path = std::string(dir) + "/" + f.name;
		else f.path = f.name;
		out.push_back(f);
	} while (FindNextFileW(hFind, &ffd) != 0);

	for (int32_t i = 0; i < out.size(); i++) {
		if ((!out[i].name.compare(".")) || (!out[i].name.compare(".."))) {
			out.erase(out.begin() + i);
			i--;
		}
	}

	return out;
}

#include <filesystem>

std::string gen::getExecutableDir() {
	wchar_t szPath[MAX_PATH];

	size_t size = GetModuleFileNameW(NULL, szPath, MAX_PATH);

	std::wstring wstr;
	if (size > MAX_PATH) {
		wstr.resize(size);
		GetModuleFileNameW(NULL, wstr.data(), wstr.size());
	}
	else {
		wstr = std::wstring(szPath);
	}
	return std::filesystem::path(wideStringToUtf8(wstr)).parent_path().string();
}

std::string gen::getPrefDir(const std::string& org, const std::string& app) {
	WCHAR *prefPathOut;
	SHGetKnownFolderPath(FOLDERID_RoamingAppData, 0, NULL, &prefPathOut);
	std::string prefPath = wideStringToUtf8(prefPathOut);
	CoTaskMemFree(prefPathOut);

	prefPath = prefPath + "/" + org + "/" + app;
	gen::createDir(prefPath.c_str());

	return prefPath;
}

#endif
