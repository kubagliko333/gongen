#include <gongen/core/registration.hpp>

gen::Identifier::Identifier(uint64_t hash, std::string literal, std::string namespaceId, std::string itemId) : hash(hash), literal(literal), namespaceId(namespaceId), itemId(itemId) {
}
	
gen::Identifier gen::BlanketIdentifier::native() {
	return gen::identifier(this->namespaceId, this->itemId);
}

gen::BlanketIdentifier::BlanketIdentifier(uint64_t hash, std::string literal, std::string registryId, std::string namespaceId, std::string itemId) : registryId(registryId), gen::Identifier(hash, literal, namespaceId, itemId) {
}

class gen::internal::IdentifierFactory {
public:
	static gen::BlanketIdentifier createBlanketIdentifier(std::string registryId, std::string namespaceId, std::string itemId) {
		std::string literal = registryId + gen::identifierDelimiter + namespaceId + gen::identifierDelimiter + itemId;
		return gen::BlanketIdentifier(gen::hash64((void*) literal.c_str(), literal.size()), literal, registryId, namespaceId, itemId);
	}

	static gen::Identifier createIdentifier(std::string namespaceId, std::string itemId) {
		std::string literal = namespaceId + gen::identifierDelimiter + itemId;
		return gen::Identifier(gen::hash64((void*) literal.c_str(), literal.size()), literal, namespaceId, itemId);
	}
};

const gen::Identifier gen::identifier(std::string namespaceId, std::string itemId) {
	return gen::internal::IdentifierFactory::createIdentifier(namespaceId, itemId);
}

const gen::Identifier gen::identifier(std::string literal) {
	size_t delimiter = literal.find(gen::identifierDelimiter);
	std::string namespaceId = literal.substr(0, delimiter);
	std::string itemId = literal.substr(delimiter+2, literal.length());
	return gen::internal::IdentifierFactory::createIdentifier(namespaceId, itemId);
	// TODO exceptions handling
}

const gen::BlanketIdentifier gen::blanketIdentifier(std::string registryId, gen::Identifier native) {
	return gen::internal::IdentifierFactory::createBlanketIdentifier(registryId, native.namespaceId, native.itemId);
}

const gen::BlanketIdentifier gen::blanketIdentifier(std::string registryId, std::string namespaceId, std::string itemId) {
	return gen::internal::IdentifierFactory::createBlanketIdentifier(registryId, namespaceId, itemId);
}

const gen::BlanketIdentifier gen::blanketIdentifier(std::string literal) {
	size_t delimiter = literal.find(gen::identifierDelimiter);
	std::string registryId = literal.substr(0, delimiter);
	std::string rest = literal.substr(delimiter+2, literal.length());
	delimiter = rest.find(gen::identifierDelimiter);
	std::string namespaceId = rest.substr(0, delimiter);
	std::string itemId = rest.substr(delimiter+2, rest.length());
	return gen::internal::IdentifierFactory::createBlanketIdentifier(registryId, namespaceId, itemId);
	// TODO exceptions handling
}

void* gen::AdaptedObject::getValue(std::string parameter) const {
	return this->data.at(hash32((void*) parameter.c_str(), parameter.size()));
}


gen::AdaptedObjectBuildHelper* gen::AdaptedObjectBuildHelper::setValue(std::string parameter, void* value) {
	this->data[hash32((void*) parameter.c_str(), parameter.size())] = value;
	return this;
}

std::unordered_map<uint32_t, void*> gen::AdaptedObjectBuildHelper::validateAndGetData() {
	for (const std::string& parameter : this->_template->parameters) {
		if (!data.contains(hash32((void*) parameter.c_str(), parameter.size()))) {
			throw ObjectStateException("The adapted object has to provide values for every template parameter! Value for '" + parameter + "' was not set!");
		}
	}
	return this->data;
}

gen::AdaptedObjectBuildHelper::AdaptedObjectBuildHelper(AdaptedTemplate* _template) : _template(_template) {
}

gen::AdaptedObject::AdaptedObject(AdaptedObjectBuildHelper* helper) : data(helper->validateAndGetData()), _template(helper->_template) {
}

gen::AdaptedTemplate::AdaptedTemplate(const RegisteredInfo<AdaptedTemplate>& registeredInfo, const AdaptedTemplate* const parent, const std::vector<std::string> parameters) : Registered<AdaptedTemplate>(this, registeredInfo), parent(parent), parameters(parameters) {
	std::vector<std::string> parametersAll = parent ? std::vector<std::string>(parent->allParameters) : std::vector<std::string>();
	parametersAll.insert(parametersAll.end(), parameters.begin(), parameters.end());
}

gen::AdaptedTemplateBuilder::AdaptedTemplateBuilder(AdaptedTemplate *parent) : parent(parent) {
}

gen::AdaptedTemplateBuilder* gen::AdaptedTemplateBuilder::addParameter(std::string parameter) {
	this->parameters.push_back(parameter);
	return this;
}

gen::AdaptedTemplate* gen::AdaptedTemplateBuilder::create(const RegisteredInfo<AdaptedTemplate>& registeredInfo) const {
	return new AdaptedTemplate(registeredInfo, this->parent, this->parameters);
}

bool gen::internal::RegistryManager::contains(uint32_t idHash) {
	return registries.contains(idHash);
}

bool gen::internal::RegistryManager::contains(std::string id) {
	return this->contains(hash32((void*) id.c_str(), id.size()));
}