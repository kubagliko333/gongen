#include <gongen/core/logger.hpp>
#include <gongen/core/core.hpp>
#include <ctime>

void gen::Logger::log(LogLevel level, const std::string& string) const {
	LogInfo info = { .logger = this->identifier, .timestamp = time(0), .level = level, .text = string };

	Registry<RegisteredForeign<LoggerCallback>>* registry = this->core->getRegistry<RegisteredForeign<LoggerCallback>>("logger_callback");
	if (this->useGlobalCallbacks) {
		for (uint64_t callbackIdHash : this->core->globalLoggerCallbacks) {
			registry->get(callbackIdHash)->foreign->onLog(info, this);
		}
	}
	for (uint64_t callbackIdHash : this->callbacks) {
		registry->get(callbackIdHash)->foreign->onLog(info, this);
	}
}

std::string gen::logLevelName(LogLevel level) {
	switch (level) {
		case LogLevel::DEBUG:
			return "Debug";
		case LogLevel::INFO:
			return "Info";
		case LogLevel::WARNING:
			return "Warn";
		case LogLevel::ERROR:
			return "Error";
		case LogLevel::FATAL:
			return "Fatal";
	}
	return "Unknown";
}


void gen::Logger::debug(const std::string& string) const {
	this->log(LogLevel::DEBUG, string);
}

void gen::Logger::info(const std::string& string) const {
	this->log(LogLevel::INFO, string);
}

void gen::Logger::warning(const std::string& string) const {
	this->log(LogLevel::WARNING, string);
}

void gen::Logger::error(const std::string& string) const {
	this->log(LogLevel::ERROR, string);
}

void gen::Logger::fatal(const std::string& string) const {
	this->log(LogLevel::FATAL, string);
}

gen::Logger::Logger(const RegisteredInfo<Logger>& registeredInfo, Core* core, const std::string& name, std::vector<uint64_t> callbacks, bool useGlobalCallbacks) : Registered(this, registeredInfo), core(core), name(name), callbacks(callbacks), useGlobalCallbacks(useGlobalCallbacks) {
}

gen::LoggerBuilder::LoggerBuilder(Core* core, Identifier identifier) : core(core), identifier(identifier) {
}

gen::LoggerBuilder* gen::LoggerBuilder::setName(std::string name) {
	this->name = name;
	return this;
}

gen::LoggerBuilder* gen::LoggerBuilder::addCallback(Identifier callbackId) {
	this->callbacks.push_back(callbackId.hash);
	return this;
}

gen::LoggerBuilder* gen::LoggerBuilder::setUseGlobalCallbacks(bool useGlobalCallbacks) {
	this->useGlobalCallbacks = useGlobalCallbacks;
	return this;
}

const gen::RegisterCallback<gen::Logger> gen::LoggerBuilder::build() const {
	return [this](const RegisteredInfo<Logger>& registeredInfo) {
		return new Logger(registeredInfo, this->core, this->name, this->callbacks, this->useGlobalCallbacks);
	};
}