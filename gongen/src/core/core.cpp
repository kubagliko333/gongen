#include <gongen/core/core.hpp>
#include <algorithm>

namespace gen {

class StandardOutLoggerCallback : public gen::LoggerCallback {
	virtual void onLog(const gen::LogInfo& info, const gen::Logger* logger) {
		std::string out;
		out.append("[");
		out.append((!logger->name.empty()) ? (logger->name) : (info.logger.literal));
		out.append("] ");
		out.append(gen::logLevelName(info.level));
		out.append(": ");
		out.append(info.text);
		out.append("\n");
		printf("%s", out.c_str());
	}
};

}

gen::Logger* gen::Core::addLogger(gen::LoggerBuilder* builder) {
	return this->getRegistry<Logger>("logger")->add(builder->identifier, builder->build());
}

void gen::Core::addLoggerCallback(const gen::Identifier& id) {
	if (std::find(this->globalLoggerCallbacks.begin(), this->globalLoggerCallbacks.end(), id.hash) != this->globalLoggerCallbacks.end()) {
		return;
	}
	this->globalLoggerCallbacks.push_back(id.hash);
}

gen::Core::Core(std::vector<uint64_t> globalLoggerCallbacks) : Unit(this, new gen::UnitMeta{ .type = UnitType::CORE, .identifier = identifier("gen", "core"), .name = "GongEn Core" }, 0, nullptr), unitRegistry(new UnitRegistry(this, "unit")), globalLoggerCallbacks(globalLoggerCallbacks) {
	// TODO Add pointers to registries in core class
	this->addRegistry((new LegalRegistryBuilder<Logger>("logger"))->allowRemoving());
	Registry<RegisteredForeign<LoggerCallback>>* loggerCallbackRegistry = this->addRegistry((new LegalRegistryBuilder<RegisteredForeign<LoggerCallback>>("logger_callback"))->restrictRemoving());
	loggerCallbackRegistry->add(identifier("gen", "standard_out"), RegisteredForeign<LoggerCallback>::create(new StandardOutLoggerCallback()));
	this->logger = this->addLogger((new gen::LoggerBuilder(this, identifier("gen", "core")))->setName("GongEn Core"));
	this->logger->info("Initialized logging system");

	this->addRegistry(this->unitRegistry);
	gen::UnitMeta* coreMeta = new gen::UnitMeta{ .type = UnitType::CORE, .identifier = identifier("gen", "core"), .name = "GongEn Core" };
	this->unitRegistry->registerInternal(coreMeta->identifier, coreMeta);
	this->unitRegistry->addInstance(coreMeta, this);
	gen_unit_loader::loadUnitsToRegistry(this->unitRegistry);
	this->unitRegistry->closeRegistration();

	std::string info = "Loaded " + std::to_string(this->unitRegistry->size()) + " units: ";
	for (const auto& [key, value] : this->unitRegistry->getMap()) {
		info += value->identifier.literal + " (" + value->self->foreign->name + "), ";
	}
	this->logger->info(info);
}

void gen::CoreBuilder::addLoggerCallback(const gen::Identifier& id) {
	this->globalLoggerCallbacks.push_back(id.hash);
}

gen::Core* gen::CoreBuilder::build() {
	return new gen::Core(this->globalLoggerCallbacks);
}
