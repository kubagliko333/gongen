#include <gongen/graphics_api/graphics.hpp>

bool formatTablesSetuped = false;
uint64_t formatSizeTable[(size_t)gen::GPUFormat::TOTAL_TEXTURE_FORMATS];
bool formatCompressedTable[(size_t)gen::GPUFormat::TOTAL_TEXTURE_FORMATS];
bool format[(size_t)gen::GPUFormat::TOTAL_TEXTURE_FORMATS];

#define ASTC_BPP UINT64_MAX

void setupFormatTables();

uint64_t gen::getGPUFormatBPP(gen::GPUFormat format) {
	if (!formatTablesSetuped) {
		setupFormatTables();
		formatTablesSetuped = true;
	}
	uint64_t out = formatSizeTable[(size_t)format];
	if (out == ASTC_BPP) return 0;
	else return out;
}

uint64_t gen::getTextureSize(gen::GPUFormat format, uint32_t width, uint32_t height, uint32_t depth, uint32_t layers) {
	if (!formatTablesSetuped) {
		setupFormatTables();
		formatTablesSetuped = true;
	}
	uint64_t bbp = formatSizeTable[(size_t)format];
	if (bbp == ASTC_BPP) return 0;
	else {
		uint64_t pixels = width * height * depth * layers;
		if (bbp == 0) return (pixels / 2);
		else return pixels * bbp;
	}
}

uint64_t gen::getAmountOfTextureMipLevels(gen::GPUFormat format, uint32_t width, uint32_t height) {
	if (!formatTablesSetuped) {
		setupFormatTables();
		formatTablesSetuped = true;
	}
	uint64_t bbp = formatSizeTable[(size_t)format];
	if (bbp == ASTC_BPP) return 0;
	else {
		bool compressed = isFormatCompressed(format);
		uint32_t levels = 0;
		uint32_t minW = 1;
		uint32_t minH = 1;
		if (compressed) {
			minW = 4;
			minH = 4;
		}
		uint32_t w = width;
		uint32_t h = height;

		while (true) {
			levels++;
			if ((w <= minW) && (h <= minH)) break;
			w = w / 2;
			h = h / 2;
		}

		return levels;
	}
}

bool gen::isFormatCompressed(gen::GPUFormat format) {
	return formatCompressedTable[(size_t)format];
}

void setupFormatTables() {
	formatSizeTable[(size_t)gen::GPUFormat::BC1_SRGB] = 0;
	formatSizeTable[(size_t)gen::GPUFormat::BC1_UNORM] = 0;
	formatSizeTable[(size_t)gen::GPUFormat::BC3_SRGB] = 1;
	formatSizeTable[(size_t)gen::GPUFormat::BC3_UNORM] = 1;
	formatSizeTable[(size_t)gen::GPUFormat::BC4_UNORM] = 0;
	formatSizeTable[(size_t)gen::GPUFormat::BC4_SNORM] = 0;
	formatSizeTable[(size_t)gen::GPUFormat::BC5_UNORM] = 1;
	formatSizeTable[(size_t)gen::GPUFormat::BC5_SNORM] = 1;
	formatSizeTable[(size_t)gen::GPUFormat::BC6H_SFLOAT] = 1;
	formatSizeTable[(size_t)gen::GPUFormat::BC6H_UFLOAT] = 1;
	formatSizeTable[(size_t)gen::GPUFormat::BC7_SRGB] = 1;
	formatSizeTable[(size_t)gen::GPUFormat::BC7_UNORM] = 1;

	formatSizeTable[(size_t)gen::GPUFormat::ETC2_RGBA_SRGB] = 1;
	formatSizeTable[(size_t)gen::GPUFormat::ETC2_RGBA_UNORM] = 1;
	formatSizeTable[(size_t)gen::GPUFormat::ETC2_RGB_SRGB] = 0;
	formatSizeTable[(size_t)gen::GPUFormat::ETC2_RGB_UNORM] = 0;
	formatSizeTable[(size_t)gen::GPUFormat::EAC_RG_SNORM] = 1;
	formatSizeTable[(size_t)gen::GPUFormat::EAC_RG_UNORM] = 1;
	formatSizeTable[(size_t)gen::GPUFormat::EAC_R_SNORM] = 0;
	formatSizeTable[(size_t)gen::GPUFormat::EAC_R_UNORM] = 0;

	formatSizeTable[(size_t)gen::GPUFormat::R8_SINT] = 1;
	formatSizeTable[(size_t)gen::GPUFormat::R8_SNORM] = 1;
	formatSizeTable[(size_t)gen::GPUFormat::R8_UINT] = 1;
	formatSizeTable[(size_t)gen::GPUFormat::R8_UNORM] = 1;
	formatSizeTable[(size_t)gen::GPUFormat::R16_SFLOAT] = 2;
	formatSizeTable[(size_t)gen::GPUFormat::R16_SINT] = 2;
	formatSizeTable[(size_t)gen::GPUFormat::R16_SNORM] = 2;
	formatSizeTable[(size_t)gen::GPUFormat::R16_UINT] = 2;
	formatSizeTable[(size_t)gen::GPUFormat::R16_UNORM] = 2;
	formatSizeTable[(size_t)gen::GPUFormat::R32_SFLOAT] = 4;
	formatSizeTable[(size_t)gen::GPUFormat::R32_SINT] = 4;
	formatSizeTable[(size_t)gen::GPUFormat::R32_UINT] = 4;

	formatSizeTable[(size_t)gen::GPUFormat::RG8_SINT] = 2;
	formatSizeTable[(size_t)gen::GPUFormat::RG8_SNORM] = 2;
	formatSizeTable[(size_t)gen::GPUFormat::RG8_UINT] = 2;
	formatSizeTable[(size_t)gen::GPUFormat::RG8_UNORM] = 2;
	formatSizeTable[(size_t)gen::GPUFormat::RG16_SFLOAT] = 4;
	formatSizeTable[(size_t)gen::GPUFormat::RG16_SINT] = 4;
	formatSizeTable[(size_t)gen::GPUFormat::RG16_SNORM] = 4;
	formatSizeTable[(size_t)gen::GPUFormat::RG16_UINT] = 4;
	formatSizeTable[(size_t)gen::GPUFormat::RG16_UNORM] = 4;
	formatSizeTable[(size_t)gen::GPUFormat::RG32_SFLOAT] = 8;
	formatSizeTable[(size_t)gen::GPUFormat::RG32_SINT] = 8;
	formatSizeTable[(size_t)gen::GPUFormat::RG32_UINT] = 8;

	formatSizeTable[(size_t)gen::GPUFormat::RGBA8_SINT] = 4;
	formatSizeTable[(size_t)gen::GPUFormat::RGBA8_SNORM] = 4;
	formatSizeTable[(size_t)gen::GPUFormat::RGBA8_UINT] = 4;
	formatSizeTable[(size_t)gen::GPUFormat::RGBA8_UNORM] = 4;
	formatSizeTable[(size_t)gen::GPUFormat::RGBA8_UNORM_SRGB] = 4;
	formatSizeTable[(size_t)gen::GPUFormat::RGBA16_SFLOAT] = 8;
	formatSizeTable[(size_t)gen::GPUFormat::RGBA16_SINT] = 8;
	formatSizeTable[(size_t)gen::GPUFormat::RGBA16_SNORM] = 8;
	formatSizeTable[(size_t)gen::GPUFormat::RGBA16_UINT] = 8;
	formatSizeTable[(size_t)gen::GPUFormat::RGBA16_UNORM] = 8;
	formatSizeTable[(size_t)gen::GPUFormat::RGBA32_SFLOAT] = 16;
	formatSizeTable[(size_t)gen::GPUFormat::RGBA32_SINT] = 16;
	formatSizeTable[(size_t)gen::GPUFormat::RGBA32_UINT] = 16;

	formatSizeTable[(size_t)gen::GPUFormat::D16_UNORM] = 2;
	formatSizeTable[(size_t)gen::GPUFormat::D24_UNORM_S8_UINT] = 4;
	formatSizeTable[(size_t)gen::GPUFormat::D32_SFLOAT] = 4;

	formatSizeTable[(size_t)gen::GPUFormat::BGRA8_SRGB] = 4;
	formatSizeTable[(size_t)gen::GPUFormat::BGRA8_UNORM] = 4;

	memset(formatCompressedTable, 0, (size_t)gen::GPUFormat::TOTAL_TEXTURE_FORMATS * sizeof(bool));
	formatCompressedTable[(size_t)gen::GPUFormat::BC1_SRGB] = true;
	formatCompressedTable[(size_t)gen::GPUFormat::BC1_UNORM] = true;
	formatCompressedTable[(size_t)gen::GPUFormat::BC3_SRGB] = true;
	formatCompressedTable[(size_t)gen::GPUFormat::BC3_UNORM] = true;
	formatCompressedTable[(size_t)gen::GPUFormat::BC4_UNORM] = true;
	formatCompressedTable[(size_t)gen::GPUFormat::BC4_SNORM] = true;
	formatCompressedTable[(size_t)gen::GPUFormat::BC5_UNORM] = true;
	formatCompressedTable[(size_t)gen::GPUFormat::BC5_SNORM] = true;
	formatCompressedTable[(size_t)gen::GPUFormat::BC6H_SFLOAT] = true;
	formatCompressedTable[(size_t)gen::GPUFormat::BC6H_UFLOAT] = true;
	formatCompressedTable[(size_t)gen::GPUFormat::BC7_SRGB] = true;
	formatCompressedTable[(size_t)gen::GPUFormat::BC7_UNORM] = true;

	formatCompressedTable[(size_t)gen::GPUFormat::ETC2_RGBA_SRGB] = true;
	formatCompressedTable[(size_t)gen::GPUFormat::ETC2_RGBA_UNORM] = true;
	formatCompressedTable[(size_t)gen::GPUFormat::ETC2_RGB_SRGB] = true;
	formatCompressedTable[(size_t)gen::GPUFormat::ETC2_RGB_UNORM] = true;
	formatCompressedTable[(size_t)gen::GPUFormat::EAC_RG_SNORM] = true;
	formatCompressedTable[(size_t)gen::GPUFormat::EAC_RG_UNORM] = true;
	formatCompressedTable[(size_t)gen::GPUFormat::EAC_R_SNORM] = true;
	formatCompressedTable[(size_t)gen::GPUFormat::EAC_R_UNORM] = true;
}
