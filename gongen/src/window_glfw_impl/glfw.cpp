#include "glfw.hpp"
#include <string.h>
#include <gongen/core/exception.hpp>

//Temporary soulution until GongEn api and impl registration is done
GEN_EXPORTED gen::WindowAPI* glfwCreateWindowAPI() {
	return new gen::GLFWWindowAPI();
}

gen::KeyButton keyRemapTable[GLFW_KEY_LAST];

void fillKeyRemapTable();

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
void character_callback(GLFWwindow* window, unsigned int codepoint);
void cursor_position_callback(GLFWwindow* window, double xpos, double ypos);
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void drop_callback(GLFWwindow* window, int count, const char** paths);
void window_size(GLFWwindow* window, int xpos, int ypos);
void window_close(GLFWwindow* window);
void window_focus(GLFWwindow* window, int focused);

gen::GLFWWindowAPI::GLFWWindowAPI() {
	if (glfwInit() != GLFW_TRUE) {
		throw gen::HardwareException("Can't Initialized GLFW");
	}
	fillKeyRemapTable();
}
gen::GLFWWindowAPI::~GLFWWindowAPI() {
	glfwTerminate();
}
gen::Window* gen::GLFWWindowAPI::createWindow(const WindowCreateInfo& info) {
	glfwWindowHint(GLFW_DECORATED, (int)info.decorations);
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

	gen::GLFWWindowUserData userData;
	userData.api = this;
	userData.ptr = NULL;
	userData.windowID = info.windowID;

	GLFWmonitor* monitor = NULL;
	int windowW = info.width;
	int windowH = info.height;
	if (info.type == gen::WindowType::FULLSCREEN) monitor = glfwGetPrimaryMonitor();
	GLFWwindow* window = glfwCreateWindow(windowW, windowH, info.title.c_str(), monitor, NULL);
	if (!window) {
		throw gen::HardwareException("Can't Create Window");
	}
	return new gen::GLFWWindow(window, userData, this);
}
void gen::GLFWWindowAPI::poolEvents() {
	events.clear();
	glfwPollEvents();
}

void gen::GLFWWindowAPI::waitEvents() {
	events.clear();
	glfwWaitEvents();
}

gen::GLFWWindow::GLFWWindow(GLFWwindow* window, GLFWWindowUserData userData, gen::WindowAPI* api) {
	this->window = window;
	this->userData = userData;
	this->userData.ptr = this;
	this->api = api;
	glfwSetWindowUserPointer(window, &this->userData);
	
	glfwSetKeyCallback(window, key_callback);
	glfwSetCharCallback(window, character_callback);
	glfwSetCursorPosCallback(window, cursor_position_callback);
	glfwSetMouseButtonCallback(window, mouse_button_callback);
	glfwSetScrollCallback(window, scroll_callback);
	glfwSetDropCallback(window, drop_callback);

	glfwSetWindowSizeCallback(window, window_size);
	glfwSetWindowCloseCallback(window, window_close);
	glfwSetWindowFocusCallback(window, window_focus);
}
gen::GLFWWindow::~GLFWWindow() {
	glfwDestroyWindow(window);
}
bool gen::GLFWWindow::isOpen() {
	return !glfwWindowShouldClose(window);
}
void gen::GLFWWindow::close() {
	glfwSetWindowShouldClose(window, 1);
}
void gen::GLFWWindow::setTitle(const std::string& title) {
	glfwSetWindowTitle(window, title.c_str());
}
void gen::GLFWWindow::resize(uint16_t width, uint16_t height) {
	glfwSetWindowSize(window, width, height);
}
void gen::GLFWWindow::setClipboard(const std::string& content) {
	glfwSetClipboardString(window, content.c_str());
}
std::string gen::GLFWWindow::getClipboard() {
	return std::string(glfwGetClipboardString(window));
}
uint32_t gen::GLFWWindow::getWidth() {
	int w, h;
	glfwGetWindowSize(window, &w, &h);
	return w;
}
uint32_t gen::GLFWWindow::getHeight() {
	int w, h;
	glfwGetWindowSize(window, &w, &h);
	return h;
}

void gen::GLFWWindow::setCursor(gen::Cursor cursor) {
	uint32_t cursor_id = (uint32_t)cursor;

	if (cursors.find(cursor_id) == cursors.end()) {
		if (cursor == gen::Cursor::ARROW) {
			cursors[cursor_id] = glfwCreateStandardCursor(GLFW_ARROW_CURSOR);
		}
		else if (cursor == gen::Cursor::IBEAM) {
			cursors[cursor_id] = glfwCreateStandardCursor(GLFW_IBEAM_CURSOR);
		}
		else if (cursor == gen::Cursor::CROSSHAIR) {
			cursors[cursor_id] = glfwCreateStandardCursor(GLFW_CROSSHAIR_CURSOR);
		}
		else if (cursor == gen::Cursor::HAND) {
			cursors[cursor_id] = glfwCreateStandardCursor(GLFW_POINTING_HAND_CURSOR);
		}
		else if (cursor == gen::Cursor::RESIZE_EW) {
			cursors[cursor_id] = glfwCreateStandardCursor(GLFW_RESIZE_EW_CURSOR);
		}
		else if (cursor == gen::Cursor::RESIZE_NS) {
			cursors[cursor_id] = glfwCreateStandardCursor(GLFW_RESIZE_NS_CURSOR);
		}
		else if (cursor == gen::Cursor::RESIZE_NWSE) {
			cursors[cursor_id] = glfwCreateStandardCursor(GLFW_RESIZE_NWSE_CURSOR);
		}
		else if (cursor == gen::Cursor::RESIZE_NESW) {
			cursors[cursor_id] = glfwCreateStandardCursor(GLFW_RESIZE_NESW_CURSOR);
		}
		else if (cursor == gen::Cursor::RESIZE_ALL) {
			cursors[cursor_id] = glfwCreateStandardCursor(GLFW_RESIZE_ALL_CURSOR);
		}
		else if (cursor == gen::Cursor::NOT_ALLOWED) {
			cursors[cursor_id] = glfwCreateStandardCursor(GLFW_NOT_ALLOWED_CURSOR);
		}
		else {
			return;
		}
	}

	if (cursors.find(cursor_id) != cursors.end()) {
		glfwSetCursor(window, cursors[cursor_id]);
	}
}

void gen::GLFWWindow::setMousePosition(uint32_t x, uint32_t y) {
	glfwSetCursorPos(window, (double)x, (double)y);
}

#ifdef _WIN32
	#define GLFW_EXPOSE_NATIVE_WIN32
#endif
#include <GLFW/glfw3native.h>

uint64_t gen::GLFWWindow::graphicsFunction(WindowGraphicsFunction func, uint64_t userData, uint64_t userData2, uint64_t userData3, uint64_t& out2) {
	if (func == gen::WindowGraphicsFunction::VK_CREATE_SURFACE) {
		VkSurfaceKHR surface = VK_NULL_HANDLE;
		glfwCreateWindowSurface((VkInstance)userData, window, NULL, &surface);
		if (!surface) {
			throw gen::HardwareException("Can't Create Vulkan Surface");
		}
		return (uint64_t)surface;
	}
	
	else if (func == gen::WindowGraphicsFunction::DX_GET_HWND) {
#ifdef _WIN32
		return (uint64_t)glfwGetWin32Window(window);
#endif
	}
	else if (func == gen::WindowGraphicsFunction::GET_FRAMEBUFFER_SIZE) {
		int w, h;
		glfwGetFramebufferSize(window, &w, &h);
		out2 = (uint64_t)h;
		return (uint64_t)w;
	}
	return 0;
}

uint64_t gen::GLFWWindowAPI::graphicsFunction(WindowGraphicsFunction func, uint64_t userData, uint64_t userData2, uint64_t userData3, uint64_t& out2) {
	if (func == gen::WindowGraphicsFunction::VK_GET_REQUIRED_EXTENSIONS) {
		uint32_t count;
		const char** extensions = glfwGetRequiredInstanceExtensions(&count);
		out2 = count;
		return (uint64_t)extensions;
	}
	else if (func == gen::WindowGraphicsFunction::VK_QUEUE_PRESENTATION_SUPPORT) {
		return glfwGetPhysicalDevicePresentationSupport((VkInstance)userData, (VkPhysicalDevice)userData2, userData3);
	}
	return 0;
}

gen::KeyButton toGenKey(int key);
gen::KeyAction toGenAction(int action);
gen::MouseButton toGenButton(int button);
uint32_t toGenKeyMod(int mods);

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
	gen::GLFWWindowUserData* userData = (gen::GLFWWindowUserData*)glfwGetWindowUserPointer(window);
	gen::Event e;
	e.type = gen::EventType::KEY;
	e.key.key = toGenKey(key);
	e.key.action = toGenAction(action);
	e.key.mods = toGenKeyMod(mods);
	userData->api->events.push_back(e);
}

void character_callback(GLFWwindow* window, unsigned int codepoint) {
	gen::GLFWWindowUserData* userData = (gen::GLFWWindowUserData*)glfwGetWindowUserPointer(window);
	gen::Event e;
	e.type = gen::EventType::CHARACTER;
	e.character.codepoint = codepoint;
	userData->api->events.push_back(e);
}

void cursor_position_callback(GLFWwindow* window, double xpos, double ypos) {
	gen::GLFWWindowUserData* userData = (gen::GLFWWindowUserData*)glfwGetWindowUserPointer(window);
	gen::Event e;
	e.type = gen::EventType::MOUSE_POSITION;
	e.mousePosition.x = xpos;
	e.mousePosition.y = ypos;
	userData->api->events.push_back(e);
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods) {
	gen::GLFWWindowUserData* userData = (gen::GLFWWindowUserData*)glfwGetWindowUserPointer(window);
	gen::Event e;
	e.type = gen::EventType::MOUSE_BUTTON;
	e.mouseButton.action = toGenAction(action);
	e.mouseButton.button = toGenButton(button);
	userData->api->events.push_back(e);
}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
	gen::GLFWWindowUserData* userData = (gen::GLFWWindowUserData*)glfwGetWindowUserPointer(window);
	gen::Event e;
	e.type = gen::EventType::MOUSE_WHEEL;
	e.mouseWheel.x = xoffset;
	e.mouseWheel.y = yoffset;
	userData->api->events.push_back(e);
}

void drop_callback(GLFWwindow* window, int count, const char** paths) {
	gen::GLFWWindowUserData* userData = (gen::GLFWWindowUserData*)glfwGetWindowUserPointer(window);
	gen::Event e;
	e.type = gen::EventType::FILE_DROP;
	for (uint32_t i = 0; i < count; i++) {
		e.fileDrop.paths.push_back(std::string(paths[i]));
	}
	userData->api->events.push_back(e);
}

void window_size(GLFWwindow* window, int xpos, int ypos) {
	gen::GLFWWindowUserData* userData = (gen::GLFWWindowUserData*)glfwGetWindowUserPointer(window);
	gen::Event e;
	e.type = gen::EventType::WINDOW_SIZE;
	e.window.windowID = userData->windowID;
	e.window.resize.w = xpos;
	e.window.resize.h = ypos;
	userData->api->events.push_back(e);
}
void window_close(GLFWwindow* window) {
	gen::GLFWWindowUserData* userData = (gen::GLFWWindowUserData*)glfwGetWindowUserPointer(window);
	gen::Event e;
	e.type = gen::EventType::WINDOW_CLOSE;
	e.window.windowID = userData->windowID;
	userData->api->events.push_back(e);
}
void window_focus(GLFWwindow* window, int focused) {
	gen::GLFWWindowUserData* userData = (gen::GLFWWindowUserData*)glfwGetWindowUserPointer(window);
	gen::Event e;
	e.type = gen::EventType::WINDOW_FOCUS;
	e.window.windowID = userData->windowID;
	e.window.focus.focus = focused;
	userData->api->events.push_back(e);
}

gen::KeyButton toGenKey(int key) {
	return keyRemapTable[key];
}

gen::KeyAction toGenAction(int action) {
	if (action == GLFW_PRESS) return gen::KeyAction::PRESS;
	if (action == GLFW_RELEASE) return gen::KeyAction::RELEASE;
	if (action == GLFW_REPEAT) return gen::KeyAction::REPEAT;
	return gen::KeyAction::INVALID;
}

gen::MouseButton toGenButton(int button) {
	if (button == GLFW_MOUSE_BUTTON_LEFT) return gen::MouseButton::LEFT;
	if (button == GLFW_MOUSE_BUTTON_MIDDLE) return gen::MouseButton::MIDDLE;
	if (button == GLFW_MOUSE_BUTTON_RIGHT) return gen::MouseButton::RIGHT;
	return gen::MouseButton::INVALID;
}

uint32_t toGenKeyMod(int mods) {
	uint32_t out = 0;
	if (mods & GLFW_MOD_SHIFT) out |= gen::KEY_MOD_SHIFT;
	if (mods & GLFW_MOD_CONTROL) out |= gen::KEY_MOD_CONTROL;
	if (mods & GLFW_MOD_ALT) out |= gen::KEY_MOD_ALT;
	return out;
}

#undef DELETE

void fillKeyRemapTable() {
	memset(keyRemapTable, 0, sizeof(gen::KeyButton) * GLFW_KEY_LAST);
	keyRemapTable[GLFW_KEY_SPACE] = gen::KeyButton::SPACE;
	keyRemapTable[GLFW_KEY_APOSTROPHE] = gen::KeyButton::APOSTROPHE;
	keyRemapTable[GLFW_KEY_COMMA] = gen::KeyButton::COMMA;
	keyRemapTable[GLFW_KEY_MINUS] = gen::KeyButton::MINUS;
	keyRemapTable[GLFW_KEY_PERIOD] = gen::KeyButton::PERIOD;
	keyRemapTable[GLFW_KEY_SLASH] = gen::KeyButton::SLASH;
	keyRemapTable[GLFW_KEY_0] = gen::KeyButton::KEY_0;
	keyRemapTable[GLFW_KEY_1] = gen::KeyButton::KEY_1;
	keyRemapTable[GLFW_KEY_2] = gen::KeyButton::KEY_2;
	keyRemapTable[GLFW_KEY_3] = gen::KeyButton::KEY_3;
	keyRemapTable[GLFW_KEY_4] = gen::KeyButton::KEY_4;
	keyRemapTable[GLFW_KEY_5] = gen::KeyButton::KEY_5;
	keyRemapTable[GLFW_KEY_6] = gen::KeyButton::KEY_6;
	keyRemapTable[GLFW_KEY_7] = gen::KeyButton::KEY_7;
	keyRemapTable[GLFW_KEY_8] = gen::KeyButton::KEY_8;
	keyRemapTable[GLFW_KEY_9] = gen::KeyButton::KEY_9;
	keyRemapTable[GLFW_KEY_SEMICOLON] = gen::KeyButton::SEMICOLON;
	keyRemapTable[GLFW_KEY_EQUAL] = gen::KeyButton::EQUAL;
	keyRemapTable[GLFW_KEY_A] = gen::KeyButton::A;
	keyRemapTable[GLFW_KEY_B] = gen::KeyButton::B;
	keyRemapTable[GLFW_KEY_C] = gen::KeyButton::C;
	keyRemapTable[GLFW_KEY_D] = gen::KeyButton::D;
	keyRemapTable[GLFW_KEY_E] = gen::KeyButton::E;
	keyRemapTable[GLFW_KEY_F] = gen::KeyButton::F;
	keyRemapTable[GLFW_KEY_G] = gen::KeyButton::G;
	keyRemapTable[GLFW_KEY_H] = gen::KeyButton::H;
	keyRemapTable[GLFW_KEY_I] = gen::KeyButton::I;
	keyRemapTable[GLFW_KEY_J] = gen::KeyButton::J;
	keyRemapTable[GLFW_KEY_K] = gen::KeyButton::K;
	keyRemapTable[GLFW_KEY_L] = gen::KeyButton::L;
	keyRemapTable[GLFW_KEY_O] = gen::KeyButton::O;
	keyRemapTable[GLFW_KEY_P] = gen::KeyButton::P;
	keyRemapTable[GLFW_KEY_Q] = gen::KeyButton::Q;
	keyRemapTable[GLFW_KEY_R] = gen::KeyButton::R;
	keyRemapTable[GLFW_KEY_S] = gen::KeyButton::S;
	keyRemapTable[GLFW_KEY_T] = gen::KeyButton::T;
	keyRemapTable[GLFW_KEY_U] = gen::KeyButton::U;
	keyRemapTable[GLFW_KEY_W] = gen::KeyButton::W;
	keyRemapTable[GLFW_KEY_X] = gen::KeyButton::X;
	keyRemapTable[GLFW_KEY_Y] = gen::KeyButton::Y;
	keyRemapTable[GLFW_KEY_Z] = gen::KeyButton::Z;
	keyRemapTable[GLFW_KEY_LEFT_BRACKET] = gen::KeyButton::LEFT_BRACKET;
	keyRemapTable[GLFW_KEY_BACKSLASH] = gen::KeyButton::BACKSLASH;
	keyRemapTable[GLFW_KEY_RIGHT_BRACKET] = gen::KeyButton::RIGHT_BRACKET;
	keyRemapTable[GLFW_KEY_GRAVE_ACCENT] = gen::KeyButton::GRAVE_ACCENT;
	keyRemapTable[GLFW_KEY_ESCAPE] = gen::KeyButton::ESCAPE;
	keyRemapTable[GLFW_KEY_ENTER] = gen::KeyButton::ENTER;
	keyRemapTable[GLFW_KEY_TAB] = gen::KeyButton::TAB;
	keyRemapTable[GLFW_KEY_BACKSPACE] = gen::KeyButton::BACKSPACE;
	keyRemapTable[GLFW_KEY_INSERT] = gen::KeyButton::INSERT;
	keyRemapTable[GLFW_KEY_DELETE] = gen::KeyButton::DELETE;
	keyRemapTable[GLFW_KEY_RIGHT] = gen::KeyButton::RIGHT;
	keyRemapTable[GLFW_KEY_LEFT] = gen::KeyButton::LEFT;
	keyRemapTable[GLFW_KEY_DOWN] = gen::KeyButton::DOWN;
	keyRemapTable[GLFW_KEY_UP] = gen::KeyButton::UP;
	keyRemapTable[GLFW_KEY_PAGE_UP] = gen::KeyButton::PAGE_UP;
	keyRemapTable[GLFW_KEY_PAGE_DOWN] = gen::KeyButton::PAGE_DOWN;
	keyRemapTable[GLFW_KEY_HOME] = gen::KeyButton::HOME;
	keyRemapTable[GLFW_KEY_END] = gen::KeyButton::END;
	keyRemapTable[GLFW_KEY_CAPS_LOCK] = gen::KeyButton::CAPS_LOCK;
	keyRemapTable[GLFW_KEY_SCROLL_LOCK] = gen::KeyButton::SCROLL_LOCK;
	keyRemapTable[GLFW_KEY_NUM_LOCK] = gen::KeyButton::NUM_LOCK;
	keyRemapTable[GLFW_KEY_PRINT_SCREEN] = gen::KeyButton::PRINT_SCREEN;
	keyRemapTable[GLFW_KEY_PAUSE] = gen::KeyButton::PAUSE;
	keyRemapTable[GLFW_KEY_F1] = gen::KeyButton::F1;
	keyRemapTable[GLFW_KEY_F2] = gen::KeyButton::F2;
	keyRemapTable[GLFW_KEY_F3] = gen::KeyButton::F3;
	keyRemapTable[GLFW_KEY_F4] = gen::KeyButton::F4;
	keyRemapTable[GLFW_KEY_F5] = gen::KeyButton::F5;
	keyRemapTable[GLFW_KEY_F6] = gen::KeyButton::F6;
	keyRemapTable[GLFW_KEY_F7] = gen::KeyButton::F7;
	keyRemapTable[GLFW_KEY_F8] = gen::KeyButton::F8;
	keyRemapTable[GLFW_KEY_F9] = gen::KeyButton::F9;
	keyRemapTable[GLFW_KEY_F10] = gen::KeyButton::F10;
	keyRemapTable[GLFW_KEY_F11] = gen::KeyButton::F11;
	keyRemapTable[GLFW_KEY_F12] = gen::KeyButton::F12;
	keyRemapTable[GLFW_KEY_F13] = gen::KeyButton::F13;
	keyRemapTable[GLFW_KEY_F14] = gen::KeyButton::F14;
	keyRemapTable[GLFW_KEY_F15] = gen::KeyButton::F15;
	keyRemapTable[GLFW_KEY_F16] = gen::KeyButton::F16;
	keyRemapTable[GLFW_KEY_F17] = gen::KeyButton::F17;
	keyRemapTable[GLFW_KEY_F18] = gen::KeyButton::F18;
	keyRemapTable[GLFW_KEY_F19] = gen::KeyButton::F19;
	keyRemapTable[GLFW_KEY_F20] = gen::KeyButton::F20;
	keyRemapTable[GLFW_KEY_F21] = gen::KeyButton::F21;
	keyRemapTable[GLFW_KEY_F22] = gen::KeyButton::F22;
	keyRemapTable[GLFW_KEY_F23] = gen::KeyButton::F23;
	keyRemapTable[GLFW_KEY_F24] = gen::KeyButton::F24;
	keyRemapTable[GLFW_KEY_F25] = gen::KeyButton::F25;
	keyRemapTable[GLFW_KEY_KP_0] = gen::KeyButton::KP_0;
	keyRemapTable[GLFW_KEY_KP_1] = gen::KeyButton::KP_1;
	keyRemapTable[GLFW_KEY_KP_2] = gen::KeyButton::KP_2;
	keyRemapTable[GLFW_KEY_KP_3] = gen::KeyButton::KP_3;
	keyRemapTable[GLFW_KEY_KP_4] = gen::KeyButton::KP_4;
	keyRemapTable[GLFW_KEY_KP_5] = gen::KeyButton::KP_5;
	keyRemapTable[GLFW_KEY_KP_6] = gen::KeyButton::KP_6;
	keyRemapTable[GLFW_KEY_KP_7] = gen::KeyButton::KP_7;
	keyRemapTable[GLFW_KEY_KP_8] = gen::KeyButton::KP_8;
	keyRemapTable[GLFW_KEY_KP_9] = gen::KeyButton::KP_9;
	keyRemapTable[GLFW_KEY_KP_DECIMAL] = gen::KeyButton::KP_DECIMAL;
	keyRemapTable[GLFW_KEY_KP_DIVIDE] = gen::KeyButton::KP_DIVIDE;
	keyRemapTable[GLFW_KEY_KP_MULTIPLY] = gen::KeyButton::KP_MULTIPLY;
	keyRemapTable[GLFW_KEY_KP_SUBTRACT] = gen::KeyButton::KP_SUBTRACT;
	keyRemapTable[GLFW_KEY_KP_ADD] = gen::KeyButton::KP_ADD;
	keyRemapTable[GLFW_KEY_KP_ENTER] = gen::KeyButton::KP_ENTER;
	keyRemapTable[GLFW_KEY_KP_EQUAL] = gen::KeyButton::KP_EQUAL;
	keyRemapTable[GLFW_KEY_LEFT_SHIFT] = gen::KeyButton::LEFT_SHIFT;
	keyRemapTable[GLFW_KEY_LEFT_CONTROL] = gen::KeyButton::LEFT_CONTROL;
	keyRemapTable[GLFW_KEY_LEFT_ALT] = gen::KeyButton::LEFT_ALT;
	keyRemapTable[GLFW_KEY_RIGHT_SHIFT] = gen::KeyButton::RIGHT_SHIFT;
	keyRemapTable[GLFW_KEY_RIGHT_CONTROL] = gen::KeyButton::RIGHT_CONTROL;
	keyRemapTable[GLFW_KEY_RIGHT_ALT] = gen::KeyButton::RIGHT_ALT;
}
