# GongEn documentation
This is the GongEn's documentation generated using Doxygen.
## Links
- [GitLab](https://gitlab.com/bitrium/gongen/gongen) - project's code, contribution.
- [Trello board](https://trello.com/b/A12FaKqf/gongen) - project management, roadmap.
- [Discord](https://discord.gg/VuFjWc4Aar) - discussion, support, announcements.